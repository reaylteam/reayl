<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Offer::class, function (Faker $faker) {
    return [
        'title' =>$faker->randomElement(['Product1','Product2','Product3','Product1','Camera','Cd',]),
        'description'=>$faker->realText,
        'price'=>$faker->randomDigit,
        'user_id'=>App\User::where('level','business')->get()->random()->id,
        'from'=>$faker->dateTime($max = 'now', $timezone = null),
        'to'=>$faker->dateTime($max = 'now', $timezone = null),
        'delivery_time'=>$faker->dateTime($max = 'now', $timezone = null),
        'countries_id'=>\App\Country::all()->random()->id,
        'category_id'=>\App\Country::all()->random()->id,
        'minimum_of_entities'=>rand(1,1000),
        // 'country_id'=>(App\Country::all()->random()->id),
        // 'category_id'=>(App\Category::all()->random()->id),
        'materials'=>'[{"keys":"color","values":"red"},{"keys":"material","values":"steel"},{"keys":"color","values":"red"},{"keys":"material","values":"steel"}]',
        'updated_at'=>$faker->dateTime($max = 'now', $timezone = null),
        'created_at'=>$faker->dateTime($max = 'now', $timezone = null) 
        
    ];
});