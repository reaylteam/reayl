<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Business_profile::class, function (Faker $faker) {
    return [
        'address' => $faker->address,
        'user_id'=>(App\User::all()->random()->id),
        'company_name'=> $faker->name,
        'bio'=> $faker->realText,
        'bussiness_email'=>$faker->email
        
        
    ];
});
