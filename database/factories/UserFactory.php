<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name'           => $faker->randomElement(['Sales Force','Wegmans Food Markets, Inc.','Ultimate Software','The Boston Consulting Group, Inc.','Edawrd Jones','Kimpton Hotels & Restaurants','WorkDay','Hyatt Hotels Corporation','Addidas','Nike','Microsoft','Stryker Corporation','Capital One','American Express','Adobe Systems Incorporated','The Cheesecake Factory Incorporated','NVIDIA Corporation','Orrick, Herrington & Sutcliffe LLP','Hilton Worldwide','Merceds','Huandi','Apple','Huawii','Dell','AT&T','KFC','Four Seasons Hotels','Nokia','Samsung','كبده البرنس','فرغلى','صبحى كابر']),
        'email'          => $faker->unique()->safeEmail,
        'password'       => Hash::make(123456), // secret
        'level'          => 'business',
        'verified'       => $faker->randomElement([0,1]),
        'remember_token' => str_random(10),
    ];
});
