<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'location'=> $faker->address,
        'description'=>$faker->realText,
        'specialization'=>array_rand(['SoftWare'=>'SoftWare','Hardware'=>'HardWare','Idustry'=>'Industry','Traveling'=>'Traveling']),
        'updated_at'=>$faker->dateTime($max = 'now', $timezone = null),
        'created_at'=>$faker->dateTime($max = 'now', $timezone = null) 
        
    ];
});