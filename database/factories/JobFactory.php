<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Job::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'location'=>$faker->address,
        'seniority_level'=>$faker->randomElement(['senior','junior','techLead','fresh']),
        'user_id'=>App\User::where('level','business')->get()->random()->id,
        'description'=>$faker->realText,
        'skills_rquired'=>$faker->text,
        'salary'=>$faker->numberBetween($min = 1000, $max = 9000) ,
        
    ];
});