<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Individual_user::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'last_name'=> $faker->realText,
        'profissional_title'=>$faker->jobTitle,
        'about_me'=>$faker->realText,
        'facebook_url'=>$faker->url,
        'twitter_url'=>$faker->url,
        'linkedin_url'=>$faker->url,
        'behance_url'=>$faker->url,
        'drobble_url'=>$faker->url,
        'github_url'=>$faker->url,
        'website_url'=>$faker->url,
        'location'=>$faker->address,
        'user_id'=>App\User::all()->random()->id
        
    ];
});

