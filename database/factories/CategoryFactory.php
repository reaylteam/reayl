<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'title' => $faker->randomElement(['Gaming','Arts','Sporting','CS','IT','PHP','Jobs','Furniter','Hiring','Politics','Tech','Development','Training','Courses','HR','Sales','Agency','Cars','Python','Data Science','AI','SoftWare Development']),
        'slug'=> str_slug($faker->randomElement(['Gaming','Arts','Sporting','CS','IT','PHP','Jobs','Furniter','Hiring','Politics','Tech','Development','Training','Courses','HR','Sales','Agency','Cars','Python','Data Science','AI','SoftWare Development'])),
        'description'=>$faker->realText,
        'category_id'=>(App\Category::all()->count())?App\User::all()->random()->id:null,
        'updated_at'=>$faker->dateTime($max = 'now', $timezone = null),
        'created_at'=>$faker->dateTime($max = 'now', $timezone = null) 
        
    ];
});
