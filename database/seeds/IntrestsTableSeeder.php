<?php

use Illuminate\Database\Seeder;
use App\Intrest;
class IntrestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('intrests')->delete();
        factory(App\Intrest::class, 50)->create();
        // Intrest::create(['name'=>'Football']);
        // Intrest::create(['name'=>'Programming']);
        // Intrest::create(['name'=>'Devoloping']);
        // Intrest::create(['name'=>'Reading']);
        // Intrest::create(['name'=>'Fashion']);
        // Intrest::create(['name'=>'Sports News']);
        // Intrest::create(['name'=>'Traveling']);
        // Intrest::create(['name'=>'News']);
    }
}
