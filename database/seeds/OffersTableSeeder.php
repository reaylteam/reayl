<?php

use Illuminate\Database\Seeder;
use App\Offer;
class OffersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('offers')->delete();
        factory(App\Offer::class, 50)->create();
        // Offer::create([]);
    }
}
