<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('ratings')->delete();
        DB::table('users')->delete();
        DB::table('individual_users')->delete();
        DB::table('business_profiles')->delete();

		factory(App\User::class, 50)->create()->each(function ($u) {
			$u->posts()->save(factory(App\Post::class)->make());
            if($u->level == 'user')
                $u->individual_user_info()->save(factory(App\Individual_user::class)->make());
            else
                $u->business_user_info()->save(factory(App\Business_profile::class)->make());
		});

    }
}
