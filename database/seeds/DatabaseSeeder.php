<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(Admins_TableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(IntrestsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(OffersTableSeeder::class);
        $this->call(CompanyTableSeeder::class);
        
        $this->call(JobsTableSeeder::class);
        
    }
}
