<?php

use Illuminate\Database\Seeder;
use App\Static_page;
class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('static_pages')->delete();
        Static_page::create(['name'=>'Football']);
        Static_page::create(['name'=>'Programming']);
        Static_page::create(['name'=>'Devoloping']);
        Static_page::create(['name'=>'Reading']);
        Static_page::create(['name'=>'Fashion']);
        Static_page::create(['name'=>'Sports News']);
        Static_page::create(['name'=>'Traveling']);
        Static_page::create(['name'=>'News']);
    }
}
