<?php
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
route::get('/forcelogin',function(){
    \Auth::loginUsingId(3);
});

Route::get('/', 'LandingController@index');

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');

Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/user/verify/{token}', 'Auth\AuthController@verifyUser');
Route::get('user/finish/{email}','BusinessUserSubusersManagementController@completeRegister');
Route::post('/post/comments/loaddata','PostsController@loadCommentsDataAjax');
Route::post('posts/loaddata','PostsController@loadDataAjax');
Route::post('companies/loaddata','CompaniesController@loadDataAjax');
Route::post('offers/loaddata','OffersController@loadDataAjax' );
Route::post('/user/setting/password','Auth\AuthController@setting_password');
Route::get('/setusertype/{type}',function($type){
    \Session::put('usertype',$type);
});
Route::group(['middleware' =>'auth'], function () {
    Route::resource('profile','ProfileController');
    Route::get('myprofile/edit','ProfileController@edit');
    Route::get('/followcompany/{id}','CompaniesController@follow');
    Route::post('/update/usercover/','ProfileController@editmycover');
    Route::post('/user/apply/attach_cv/','JobsController@apply');
    Route::get('/users/{user_id}/{user_name}/list','BusinessUserSubusersManagementController@list_users');
    Route::get('/users/delete/{id}','BusinessUserSubusersManagementController@delete');
    Route::get('/offers/delete/{id}','OffersController@delete');
    // Route::get('/jobs/delete/{id}','JobsController@delete');
    Route::get('/users/add-account/','BusinessUserSubusersManagementController@registerUser');
    Route::post('/users/register','BusinessUserSubusersManagementController@register');
    Route::get('myApplications/{user_id}/{user_name}','JobsController@myApplications');
    Route::post('/review/item','ReviewController@reviewItem');
    Route::get('/user/location/setup/{lat}/{lang}','ProfileController@setUpLocation');
    Route::post('editmyprofile','ProfileController@editmyprofile');
    Route::get('/newsfeed','PostsController@index');
    Route::post('registersubuser','BusinessUserSubusersManagementController@registersubuser');
    Route::get('/usertobusiness','BusinessUserSubusersManagementController@usertobusiness');
    Route::delete('cart/{id}', 'CartController@destroy')->name('cart-item-delete');
    Route::get('/cart/add/offer/{offer_id}','CartController@addToCart');
    Route::get('/cart/list/{user_id}/{user_name}','CartController@myOffers');
    Route::resource('cart','CartController');
});
Route::get('shared/{shareable_link}', ['middleware' => 'shared', function (ShareableLink $link) {
    return $link->shareable;
}]);

Route::group(['middleware'=>'web','prefix' => LaravelLocalization::setLocale()],function(){
	// Route::resource('posts','PostsController');
	Route::resource('companies','CompaniesController');
	Route::get('/Alloffers/{companyid}/','OffersController@CompanyOffers');
    Route::get('offerDetailes/{offer_id}','OffersController@offerDetailes');
    Route::resource('offers','OffersController');
    Route::get('/search/results/','SearchController@home_search');
    Route::get('/search/results/filter/','SearchController@filter_search');
    Route::get('/search/results/offers','SearchController@searchOffer');
    Route::get('/search/offers/results','SearchController@searchOffers');
    // Route::resource('jobs','JobsController');
    // Route::get('/company/jobs/{id}','JobsController@comapnyjobs');
    // Route::get('/jobs/showDeatile/{id}/{title}','JobsController@JobDeatile');
    Route::get('/followers','ProfileController@followers');
    Route::get('/followings','ProfileController@followings');
    Route::get('/profile/{id}/{name}','ProfileController@ProfileShow');
    Route::get('/showProfile/{type}/{id}/{name}','ProfileController@publicProfile');
    Route::get('/offersOf/{user_name}/{id}/all','OffersController@allOffers');
    Route::get('/postsOf/{user_name}/{id}/all','PostsController@allPosts');
    // Route::get('/tags/search/{id}','TagsController@searchTag');
    Route::get('/categories/search/{id}','CategoriesController@searchCategory');
    Route::post('doLogin','Auth\AuthController@doLogin')->name('doLogin');
    //Route::get('site/pages/{id}','StaticPagesController@show');
    Route::get('/all/users/','LandingController@list_all_users');
});


Route::get('/chat', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');

//static Routes Section


Route::get('/site/pages/{page_name}','StaticPagesController@getPage');
