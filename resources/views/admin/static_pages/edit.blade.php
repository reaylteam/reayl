@extends('admin.index')
@section('content')


<div class="box">
  <div class="box-header">
    <h3 class="box-title">{{ $title }}</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    {!! Form::open(['url'=>aurl('static_pages/'.$page->id),'method'=>'put' ]) !!}
     <div class="form-group">
        {!! Form::label('name',trans('admin.name')) !!}
        {!! Form::text('name',$page->name,['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::hidden('slug',$page->slug,['class'=>'form-control']) !!}
     </div>
     <div class="form-group">
        {!! Form::label('description',trans('admin.content')) !!}
        {!! Form::textarea('content',$page->content,['class'=>'form-control']) !!}
     </div>

       


     {!! Form::submit(trans('admin.save'),['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
  </div>
  <!-- /.box-body -->
</div>
<!-- /.box -->



@endsection