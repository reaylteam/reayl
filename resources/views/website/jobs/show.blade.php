@extends('front.main')
@section('content')

<div class="content-wrapper pl-0 pt-0 pr-md-0 pr-sm-0 pr-xs-0">
            <div class="container-fluid pl-0 pr-md-0 pr-sm-0 pr-xs-0">
                <div class="row">
                    <!-- Page Content -->
                    <div class="col-lg-9 col-md-12 col-sm-12 pr-4 pr-md-0 pr-sm-0 pr-xs-0">
                        <div class="container pr-0 pl-0 pr-md-0 pr-sm-0 pr-xs-0">
                            <img src="/front_assets/images/brands/cover-1.jpg" class="main-img container px-0" alt="">
                        </div>
                        <div class="container border-right">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-9">
                                    <div class="profile-img ml-0 mx-xs-auto">
                                        <img src="/front_assets/images/brands/udacity.png" class="rounded-circle" alt="image">
                                    </div>
                            @if (\Session::has('message'))
								<div class="alert alert-info" name="session-status_txt">{{ \Session::get('message') }}</div>
							@endif
                                    <div class="d-flex mt--3 d-xs-block text-xs-center">
                                        <div class="mr-auto">
                                            <h3 class="d-block my-2" name="job-title_txt">{{$job->title}}</h3>
                                            <div class="d-block">
                                                <small class="d-inline">Posted <span class=" text-muted" name="job-duration_txt">{{$job->duration}}</span></small>
                                                <small class="ml-3 d-inline">Number of applicants <span class=" text-muted" name="applicants-number_txt" >{{$job->applications_count->count()}}</span></small>
                                            </div>
                                            <div class="d-block">
                                                <small class="d-inline" name="job-owner_txt">{{$job->user->name}}</small>
                                                <small class="ml-3 d-inline">Company Location <span name="job-location_txt">{{$job->user->business_user_info->address}}</span></small>
                                            </div>
                                        </div>
                                        <div class="ml-auto">
                                            <a href="#" class="btn btn-primary btn-rounded btn-fw btn-gred px-5" data-toggle="modal" data-target="#attach_cv" name="apply-popup_link">Apply Now</a>
                                        </div>
                                    </div>
                                    <div class="content my-4">
                                        <h4>Job Description</h4>
                                        <div class="text-muted" name="job-description_txt">
                                            {!! $job->description !!}
                                        </div>
                                        <h4>Skills required</h4>
                                        <div class="tags">
                                            <a class="badge badge-primary badge-pill tag-item" href="#" name="job-skills_txt">{{$job->skills_rquired}}</a>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Content -->
                    <!-- Right Sidebar -->
                    <div class="col-lg-3 col-md-12 col-sm-12 px-4 pt-5">
                        <!-- wedget -->
                        <div class="wedget">
                            <div class="row">
                                <h6 class="col-12 pb-2">OTHER JOBS FOR SAME COMPANY</h6>
                                <div class="col-12 jobs-list pl-2">
                                    @foreach($first_four_jobs as $job)
                                    <!-- jobs list item -->
                                    <div class="list d-flex align-items-center pl-2 pt-3">
                                        <div class="wrapper w-100 ml-3">
                                            <a href="{{URL::to('jobs/showDeatile/'.$job->id.'/'.$job->title)}}" class="d-block mb-0" name="job-title_txt"><b>{{$job->title}}</b></a>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex align-items-center">
                                                    <p class="mb-0" name="job-skills_txt">{{$job->skills_rquired}}</p>
                                                </div>
                                                <small class="text-muted ml-auto" name="job-salary_txt">{{$job->salary}} USD</small>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /jobs list item -->
                                    @endforeach
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /wedget -->
                        <hr class="p-2">
                        <!-- wedget -->
                        <div class="wedget">
                            <div class="row">
                                <h6 class="mb-2 col text-muted">APPS WE USE TO STAY CONNECTED</h6>
                            </div>
                            <div class="row">
                                <div class="social-icons text-center">
                                    <a href="#"><i class="fa fa-phone bg-light"></i></a>
                                    <a href="#"><i class="fa fa-envelope bg-light"></i></a>
                                    <a href="#"><i class="fa fa-skype bg-light"></i></a>
                                    <a href="#"><i class="fa fa-slack bg-light"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- /wedget -->
                    </div>
                    <!-- /Right Sidebar -->
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <div class="content-wrapper bg-gray">
            <div class="container">
                <!--Companies list-->
                <div class="row companies-list">
                    <div class="col-12 p-3">
                        <h4 class="mb-2">
                            YOU MAY LIKE
                        </h4>
                    </div>
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                                          </a>
                                <h6 class="d-block my-2">Amazone</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                                          </a>
                                <h6 class="d-block my-2">Dove</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                                  </a>
                                <h6 class="d-block my-2">Servicam</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                                          </a>
                                <h6 class="d-block my-2">Amazone</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                                          </a>
                                <h6 class="d-block my-2">Dove</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                                          </a>
                                <h6 class="d-block my-2">Servicam</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                </div>
                <!--/Companies list-->
            </div>
        </div>
        <!-- page-body-wrapper ends -->
@include('website.modals.attachCv')
@endsection