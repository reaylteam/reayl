@extends('front.main')
@section('content')   
    <nav class="container-fluid bg-gray p-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-6 col-sm-12 text-md-left text-sm-center text-xs-center">
                    <h4 class="p-1">
                        Jobs
                        <small class="text-muted ml-3" name="companies-count_txt">
                        {{count($companies)}} Professional Company
                    </small>
                    </h4>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="input-group float-right">
                        <div class="input-group-prepend text-center">
                            <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="content-wrapper">
        <div class="container">
            <div class="row">
                <!-- Page Content -->
                <div class="col-lg-9 col-md-12 col-sm-12 pr-4 px-xs-0">
                    <div class="container pr-4 px-xs-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="browse">
                                    <h6 class="mb-3 ml-2">BROWSE JOBS</h6>
                                    <form action="#">
                                        <div class="container">
                                            <div class="input-group find-group float-right">
                                                <div class="input-group-prepend text-center">
                                                    <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                </div>
                                                <input type="text" class="form-control border-0 search-top" placeholder="Search..." aria-label="Username" aria-describedby="basic-addon1">
                                                <input type="submit" class="btn btn-primary btn-rounded border-0 btn-fw btn-gred" value="FIND JOB" />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <p class="m-2"><cite class="text-muted" name="company-jobs_txt">{{count($jobs)}}</cite> <i>Jobs</i></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 jobs-list">
                            @foreach($jobs as $job)
                                <!-- jobs list item -->
                                <a href="{{URL::to('/jobs/showDeatile/'.$job->id.'/'.$job->title)}}" >
                                    <div class="list d-flex align-items-center pl-2 pt-3">
                                        <img class="img-md rounded-circle border" src="/front_assets/images/Brands/dove.gif" alt="">
                                        <div class="wrapper w-100 ml-3">
                                            <a href="{{URL::to('/jobs/showDeatile/'.$job->id.'/'.$job->title)}}" class="d-block mb-0" name="job-detail_link"><b>{{$job->title}}</b></a>
                                            <small class="text-muted" name="job-company_name_txt">in {{$job->user->name}}</small>
                                            <div class="d-flex justify-content-between align-items-center">
                                                <div class="d-flex align-items-center">
                                                    <p class="mb-0"><b name="job-description_txt">{{$job->description}}</b></p>
                                                </div>
                                                <small class="text-muted mr-auto ml-5" name="job-salary_txt">{{$job->salary}} USD . Full time</small>
                                            </div>
                                            <small class="text-muted" name="job-location_txt" >{{$job->location}}</small>
                                        </div>
                                    </div>
                                </a>
                                <!-- /jobs list item -->
                                <hr>
                            @endforeach
                            </div>
                        </div>
                        <!-- Newsfeeds -->
                        <div class="my-5">
                            <div class="clearfix"></div>
                            <!-- load more -->
                            <div class="container text-center">
                                <a href="#" class="load-more mx-auto">
                                    <span>Load More Jobs</span>
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                            </div>
                        </div>
                        <!-- /Newsfeeds -->
                    </div>
                </div>
                <!-- /Page Content -->
                <!-- Right Sidebar -->
                <div class="right-sidebar col-lg-3 col-md-12 col-sm-12 pl-4 px-xs-0">
                    <!-- wedget -->
                    {{-- @include('website.partials.top_tags') --}}
                    <!-- /wedget -->
                    {{-- <hr class="p-2"> --}}
                    {{-- @include('website.partials.people_may_know') --}}
                </div>
                <!-- /Right Sidebar -->
            </div>
        </div>
    </div>
@endsection
