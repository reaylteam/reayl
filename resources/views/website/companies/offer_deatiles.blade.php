@extends('front.main') @section('header_after')
<link rel="stylesheet" href="/front_assets/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css"> @endsection @section('content')
<!--/navbar header for mobile-->
<!-- partial -->
<nav class="container-fluid bg-gray p-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-md-6 col-sm-12 text-md-left text-sm-center text-xs-center">
                <h4 class="d-flex p-1 mb-0">
                    {{ $offer->company->name }}
                    <small class="d-flex text-muted ml-3 my-auto" name="companies-count_txt">
                        <div id="connection" class="m-1">
                            <span id="followers">{{ count($offer->company->follower) }}</span> Followers
                        </div>
                        <i> . </i>
                        <div id="connection" class="m-1">
                           Following <span id="following">{{ count($offer->company->following) }}</span>
                        </div>
                    </small>
                </h4>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-12">
                <div class="text-lg-center text-md-center text-sm-center text-xs-center my-xs-2">
                    @if(\Auth::check())
                        @if(\Auth::user()->id != $offer->user_id)
                            <button type="submit" class="btn btn-primary btn-rounded btn-md btn-gred px-5 ml-3 followbtn" company-id="{{$offer->user_id}}">{{(\Auth::user()->checkfollow($offer->user_id))?'UnFollow':'Follow'}}</button> 
                        @endif
                    @else
                        <button type="submit" class="btn btn-primary btn-rounded btn-md btn-gred px-5 ml-3" data-toggle="modal" data-target="#sign_in">Follow</button> 
                    @endif
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="col-12 alert alert-fill-danger rounded-0 mb-0 p-2" role="alert" style="display:none;">
    <i class="fa fa-exclamation-circle"></i> It's may Already in Cart

    <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="false">&times;</span>
    </button>
</div>

<div class="col-12 alert alert-fill-success rounded-0 mb-0 p-2" role="alert" style="display:none">
    <i class="fa fa-exclamation-circle"></i> Offer Added Successfully

    <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="false">&times;</span>
    </button>
</div>
<div class="content-wrapper py-1">
    <div class="container">
        <div class="row user-profile">
            <div class="col-lg-4 side-left">
                <div class="row">
                    <div class="col-12 pb-2">
                        <div class="card">
                            <div class="card-body pb-1 text-center">
                                @if(count($offer->images()))
                                <img src="{{ $offer->images()->first()->getUrl()}}" class="img-lg rounded-circle my-1" alt=""> @else
                                <img src="/front_assets/images/products/anonymous-offer.jpg" class="img-lg rounded-circle my-1" alt=""> @endif
                                <h4 class="name my-1">{{ $offer->title }}</h4>
                                <div class="row"><span class="badge badge-success mx-auto">{{ (date("Y-m-d")>$offer->to)?'Not Available':'Available until '.$offer->to->format('Y-m-d') }}</span></div>
                                <div class="row text-center my-2">
                                    <a class="text-dark mx-auto" href="#">
                                        <img src="/front_assets/images/brands/anonymous-category.png" class="img-xs rounded-circle" alt="">
                                        {{ ($offer->category)?$offer->category->title:'No Category'}}
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-12 my-1">
                        <div class="card">
                            <div class="card-body overview p-1">
                                <div id="legend" class="donut-legend">
                                    <span><strong>Colors:</strong></span> @foreach ($options as $option)
                                    <span>
                                        <span >&nbsp;</span> {{ $option['keys']}} : {{ $option['values'] }}
                                    </span>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 my-1">
                        <div class="card">
                            <div class="card-body overview p-1">
                                <ul class="achivements">
                                    <li>
                                        <p>Posted</p>
                                        <p>{{ $offer->duration}}</p>
                                    </li>
                                    <li>
                                        <p>Country</p>
                                        <p>{{ $offer->country->name }}</p>
                                    </li>
                                    <li>
                                        <p>Price</p>
                                        <p>{{ $offer->price}}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 side-right stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="wrapper d-block d-sm-flex align-items-center justify-content-between">
                            <ul class="nav nav-tabs tab-solid tab-solid-primary mb-0 d-flex" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="details-tab" data-toggle="tab" href="#details" role="tab" aria-controls="details" aria-expanded="true">Offer Details</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="avatar">Reviews</a>
                                </li>
                            </ul>
                            @if(\Auth::check() && \Auth::user()->id ==$offer->user_id)
                            <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred py-3" id="edit_offer_btn" offerID="{{$offer->id}}">Edit offer</button> @endif @if(\Auth::check())
                            <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred py-3 add_to_cart" offerID="{{$offer->id}}">Add to cart</button> @else
                            <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred py-3 " data-toggle="modal" data-target="#sign_in">Add to cart</button> @endif
                        </div>
                        <div class="wrapper">
                            <div class="tab-content border-0" id="myTabContent">
                                <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details">
                                    <div class="owl-carousel owl-theme full-width">
                                        @if(count($offer->images())) @foreach ($offer->images() as $image)
                                        <div class="item">
                                            <img src="{{ $image->getUrl() }}" alt="image" />
                                        </div>
                                        @endforeach @endif
                                    </div>
                                    <div class="row">
                                        <h4>Offer description:</h4>
                                        <p>{{ $offer->description }}</p>
                                    </div>
                                </div>
                                <!-- tab content ends -->
                                <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 border-right my-3">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <h3 class="text-muted font-weight-light">Averarge review</h3>
                                                </div>
                                                <span class="average-review span-lg rounded-circle mx-auto" name="average_review">{{$offer->avgrating()}}</span>
                                                <div class="col-12 text-center mt-3">
                                                    <select class="mx-auto" id="average_review" name="average_rating" autocomplete="off">
                                                    <option value="1" {{($offer->avgrating()==1)?'selected':''}}>1</option>
                                                    <option value="2" {{($offer->avgrating()==2)?'selected':''}}>2</option>
                                                    <option value="3" {{($offer->avgrating()==3)?'selected':''}}>3</option>
                                                    <option value="4" {{($offer->avgrating()==4)?'selected':''}}>4</option>
                                                    <option value="5" {{($offer->avgrating()==5)?'selected':''}}>5</option>
                                                  </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12 my-auto my-3">
                                            @if(\Auth::user()->id!=$offer->user_id)
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <h3 class="text-muted font-weight-light">Review this offer :</h3>
                                                    </div>
                                                    <div class="col-12 text-center mt-3">
                                                        <select class="mx-auto" id="make_review" name="rating" autocomplete="off">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-12 border-top my-3 p-2">
                                            @if(\Auth::user()->id!=$offer->user_id)
                                            <h5>Type your review :</h5>
                                            <form class="create-post-form" id="review_form">
                                                <div class="row">
                                                    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 my-sm-2 my-xs-2">
                                                        <a href="#">
                                                            <img class="img-sm rounded-circle border" src="/front_assets/images/faces/anonymous-user.png" alt="">
                                                       </a>
                                                    </div>
                                                    <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 my-auto">
                                                        <div class="input-group find-group float-right">
                                                            <input type="text" class="form-control border-0 search-top comment-body-input" id="review_body" placeholder="Write a review..." aria-label="Username" aria-describedby="basic-addon1">
                                                            @if(\Auth::check())
                                                                <span class="btn btn-primary btn-rounded border-0 btn-sm btn-gred" name="review-btn" id="review_btn">
                                                                    Review
                                                                </span>
                                                            @else    
                                                                <span class="btn btn-primary btn-rounded border-0 btn-sm btn-gred" name="review-btn"  data-toggle="modal" data-target="#sign_in">
                                                                    Review
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::hidden('rateable_type','App\Offer',['id'=>'rateable_type']) !!} {!! Form::hidden('item_id',$offer->id,['id'=>'item_id']) !!}
                                            </form>
                                            @endif
                                            <!--list of reviews-->
                                            <ul class="bullet-line-list" id="review_div">
                                                @foreach($offer->reviews() as $review)
                                                <li id="user_{{ $review->user_id }}">
                                                    <div class="d-flex align-items-center border-bottom py-1">
                                                        @if($review->user->profile())
                                                        <img class="img-sm rounded-circle" src="{{ $review->user->profile()->getUrl() }}" alt=""> @else
                                                        <img class="img-sm rounded-circle" src="/front_assets/images/faces/anonymous-user.png" alt=""> @endif {{-- <img class="img-sm rounded-circle" src="http://127.0.0.1:8000/media/9/24143.png" alt=""> --}}

                                                        <div class="wrapper w-100 ml-1 pl-0 user-review">
                                                            <span class="mr-auto"><b>{{$review->user->name}} </b></span>
                                                            <small class="text-muted ml-1">{{ $review->created_at->diffForHumans() }}</small>
                                                            <select class="mx-auto user_rating" name="rating" >
                                                                <option value="1" {{ ($review->rating >= 1)?'selected':'' }}>1</option>
                                                                <option value="2" {{ ($review->rating >=2)?'selected':'' }}>2</option>
                                                                <option value="3" {{ ($review->rating >=3)?'selected':'' }}>3</option>
                                                                <option value="4" {{ ($review->rating >= 4)?'selected':'' }}>4</option>
                                                                <option value="5" {{ ($review->rating ==5)?'selected':'' }}>5</option>
                                                            </select>
                                                            <p class="mb-0">{{$review->comment}}</p>
                                                        </div>
                                                    </div>
                                                </li>
                                                @endforeach
                                                <div class="clearfix"></div>
                                                <!-- load more comments -->
                                                {{-- <div class="container text-center mt-2">
                                                    <button type="button" class="btn btn-primary btn-rounded btn-xs btn-gred"><i class="fa fa-spinner fa-pulse"></i> Load more reviews</button>
                                                </div> --}}
                                                <!-- load more comments -->
                                            </ul>
                                            <!--/list of reviews-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- content-wrapper ends -->
<div class="content-wrapper bg-gray">
    <div class="container">
        <!--Companies list-->
        <div class="row companies-list">
            <div class="col-12 p-3">
                <h4 class="mb-2">
                    YOU MAY LIKE
                </h4>
            </div>
            @foreach($likeoffers as $likeoffer)
                <!--company-list-item-->
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                    <div class="card text-center bg-gray">
                        <div class="avatar">
                            @if($offer->images()->first())
                                <a href="{{URL::to('offerDetailes/'.$likeoffer->id)}}" class="d-block mb-0" name="offer_detail" id="offer_detail">
                                    <img class="mb-4 img-lg rounded border" src="{{$likeoffer->images()->first()->getUrl()}}" alt="" name="offer_img" id="offer_img">
                                </a>
                            @else
                               <a href="{{URL::to('offerDetailes/'.$likeoffer->id)}}" class="d-block mb-0" name="offer_detail" id="offer_detail">
                                   <img class="mb-4 img-lg rounded border" src="/front_assets/images/products/anonymous-offer.jpg" alt="" name="offer_img" id="offer_img">
                                </a>
                            @endif
                            
                            <h6 class="d-block my-2">{{ $likeoffer->title }}</h6>
                            <small class="d-block text-muted">{{ ($likeoffer->category)?$likeoffer->category->title:'No Category'}}</small>
                            <small class="d-block text-muted">$ {{ $likeoffer->price }}</small>
                        </div>
                    </div>
                </div>
                <!--/company-list-item-->
            @endforeach
            
        </div>
        <!--/Companies list-->
    </div>
</div>
<!-- page-body-wrapper ends -->

<!-- Edit Offer Modal start -->
<div class="modal fade" id="edit_offer" tabindex="-1" role="dialog" aria-labelledby="edit_offer" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                <div class="content">
                    <h3 class="col-12 font-weight-light text-center mb-3">Edit offer</h3>
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="container" id="offer_edit_model">


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Edit Offer Modal Ends -->

@endsection @section('footer_after')
<script src="/front_assets/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
<script>
    (function($) {
        'use strict';
        // Jquery Bar Rating Starts
        $(function() {
            function ratingEnable() {
                $('.user_rating').barrating({
                    theme: 'fontawesome-stars',
                    showSelectedRating: true,
                    deselectable: false,
                    readonly: 'true'
                });
            }
            ratingEnable();
        });
        // Jquery Bar Rating Ends
    })(jQuery);
</script>
@endsection