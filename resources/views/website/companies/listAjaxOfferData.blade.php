@foreach($offers as $offer)
<!-- offers list item -->
<div class="list d-flex align-items-center col-lg-3 col-md-4 col-sm-6 my-3">
    @if($offer->image()->first())
    <a href="{{URL::to('offerDetailes/'.$offer->id)}}" class="d-block mb-0" name="offer_detail" id="offer_detail">
        <img class="img-lg rounded border" src="{{$offer->image()->first()->getUrl()}}" alt="" name="offer_img" id="offer_img">
    </a>
    @else
    <a href="{{URL::to('offerDetailes/'.$offer->id)}}" class="d-block mb-0" name="offer_detail" id="offer_detail">
     <img class="img-lg rounded border" src="/front_assets/images/products/anonymous-offer.jpg" alt="" name="offer_img" id="offer_img">
 </a>
 @endif
 <div class="wrapper w-100 ml-2">
    <a href="{{URL::to('offerDetailes/'.$offer->id)}}" class="d-block mb-0" name="offer_detail" id="offer_detail">
        <h5 class="mb-0"><b>{{$offer->title}}</b></h5>
    </a>
    <p class="mb-0"><small class="text-muted">$ {{$offer->price}}</small></p>
    <p class="mb-0"><small class="text-muted">15 Post, 154 Following</small></p>
    <div class="d-flex justify-content-between align-items-center">
        <div class="d-flex align-items-center">
            <span class="badge badge-success">Available</span>
        </div>
    </div>
</div>
</div>
<!-- /offers list item -->
@endforeach