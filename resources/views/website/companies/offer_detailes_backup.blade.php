@extends('front.main')
@section('header_after')
 <link rel="stylesheet" href="/front_assets/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css">
@endsection
@section('content')
<!--/navbar header for mobile-->
<!-- partial -->
 <div class="content-wrapper pl-0 pt-0 pr-md-0 pr-sm-0 pr-xs-0">
            <div class="container-fluid pl-0 pr-md-0 pr-sm-0 pr-xs-0">
                <div class="row">
                    <!-- Page Content -->
                    <div class="col-lg-9 col-md-12 col-sm-12 pr-4 pr-md-0 pr-sm-0 pr-xs-0">
                        <div class="container pr-0 pl-0 pr-md-0 pr-sm-0 pr-xs-0">
                            <img src="/front_assets/images/brands/cover-1.jpg" class="main-img container px-0" alt="">
                        </div>
                        <div class="container border-right">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-9">
                                    <div class="profile-img ml-0 mx-xs-auto">
                                        @if($image->first())
                                        <img src="{{ $image->first()->getUrl() }}" class="main-img container px-0" alt="">
                                        @else
                                        <img src="/front_assets/images/brands/udacity.png" class="main-img container px-0" alt="">
                                        @endif
                                        
                                    </div>
                                    <div class="d-flex mt--3 d-xs-block text-xs-center">
                                        <div class="mr-auto">
                                            <h3 class="d-block my-2">{{$offer->title}} 
                                                
                                                @if( (\Carbon\Carbon::now())->format('d-m-Y') >= $offer->to->format('d-m-Y') )
                                                    
                                                    <span class="badge badge-danger">Not Available</span>
                                                @else
                                                    <span class="badge badge-success">Available</span>
                                                @endif
                                            </h3>
                                            <div class="d-block">
                                                <small class="d-inline">Posted <span class=" text-muted">{{$offer->duration}}</span></small>
                                                <small class="ml-3 d-inline">Number of views <span class=" text-muted">82 views</span></small>
                                                <small class="ml-3 d-inline">Price <span class=" text-muted">{{$offer->price}}</span></small>
                                                
                                                <small class="ml-0 d-block">From <span class=" text-muted">{{$offer->from->format('d-m-Y')}}</span></small>
                                                <small class="ml-0 d-inline">To <span class=" text-muted">{{$offer->to->format('d-m-Y')}}</span></small>

                                                <small class="ml-0 d-block">Minimum Of peices <span class=" text-muted">{{$offer->minimum_of_entities}}</span></small>

                                                <h4 class="ml-0 d-block">Data sheet <span class=" text-muted"></span></h4>                                            </div>
                                            <ul class="ml-2">
                                                
                                                @foreach($options as $material)
                                                    <li>{{$material['keys']}} <p class="ml-2">{{$material['values']}}</p></li>
                                                @endforeach
                                                
                                            </ul>
                                                
                                            <div class="d-block">
                                                <small class="d-inline">{{$offer->company->name}}</small>
                                                <small class="ml-3 d-inline">Company Location <span>Egypt</span></small>
                                            </div>
                                        </div>
                                        <div class="ml-auto">
                                            <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5">Buy Now</button>
                                        </div>
                                    </div>
                                    <div class="content my-4">
                                        <h4>Offer Description</h4>
                                        <div class="text-muted">
                                            {!! $offer->description !!}
                                        </div>
                                        <h4>Offer Tags</h4>
                                        <div class="tags">
                                        	@foreach($offer->tags as $tag)
                                            	<a class="badge badge-primary badge-pill tag-item" href="#">{{$tag->name}}</a>
                                            @endforeach
                                            
                                        </div>
                                    </div>
                                </div>
                                @include('website.reviews.add-review')
                                @include('website.reviews.list_offers')

                            </div>
                        </div>
                    </div>
                    <!-- /Page Content -->
                    <!-- Right Sidebar -->
                    <div class="col-lg-3 col-md-12 col-sm-12 px-4 pt-5">
                        <!-- wedget -->
                        @include('website.partials.you_may_like')
                        <!-- /wedget -->
                        <hr class="p-2">
                        <!-- wedget -->
                        <div class="wedget">
                            <div class="row">
                                <h6 class="mb-2 col text-muted">APPS WE USE TO STAY CONNECTED</h6>
                            </div>
                            <div class="row">
                                <div class="social-icons text-center">
                                    <a href="#"><i class="fa fa-phone bg-light"></i></a>
                                    <a href="#"><i class="fa fa-envelope bg-light"></i></a>
                                    <a href="#"><i class="fa fa-skype bg-light"></i></a>
                                    <a href="#"><i class="fa fa-slack bg-light"></i></a>
                                </div>
                            </div>
                        </div>
                        <!-- /wedget -->
                    </div>
                    <!-- /Right Sidebar -->
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <div class="content-wrapper bg-gray">
            <div class="container">
                <!--Companies list-->
                <div class="row companies-list">
                    <div class="col-12 p-3">
                        <h4 class="mb-2">
                            YOU MAY LIKE
                        </h4>
                    </div>
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                <img  class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                                          </a>
                                <h6 class="d-block my-2">Amazone</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                                          </a>
                                <h6 class="d-block my-2">Dove</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                                  </a>
                                <h6 class="d-block my-2">Servicam</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                                          </a>
                                <h6 class="d-block my-2">Amazone</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                                          </a>
                                <h6 class="d-block my-2">Dove</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                    <!--company-list-item-->
                    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                        <div class="card text-center bg-gray">
                            <div class="avatar">
                                <a href="#">
                                              <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                                          </a>
                                <h6 class="d-block my-2">Servicam</h6>
                                <small class="d-block text-muted">Food, Pizza</small>
                                <small class="d-block text-muted">15 Post, 154 Following</small>
                            </div>
                        </div>
                    </div>
                    <!--/company-list-item-->
                </div>
                <!--/Companies list-->
            </div>
        </div>
        <!-- page-body-wrapper ends -->
@endsection

@section('footer_after')
<script src="/front_assets/plugins/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
@endsection