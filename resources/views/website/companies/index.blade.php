@extends('front.main')
@section('content')
<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <!-- Page Content -->
                    <div class="col-lg-12 col-md-12 col-sm-12 px-xs-0">
                        <div class="container px-xs-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="browse">
                                        <h6 class="mb-3 ml-2">BROWSE COMPANIES</h6>
                                        <form action="#" method="GET">
                                            <div class="row">
                                                <div class="col-5 input-group find-group">
                                                    <div class="input-group-prepend text-center">
                                                        <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control border-0 search-top" placeholder="Search..." aria-label="Username" name="search_key_word" aria-describedby="basic-addon1">
                                                </div>
                                                <div class="col-2 input-group find-group category-select mx-2">
                                                    <select class="form-control js-example-basic-single">
                                                          <option value=""></option>
                                                           @foreach(\App\Country::all() as $country)
                                                            <option value="{{$country->code}}">{{$country->name}}</option>
                                                           @endforeach   
                                                    </select>
                                                </div>
                                                <div class="col-2 input-group find-group category-select">
                                                    <select class="form-control js-example-basic-single-3">
                                                           <option value=""></option>  
                                                           <option value="asd">All Categories</option>
                                                           <option value="dsa">category1</option>
                                                    </select>
                                                </div>
                                                <div class="col-2 text-right ml-auto">
                                                    <input type="submit" class="btn btn-primary btn-rounded border-0 btn-lg btn-gred" value="Find Company" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p class="m-2"><cite class="text-muted" id="Companies_count">{{count($companies)}}</cite> <i>Companies from </i> <cite class="text-muted">{{ $companiescount }}</cite></p>
                                </div>
                            </div>
                            <div class="row jobs-list my-2">
                            @foreach($companies as $company)
                                <!-- companies list item -->
                                <div class="list d-flex align-items-center col-lg-3 col-md-4 col-sm-6 my-3 company">
                                    @if($company->profile())
                                        <a href="/profile/{{$company->id}}/{{$company->name}}" class="d-block mb-0">
                                            <img class="img-lg rounded border" src="{{URL::to($company->profile()->getUrl())}}" alt="">
                                        </a>
                                    @else
                                        <a href="/profile/{{$company->id}}/{{$company->name}}" class="d-block mb-0">
                                            <img class="img-lg rounded border" src="/front_assets/images/brands/anonymous-company.png" alt="">
                                        </a>
                                    @endif
                                    
                                    <div class="wrapper w-100 ml-2 pl-0">
                                        <a href="/profile/{{$company->id}}/{{$company->name}}" class="d-block mb-0">
                                            <h5 class="mb-0"><b>{{$company->name}}</b></h5>
                                        </a>
                                        
                                        <p class="mb-0"><small class="text-muted">{{$company->business_user_info->address}}</small></p>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex align-items-center">
                                            @if(\Auth::check())
                                            <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more followbtn" company-id="{{$company->id}}">{{\Auth::user()->checkfollow($company->id)?'UnFollow':'Follow'}}</button>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /companies list item -->
                            @endforeach
                            </div>
                            <!-- Load more -->
                            <div class="my-5">
                                <div class="clearfix"></div>
                                <div class="container text-center">
                                    <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred" id="btn-more" DataID="{{$company->id}}" > Load more</button>
                                </div>
                            </div>
                            <!-- /Load more -->
                        </div>
                    </div>
                    <!-- /Page Content -->
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- row-offcanvas ends -->
</div>

@endsection

@section('footer_after')
<script>
    $(document).ready(function(){
        $(document).on('click','#btn-more',function(){
            var id = $(this).attr('DataID');
            $("#btn-more").html("<i class='fa fa-spinner fa-pulse'></i>Loading....");
            $.ajax({
                url : 'companies/loaddata',
                method : "POST",
                data : {id:id, _token:"{{csrf_token()}}"},
                dataType : "text",
                success : function (data)
                {

                    if(data != '') 
                    {

                        $('.jobs-list').append(data);
                        $("#btn-more").html(" load more");
                        id = parseInt(id) + 16;
                        $("#btn-more").attr('DataID',id);
                        $("#Companies_count").html($(".company").length);
                    }
                    else
                    {
                        $('#btn-more').html("No Data");
                    }
                }
            });
        });  
    }); 
</script>
@endsection