@extends('front.main')
@section('content')
        
<nav class="container-fluid bg-gray p-2">
    <div class="container">
        <div class="row">
            <div class="d-flex col-lg-7 col-md-8 col-sm-12 text-md-left text-sm-center text-xs-center">
                <h4 class="d-flex p-1 mb-0">
                    {{$company->name}}
                    <small class="d-flex text-muted ml-3 my-auto">
                        <div id="connection" class="m-1">
                            <span id="connections">163</span> Connections
                        </div>
                        <i> . </i>
                        <div id="connection" class="m-1">
                            <span id="followers">{{count($company->follower)}}</span> Followers
                        </div>
                        <i> . </i>
                        <div id="connection" class="m-1">
                           Following <span id="followers">{{count($company->following)}}</span>
                        </div>                                        
                    </small>
                </h4>
            </div>
            <div class="col-lg-2 col-md-4 col-sm-12">
                <div class="text-lg-right text-md-right text-sm-center text-xs-center my-xs-2">
                @if(\Auth::check())
                    <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5 followbtn" company-id="{{$company->id}}">{{\Auth::user()->checkfollow($company->id)?'UnFollow':'Follow'}}</button>
                @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
                <div class="input-group float-right">
                    <div class="input-group-prepend text-center">
                        <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>
    </div>
</nav>
<div class="profile-about">
    <div class="cover-img" style="background-image: url(/front_assets/images/brands/anonymous-cover.jpg)"></div>
    <div class="profile-img">
        <img src="/front_assets/images/brands/anonymous-company.png" class="rounded-circle" alt="image">
    </div>
    <div class="container mt--3">
        <div class="row text-center">
            <div class="col-lg order-lg-1 order-md-2">
                <div class="social-icons text-center">
                    <a href="#"><i class="fa fa-facebook bg-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter bg-twitter"></i></a>
                    <a href="#"><i class="fa fa-linkedin bg-linkedin"></i></a>
                    <a href="#"><i class="fa fa-behance bg-behance"></i></a>
                    <a href="#"><i class="fa fa-dribbble bg-dribbble"></i></a>
                </div>
                <div class="text-center mt-3">
                    <span class="d-block text-muted my-2">
                      <i class="fa fa-globe"></i>
                      <span>www.udacity.com</span>
                    </span>
                    <span class="d-block text-muted my-2">
                      <i class="fa fa-map-marker text-gray"></i>
                      <span>{{$company->business_user_info->address}}</span>
                    </span>
                </div>
            </div>
            <div class="col-lg-5 order-lg-2 order-md-1">
            <p class="text-muted">{{$company->name}} is {{$company->business_user_info->vission}} {{$company->business_user_info->mission}}</p>
                <div class="d-flex justify-content-between align-items-center px-4 py-3">
                    <div class="mr-auto">
                        <a href="{{URL::to('/Alloffers/'.$company->id)}}" class="btn btn-light btn-rounded btn-fw px-lg-4">Show All Offers</a>
                    </div>
                    <div class="ml-auto">
                        <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-lg-5">Hire me</button>
                    </div>
                </div>
            </div>
            <div class="col-lg order-lg-3 order-md-3">
                <div class="tags">
                    @foreach ($company->tags as $tag)
                        <a class="badge badge-primary badge-pill tag-item" href="#">{{$tag->name}}<span></span></a>
                    @endforeach
                </div>
            </div>
        </div>
        <hr>
    </div>
</div>
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <!-- Page Content -->
            <div class="col-lg-9 col-md-12 col-sm-12 pr-4 px-xs-0">
                <div class="container pr-4  px-xs-0">
                    <!-- Newsfeeds -->
                    <div class="mb-5">
                        <!-- Post -->
                        @foreach ($company->posts as $post)
                            <div class="newsfeed border-bottom pb-2 my-4">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-12 text-sm-center text-xs-center">
                                        <a href="#">
                                        <img class="img-lg rounded border" src="/front_assets//images/Brands/servicam.jpg" alt="">
                                    </a>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-12">
                                    <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 text-xs-center">{{$post->user->name}}</h6>
                                    <p class="text-muted text-xs-center">Since {{$post->duration}} </p>
                                    <p class="my-2">{{$post->body}}</p>
                                </div>
                            </div>
                            <div class="container newsfeed-footer d-flex justify-content-between align-items-center pt-5">
                                <div class="d-flex align-items-center mr-auto">
                                    <a class="like-btn" href="#/" PostId="{{$post->id}}">
                                        <span class="text-muted mr-1"><i class="fa fa-heart" style="color:{{$post->liked()?'#7e4cf9':''}};"></i></span>
                                        <span class="text-muted">{{$post->likeCount}} Likes</span>
                                    </a>
                                </div>
                                {{-- <div class="d-flex align-items-center ml-auto mr-3">
                                    <a class="share-btn" href="#/" PostId="{{$post->id}}" PostId="{{$post->id}}" data-toggle="modal" data-target="#share_post" data-id="{{$post->id}}" data-body="{{$post->body}}" data-user="{{$post->user->name}}" data-date="{{$post->duration}}">
                                        <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
                                        <span class="text-muted"> Share</span>
                                    </a>
                                </div> --}}
                                <div class="d-flex align-items-center">
                                    <a class="comment-btn" href="#">
                                        <span class="text-muted mr-1"><i class="fa fa-comments"></i></span>
                                        <span class="text-muted">{{$post->comments->count()}} Comments</span>
                                    </a>
                                        
                                </div>
                            </div>
                        @endforeach
                        <!-- /Post -->
                        <div class="clearfix"></div>
                        <!-- load more -->
                        <div class="container text-center">
                            <a href="#" class="load-more mx-auto">
                                <span>Load More Post</span>
                                <i class="fa fa-ellipsis-h"></i>
                              </a>
                        </div>
                    </div>
                    <!-- /Newsfeeds -->
                </div>
            </div>
            <!-- /Page Content -->
            <!-- Right Sidebar -->
            <div class="right-sidebar col-lg-3 col-md-12 col-sm-12 pl-4 px-xs-0">
                <!-- wedget -->
                <div class="wedget">
                    <div class="row">
                        <h5 class="mb-2 col">JOBS LIST</h5>
                        <div class="dropdown show">
                            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <h6 class="dropdown-header">Settings</h6>
                                <a class="dropdown-item" href="#">Action</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col jobs-list ml-2">
                            <!-- jobs list item -->
                            <div class="list d-flex align-items-center pl-2 pt-3">
                                <div class="wrapper w-100 ml-3">
                                    <a href="#" class="d-block mb-0"><b>New Approaches to Swarm...</b></a>
                                    <small class="text-muted">Giza Company</small>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0">Front End Developer</p>
                                        </div>
                                        <small class="text-muted ml-auto">30-60 USD</small>
                                    </div>
                                </div>
                            </div>
                            <!-- /jobs list item -->
                            <!-- jobs list item -->
                            <div class="list d-flex align-items-center pl-2 pt-3">
                                <div class="wrapper w-100 ml-3">
                                    <a href="#" class="d-block mb-0"><b>Helping people finding the best...</b></a>
                                    <small class="text-muted">Mobarak Company</small>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0">UI & UX Designer</p>
                                        </div>
                                        <small class="text-muted ml-auto">30-60 USD</small>
                                    </div>
                                </div>
                            </div>
                            <!-- /jobs list item -->
                            <!-- jobs list item -->
                            <div class="list d-flex align-items-center pl-2 pt-3">
                                <div class="wrapper w-100 ml-3">
                                    <a href="#" class="d-block mb-0"><b>New Approaches to Swarm...</b></a>
                                    <small class="text-muted">Giza Company</small>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0">Front End Developer</p>
                                        </div>
                                        <small class="text-muted ml-auto">30-60 USD</small>
                                    </div>
                                </div>
                            </div>
                            <!-- /jobs list item -->
                        </div>
                    </div>
                </div>
                <!-- /wedget -->
                <hr class="p-2">
                <!-- wedget -->
                <div class="wedget">
                    <div class="row">
                        <h5 class="mb-2 col">NEW OFFERS</h5>
                        <div class="dropdown show">
                            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                                <h6 class="dropdown-header">Settings</h6>
                                <a class="dropdown-item" href="#">Action</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="jobs-list ml-2">
                            <!-- jobs list item -->
                            <div class="list d-flex align-items-center pl-2 pt-3">
                                <img class="img-md rounded border" src="images/brands/Natural-Language-Processing-Definition.jpg" alt="">
                                <div class="wrapper w-100 ml-1">
                                    <a href="#" class="d-block mb-0"><b>Natural Language Processing</b></a>
                                    <small class="text-muted">Price EGP 14000</small>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0">Available in English</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /jobs list item -->
                            <!-- jobs list item -->
                            <div class="list d-flex align-items-center pl-2 pt-3">
                                <img class="img-md rounded border" src="images/brands/Natural-Language-Processing-Definition.jpg" alt="">
                                <div class="wrapper w-100 ml-1">
                                    <a href="#" class="d-block mb-0"><b>Natural Language Processing</b></a>
                                    <small class="text-muted">Price EGP 14000</small>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0">Available in English</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /jobs list item -->
                            <!-- jobs list item -->
                            <div class="list d-flex align-items-center pl-2 pt-3">
                                <img class="img-md rounded border" src="images/brands/Natural-Language-Processing-Definition.jpg" alt="">
                                <div class="wrapper w-100 ml-1">
                                    <a href="#" class="d-block mb-0"><b>Natural Language Processing</b></a>
                                    <small class="text-muted">Price EGP 14000</small>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="d-flex align-items-center">
                                            <p class="mb-0">Available in English</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /jobs list item -->
                        </div>
                    </div>
                </div>
                <!-- /wedget -->
            </div>
            <!-- /Right Sidebar -->
        </div>
    </div>
</div>
<div class="content-wrapper bg-gray">
    <div class="container">
        <!--Companies list-->
        <div class="row companies-list">
            <div class="col-12 p-3">
                <h4 class="mb-2">
                    YOU MAY LIKE
                </h4>
            </div>
            <!--company-list-item-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                <div class="card text-center bg-gray">
                    <div class="avatar">
                        <a href="#">
                              <img  class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                          </a>
                        <h6 class="d-block my-2">Amazone</h6>
                        <small class="d-block text-muted">Food, Pizza</small>
                        <small class="d-block text-muted">15 Post, 154 Following</small>
                    </div>
                </div>
            </div>
            <!--/company-list-item-->
            <!--company-list-item-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                <div class="card text-center bg-gray">
                    <div class="avatar">
                        <a href="#">
                              <img  class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                          </a>
                        <h6 class="d-block my-2">Dove</h6>
                        <small class="d-block text-muted">Food, Pizza</small>
                        <small class="d-block text-muted">15 Post, 154 Following</small>
                    </div>
                </div>
            </div>
            <!--/company-list-item-->
            <!--company-list-item-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                <div class="card text-center bg-gray">
                    <div class="avatar">
                        <a href="#">
                      <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                  </a>
                        <h6 class="d-block my-2">Servicam</h6>
                        <small class="d-block text-muted">Food, Pizza</small>
                        <small class="d-block text-muted">15 Post, 154 Following</small>
                    </div>
                </div>
            </div>
            <!--/company-list-item-->
            <!--company-list-item-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                <div class="card text-center bg-gray">
                    <div class="avatar">
                        <a href="#">
                      <img class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                  </a>
                        <h6 class="d-block my-2">Amazone</h6>
                        <small class="d-block text-muted">Food, Pizza</small>
                        <small class="d-block text-muted">15 Post, 154 Following</small>
                    </div>
                </div>
            </div>
            <!--/company-list-item-->
            <!--company-list-item-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                <div class="card text-center bg-gray">
                    <div class="avatar">
                        <a href="#">
                      <img class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                  </a>
                        <h6 class="d-block my-2">Dove</h6>
                        <small class="d-block text-muted">Food, Pizza</small>
                        <small class="d-block text-muted">15 Post, 154 Following</small>
                    </div>
                </div>
            </div>
            <!--/company-list-item-->
            <!--company-list-item-->
            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                <div class="card text-center bg-gray">
                    <div class="avatar">
                        <a href="#">
                      <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                  </a>
                        <h6 class="d-block my-2">Servicam</h6>
                        <small class="d-block text-muted">Food, Pizza</small>
                        <small class="d-block text-muted">15 Post, 154 Following</small>
                    </div>
                </div>
            </div>
            <!--/company-list-item-->
        </div>
        <!--/Companies list-->
    </div>
</div>

@endsection