@foreach($companies as $company)
<!-- companies list item -->
<div class="list d-flex align-items-center col-lg-3 col-md-4 col-sm-6 my-3 company">
    @if($company->profile())
    <a href="/profile/{{$company->id}}/{{$company->name}}" class="d-block mb-0">
        <img class="img-lg rounded border" src="{{URL::to($company->profile()->getUrl())}}" alt="">
    </a>
    @else
    <a href="/profile/{{$company->id}}/{{$company->name}}" class="d-block mb-0">
        <img class="img-lg rounded border" src="/front_assets/images/brands/anonymous-company.png" alt="">
    </a>
    @endif

    <div class="wrapper w-100 ml-2">
        <a href="/profile/{{$company->id}}/{{$company->name}}" class="d-block mb-0">
            <h5 class="mb-0"><b>{{$company->name}}</b></h5>
        </a>
        <p class="mb-0"><small class="text-muted">15 Post, 154 Following</small></p>
        <p class="mb-0"><small class="text-muted">{{$company->business_user_info->address}}</small></p>
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-flex align-items-center">
                @if(\Auth::check() && \Auth::user()->id != $company->id)
                    <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more followbtn" company-id="{{$company->id}}">{{\Auth::user()->checkfollow($company->id)?'UnFollow':'Follow'}}</button>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- /companies list item -->
@endforeach