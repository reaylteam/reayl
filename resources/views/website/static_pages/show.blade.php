@extends('front.main')
@section('content')
<!-- partial -->
<div class="content-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="faq-section">
            <div class="container-fluid bg-success py-2">
              <p class="mb-0 text-white">{{$page->name}}</p>
            </div>
            <div id="accordion" role="tablist" aria-multiselectable="true" class="accordion">
                <div class="card">
                    <div class="card-header" role="tab" id="headingOne">
                        <h5 class="mb-0">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {{$page->slug}}
                            </a>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                        <div class="card-body">
                            {{$page->content}}
                        </div>
                    </div>
                </div>
            
            </div>
          </div>
          
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->

@endsection