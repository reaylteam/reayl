    <div class="newsfeed border-bottom pb-2 my-4">
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-12 text-sm-center text-xs-center">
                <a href="{{URL::to('/showProfile/'.$post->user->level.'/'.$post->user->id.'/'.$post->user->name)}}" name="user-profile-link">
                @if($post->user->profile())
                    <img class="img-lg rounded border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="" name="user-profile-image">
                @else
                    <img class="img-lg rounded border" src="/front_assets/images/faces/anonymous-user.png" name="user-profile-image" alt="">
                @endif
            </a>
        </div>
        <div class="col-lg-10 col-md-10 col-sm-12">
            <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 text-xs-center" name="user-name">{{$post->user->name}}</h6>
            <p class="text-muted text-xs-center" name="post-duration">Since {{$post->duration}} </p>
            <p class="my-2" name="post-body"> {{$post->body}}</p>
        </div>
    </div>
    <div class="container newsfeed-footer d-flex justify-content-between align-items-center pt-5">
        <div class="d-flex align-items-center mr-auto">
            <a class="like-btn" href="#/" PostId="{{$post->id}}" name="like-link-action">
                @if(Auth::check())
                <span class="text-muted mr-1" name="span-container-of-like-shpe"><i class="fa fa-heart" name="like-heart-shape" style="color:{{  $post->liked()?'#7e4cf9':'' }};"></i></span>
                @else
                    <span class="text-muted mr-1" name="span-container-of-like-shpe" data-toggle="modal" data-target="#sign_in">
                            <i class="fa fa-heart" name="like-heart-shape" style="color:{{  $post->liked()?'#7e4cf9':'' }};"></i>
                    </span>
                @endif    
                <span class="text-muted" name="post-like">{{$post->likeCount}} Likes</span>
            </a>
        </div>
        <div class="d-flex align-items-center ml-auto mr-3">
                {{-- @if(Auth::check())
                    <a class="share-btn" href="#/" PostId="{{$post->id}}" PostId="{{$post->id}}" data-toggle="modal" data-target="#share_post" data-id="{{$post->id}}" data-body="{{$post->body}}" data-user="{{$post->user->name}}" data-date="{{$post->duration}}">
                        <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
                        
                            <span class="text-muted" name="share-text"> Share</span>
                        
                    </a>
                @else
                    <a class="share-btn" href="#">
                        <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
                        
                        <span class="text-muted" name="share-text" data-toggle="modal" data-target="#sign_in">
                                Share
                        </span>
                        
                    </a>
                    
                @endif --}}
        </div>
        <div class="d-flex align-items-center">
            <a class="comment-btn" href="#" name="comment-button-action">
                <span class="text-muted mr-1" name="span-container-of-comment-shape"><i class="fa fa-comments" name="comment-shape"></i></span>
                <span class="text-muted" name="post-comments-count">{{$post->comments->count()}} Comments</span>
            </a>
                
        </div>
    </div>
    <!--post comments-->
    <div class="container border-top px-2">
        <form class="create-post-form">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 my-sm-2 my-xs-2">
                    <a href="#">
                        @if($post->user->profile())
                        <img class="img-sm rounded-circle border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                        @else
                        <img class="img-sm rounded-circle border" src="/front_assets/images/faces/face1.jpg" alt="">
                        @endif   
                    </a>
                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 my-auto">
                    <div class="input-group find-group float-right">
                        <input type="text" class="form-control border-0 search-top" id="postContent"  placeholder="Write a comment..." aria-label="Username" aria-describedby="basic-addon1">
                        @if(Auth::check())
                            <input type="submit" class="btn btn-primary btn-rounded border-0 btn-xs btn-gred comment_event" name="comment-btn" value="COMMENT" postID="{{$post->id}}" modelName="\App\Post">
                        @else
                            <span class="btn btn-primary btn-rounded border-0 btn-xs btn-gred" name="comment-btn" data-toggle="modal" data-target="#sign_in">
                                COMMENT
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </form>
        <!--list of comments-->
        <ul class="bullet-line-list" name="list-of-comments">
            @foreach($post->comments as $comment)
                <li  name="comment">
                    <div class="d-flex align-items-center border-bottom py-1">
                        @if($comment->user->profile())
                            <img class="img-sm rounded-circle" src="{{URL::to($comment->user->profile()->getUrl())}}" name="user-profile-image" alt="">
                        @else
                            <img class="img-sm rounded-circle" src="/front_assets/images/faces/face1.jpg" name="user-profile-image" alt="">
                        @endif 
                        <div class="wrapper w-100 ml-3">
                            <span class="mr-auto" name="user-name"><b>{{$comment->user->name}} </b></span>
                            <small class="text-muted ml-2" name="comment-duration">{{$comment->duration}}</small>
                            <p class="mb-0" name="comment-body">{{$comment->content}}</p>

                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
        <!--/list of comments-->
    </div>
</div>
