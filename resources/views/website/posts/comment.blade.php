<li>
	<div class="d-flex align-items-center border-bottom py-1">
		@if($comment->user->profile())
		    <img class="img-sm rounded-circle" src="{{URL::to($comment->user->profile()->getUrl())}}" alt="" name="user-profile_img">
		@else
		    <img class="img-sm rounded-circle" src="/front_assets/images/faces/anonymous-user.png" alt="" name="default-profile_img">
		@endif
		<div class="wrapper w-100 ml-3">
			<span class="mr-auto"><b name="username_txt">{{$comment->user->name}} </b></span>
			<small class="text-muted ml-2" name="comment-duration_txt">{{$comment->duration}}</small>
			<p class="mb-0" name="comment-content_txt">{{$comment->content}}</p>

		</div>
	</div>
</li>