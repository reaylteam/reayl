<div class="newsfeed border-bottom pb-2 my-4">
    <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-12 text-sm-center text-xs-center">
            <a href="#">
                @if($post->user->profile())
                    <img class="img-lg rounded border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                @else
                    <img class="img-lg rounded border" src="/front_assets/images/brands/anonymous-company.png" alt="">
                @endif
              
          </a>
      </div>
      <div class="col-lg-10 col-md-10 col-sm-12">
        <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 text-xs-center">{{$post->user->name}}</h6>
        <p class="text-muted text-xs-center">Since {{$post->duration}} </p>
        <p class="my-2">{{$post->body}}</p>
    </div>
</div>
<div class="container newsfeed-footer d-flex justify-content-between align-items-center pt-5">
    <div class="d-flex align-items-center mr-auto">
        <a class="like-btn" href="#/" PostId="{{$post->id}}">
            <span class="text-muted mr-1"><i class="fa fa-heart" ></i></span>
            <span class="text-muted" >{{$post->likeCount}} Likes</span>
        </a>
    </div>
    <div class="d-flex align-items-center ml-auto mr-3">
        {{-- <a class="share-btn" href="#/" PostId="{{$post->id}}" data-toggle="modal" data-target="#share_post">
            <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
            <span class="text-muted"> Shares</span>
        </a> --}}
    </div>
    <div class="d-flex align-items-center">
        <a class="comment-btn" href="#">
            <span class="text-muted mr-1"><i class="fa fa-comments"></i></span>
            <span class="text-muted">{{$post->comments->count()}} Comments</span>
        </a>
    </div>
</div>
</div>