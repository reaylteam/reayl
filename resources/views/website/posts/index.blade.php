@extends('front.main')
@section('header_after')
<style>
#ajaxSpinnerContainer {height:11px;}
#ajaxSpinnerImage {display:none;}
</style>
<link rel="stylesheet" type="text/css" href="/front_assets/comments/css/jquery-comments.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

@endsection
@section('content')

<div class="content-wrapper">
<div class="container">
    <div class="row">
        <!-- Page Content -->
        <div class="col-lg-12 col-md-12 col-sm-12 px-xs-0">
            <div class="container px-xs-0">
<!--
                <div class="row">
                    <div class="col-lg-12">
                        <div class="create-post">
                            <form class="create-post-form">
                                <textarea class="form-control" rows="4" placeholder="Write your post..." data-emojiable="true" data-emoji-input="unicode"></textarea>
                                <input id="upload_image_input" type="file" class="dropify" data-max-file-size="1000kb" />
                                <div class="d-flex justify-content-between align-items-center p-3">
                                    <div class="mr-auto">
                                        <a id="upload_image_icon" class="add-img pr-2" href="#"><i class="fa fa-photo"></i></a>
                                    </div>
                                    <div class="ml-auto">
                                        <input type="submit" class="btn btn-primary btn-rounded border-0 btn-fw btn-gred" value="POST NOW" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
-->
                <!-- posts -->
                <div class="my-5">
                    @foreach($posts as $post)
                    <!-- Post -->
                        @include('website.posts.post_content')
                    <!-- /Post -->
                    @endforeach
                    <div class="clearfix"></div>
                    
                </div>
                <!-- /posts -->
                <!-- load more posts -->
                <div class="container text-center">
                    <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred" id="btn-more" DataID="{{$post->id}}" > Load more</button>
                </div>
                <!-- load more posts -->
            </div>
        </div>
        <!-- /Page Content -->
    </div>
</div>
</div>
@endsection

@include('website.posts.modal_share')
@section('footer_after')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.textcomplete/1.8.0/jquery.textcomplete.js"></script>
<script type="text/javascript" src="/front_assets/comments/js/jquery-comments.js"></script>
<script>
    $(document).ready(function(){
        $(document).on('click','#btn-more',function(){
            var id = $(this).attr('DataID');
            $("#btn-more").html("<i class='fa fa-spinner fa-pulse'></i>Loading....");
            $.ajax({
                url : 'posts/loaddata',
                method : "POST",
                data : {id:id, _token:"{{csrf_token()}}"},
                dataType : "text",
                success : function (data)
                {

                    if(data != '') 
                    {

                        $('.my-5').append(data);
                        $("#btn-more").html(" load more");
                        id = parseInt(id) + 16;
                        $("#btn-more").attr('DataID',id);
                        $("#Companies_count").html($(".company").length);
                    }
                    else
                    {
                        $('#btn-more').html("No Data");
                    }
                }
            });
        });  
    }); 
</script>
<script>
    $(document).ready(function(){
        $(document).on('click','#comments-btn-more',function(){
            var id        = $(this).attr('DataID');
            var comment_id= $(this).attr('CommentID');
            $("#btn-more").html("<i class='fa fa-spinner fa-pulse'></i>Loading....");
            $.ajax({
                url : '/post/comments/loaddata',
                method : "POST",
                data : {id:id, _token:"{{csrf_token()}}",comment_id:comment_id},
                dataType : "text",
                success : function (data)
                {

                    if(data != '') 
                    {

                        $('#comment_list_'+id).append(data);
                        $("#btn-more").html(" load more");
                        id = parseInt(id) + 16;
                        $("#comments-btn-more").attr('CommentID',id);
                        $("#Companies_count").html($(".company").length);
                    }
                    else
                    {
                        $('#btn-more').html("No Data");
                    }
                }
            });
        });  
    }); 
</script>
@endsection