<!-- Share post Modal start -->
<div class="modal fade" id="share_post" tabindex="-1" role="dialog" aria-labelledby="share_post" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="font-weight-light text-center mb-2">SHARE POST</h3>
                <ul class="bullet-line-list">
                    <form class="create-post-form">
                        <li>
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 my-sm-2 my-xs-2">
                                    <a href="#">
                                      <img class="img-md rounded border" src="/front_assets/images/faces/face1.jpg" alt="">
                                    </a>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                    <textarea class="form-control modal-body" rows="4" placeholder="Write your post..." ></textarea>
                                </div>
                            </div>
                        </li>
                        <hr>
                        <li>
                            <!-- Post -->
                            <div class="newsfeed my-2">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-12">
                                        <a href="#">
                                          <img class="img-md rounded border" src="/front_assets/images/faces/face1.jpg" alt="">
                                        </a>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-12">
                                        <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 user_name" >Servicam</h6>
                                        <p class="text-muted post_date">Since 3 Dayes</p>
                                        <p class="my-2 bost_content">MIT graduate with extensive experience in finance and technology. Passionate about exciting consumer electronics and internet startups around the globe .</p>
                                        {{-- <img class="container rounded border px-0" src="/images/products/camera.jpg" alt=""> --}}
                                    </div>
                                </div>
                            </div>
                            <!-- /Post -->
                            <div class="d-flex justify-content-between align-items-center p-3">
                                <div class="ml-auto">
                                    <button type="submit" class="btn btn-primary btn-rounded border-0 btn-fw btn-gred">Share</button>
                                </div>
                            </div>
                        </li>
                    </form>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Share post Modal Ends -->
