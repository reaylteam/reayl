    <div class="newsfeed border-bottom pb-2 my-4">
        <div class="row">
            <div class="col-lg-1 col-md-2 col-sm-12 text-sm-center text-xs-center">
                <a href="{{URL::to('/profile/'.$post->user->id.'/'.$post->user->name)}}">
                @if($post->user->profile())
                    <img class="img-lg rounded border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                @else
                    <img class="img-lg rounded border" src="/front_assets/images/brands/anonymous-company.png" alt="">
                @endif
            </a>
        </div>
        <div class="col-lg-11 col-md-10 col-sm-12 pl-4">
            <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 text-xs-center">{{$post->user->name}}</h6>
            <p class="text-muted text-xs-center">Since {{$post->duration}} </p>
            <p class="my-2">{{$post->body}}</p>
        </div>
    </div>
    <div class="container newsfeed-footer d-flex justify-content-between align-items-center pt-5">
        <div class="d-flex align-items-center mr-auto">
            <a class="like-btn" href="#/" PostId="{{$post->id}}">
                @if(Auth::check())
                    <span class="text-muted mr-1" name="span-container-of-like-shpe"><i class="fa fa-heart" name="like-heart-shape" style="color:{{  $post->liked()?'#7e4cf9':'' }};"></i></span>
                @else
                    <span class="text-muted mr-1" name="span-container-of-like-shpe" data-toggle="modal" data-target="#sign_in">
                            <i class="fa fa-heart" name="like-heart-shape" style="color:{{  $post->liked()?'#7e4cf9':'' }};"></i>
                    </span>
                @endif    
                <span class="text-muted">{{$post->likeCount}} Likes</span>
            </a>
        </div>
        <div class="d-flex align-items-center ml-auto mr-3">
            {{-- @if(Auth::check())
                <a class="share-btn" href="#/" PostId="{{$post->id}}" PostId="{{$post->id}}" data-toggle="modal" data-target="#share_post" data-id="{{$post->id}}" data-body="{{$post->body}}" data-user="{{$post->user->name}}" data-date="{{$post->duration}}">
                    <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
                    
                        <span class="text-muted" name="share-text"> Share</span>
                    
                </a>
            @else
                <a class="share-btn" href="#">
                    <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
                    
                    <span class="text-muted" name="share-text" data-toggle="modal" data-target="#sign_in">
                            Share
                    </span>
                    
                </a>
                
            @endif --}}
        </div>
        <div class="d-flex align-items-center">
            <a class="comment-btn" href="#">
                <span class="text-muted mr-1"><i class="fa fa-comments"></i></span>
                <span class="text-muted">{{$post->comments->count()}} Comments</span>
            </a>
                
        </div>
    </div>
    <!--post comments-->
    <div class="container border-top px-2">
        <form class="create-post-form">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 my-sm-2 my-xs-2">
                    <a href="#">
                        @if($post->user->profile())
                        <img class="img-sm rounded-circle border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                        @else
                        <img class="img-sm rounded-circle border" src="/front_assets/images/faces/anonymous-user.png" alt="">
                        @endif   
                    </a>
                </div>
                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 my-auto">
                    <div class="input-group find-group float-right">
                        <input type="text" class="form-control border-0 search-top comment-body-input" id="postContent"  placeholder="Write a comment..." aria-label="Username" aria-describedby="basic-addon1">
                        @if(Auth::check())
                            <input type="submit" class="btn btn-primary btn-rounded border-0 btn-xs btn-gred comment_event" name="comment-btn" value="COMMENT" postID="{{$post->id}}" modelName="\App\Post">
                        @else
                            <span class="btn btn-primary btn-rounded border-0 btn-xs btn-gred" name="comment-btn" data-toggle="modal" data-target="#sign_in">
                                COMMENT
                            </span>
                        @endif
                    </div>
                </div>
            </div>
        </form>
        <!--list of comments-->
        <ul class="bullet-line-list" id="comment_list_{{$post->id}}">
            @foreach($post->comments->take(5) as $comment)
                <li>
                    <div class="d-flex align-items-center border-bottom py-1">
                        @if($comment->user->profile())
                            <img class="img-sm rounded-circle" src="{{URL::to($comment->user->profile()->getUrl())}}" alt="">
                        @else
                            <img class="img-sm rounded-circle" src="/front_assets/images/faces/anonymous-user.png" alt="">
                        @endif 
                        <div class="wrapper w-100 ml-3">
                            <span class="mr-auto"><b>{{$comment->user->name}} </b></span>
                            <small class="text-muted ml-2">{{$comment->duration}}</small>
                            <p class="mb-0">{{$comment->content}}</p>

                        </div>
                    </div>
                </li>
            @endforeach
            <div class="clearfix"></div>
            <!-- load more comments -->
            
        </ul>
        @if($post->comments->count())
        <!--/list of comments-->
        <div class="container text-center mt-2">
                <button type="button" class="btn btn-primary btn-rounded btn-xs btn-gred" id="comments-btn-more" DataID="{{$post->id}}" CommentID="{{$comment->id}}"> load more comments</button>
        </div>
            <!-- load more comments -->
        @endif
    </div>
</div>
