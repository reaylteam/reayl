<div class="col-md-6 grid-margin stretch-card">
              <div class="card">
                <div class="card-body pb-0">
                  <h6 class="card-title">Reviews</h6>
                  <div class="row reviews">
                    @foreach($offer->reviews() as $review)
                      <div class="col-12">
                        <div class="wrapper border-bottom py-2">
                          <div class="d-flex">
                            @if($review->user->getMedia('profiles')->first())
                            
                              <img class="img-sm rounded-circle" src="{{ $review->user->getMedia('profiles')->first()->getUrl() }}" alt="">
                            @else
                              <img class="img-sm rounded-circle" src="/front_assets/images/faces/face1.jpg" alt="">
                            @endif
                            <div class="wrapper ml-4">
                              <p class="mb-0">{{$review->user->name}}</p>
                              <small class="text-muted mb-0">{{$review->comment}}</small>
                            </div>
                          <div class="rating ml-auto d-flex align-items-center">
                          <span >{{$review->rating}}</span>
                          </div>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>