<li id="user_{{ $review->user_id }}">
    <div class="d-flex align-items-center border-bottom py-1">
        @if($review->user->profile())
        <img class="img-sm rounded-circle" src="{{ $review->user->profile()->getUrl() }}" alt=""> @else
        <img class="img-sm rounded-circle" src="/front_assets/images/faces/anonymous-user.png" alt=""> @endif {{-- <img class="img-sm rounded-circle" src="http://127.0.0.1:8000/media/9/24143.png" alt=""> --}}

        <div class="wrapper w-100 ml-1 pl-0 user-review">
            <span class="mr-auto"><b>{{$review->user->name}} </b></span>
            <small class="text-muted ml-1">{{ $review->created_at->diffForHumans() }}</small>
            <select class="mx-auto user_rating" name="rating" >
                <option value="1" {{ ($review->rating >= 1)?'selected':'' }}>1</option>
                <option value="2" {{ ($review->rating >=2)?'selected':'' }}>2</option>
                <option value="3" {{ ($review->rating >=3)?'selected':'' }}>3</option>
                <option value="4" {{ ($review->rating >= 4)?'selected':'' }}>4</option>
                <option value="5" {{ ($review->rating ==5)?'selected':'' }}>5</option>
            </select>
            <p class="mb-0">{{$review->comment}}</p>
        </div>
    </div>
</li>