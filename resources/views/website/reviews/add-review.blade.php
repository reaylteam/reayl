<div class="col-md-6 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Review This offer</h4>
      <p class="card-description">
      </p>
      <form class="forms-sample" action="#">
        <div class="form-group">
          <label for="exampleTextarea1">Rate</label>
          <div class="rating ml-auto d-flex align-items-center">
            <select id="dashboard-rating-2" name="rating">
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="exampleTextarea1">comment</label>
          <textarea class="form-control" name="comment" id="comment" rows="2"></textarea>
        </div>
        {!! Form::hidden('rateable_type','App\Offer',['id'=>'rateable_type']) !!}
        {!! Form::hidden('item_id',$offer->id,['id'=>'item_id']) !!}
        <input type="submit" class="btn btn-success mr-2 review" />
        <button class="btn btn-light">Cancel</button>
      </form>
    </div>
  </div>
</div>