@section('header_after')
<link rel="stylesheet" type="text/css" href="/front_assets/css/statistics.css">
<link rel="stylesheet" href="/front_assets/node_modules/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="/front_assets/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" /> @endsection

{!! Form::model($offer,['route' => ['offers.update', $offer->id],'class'=>'forms-sample','enctype'=>'multipart/form-data','id'=>'edit_offer_form','files'=>'true','method' => 'patch']) !!}

  <div class="form-group">
      <label for="exampleInputName1">Offer Tittle</label> 
      {!! Form::text('title',$offer->title,['class'=>'form-control form-input','id'=>'title','required'=>'required']) !!}
      <span class="invalid-feedback" name="name-feedback">
          <strong name="title-error_txt">{{ $errors->first('title') }}</strong>
      </span>
  </div>
  <div class="row">
      <div class="form-group col-6 offer-select-category">
          <label for="offer_category">Offer Category</label>
          <select class="form-control js-example-basic-single-3 form-input" name="category_id" id="category_id" required>
              @foreach(\App\Category::all() as $category)
          <option value="{{ $category->id }}" {{($offer->category_id==$category->id)?'selected':'' }}>{{ $category->title }}</option>
              @endforeach
          </select>
          <span class="invalid-feedback" name="name-feedback">
              <strong name="category_id-error_txt">{{ $errors->first('category_id') }}</strong>
          </span>
      </div>

      <div class="form-group col-6">
          <label>Offer Country</label> 
          {!! Form::select('countries_id',\App\Country::all()->pluck('name','id'), $offer->countries_id, ['class' => 'form-control form-input','id'=>'countries_id' ,'required'=>'required']) !!}
          {{-- <small class="help-block"></small> --}}
          <span class="invalid-feedback" name="name-feedback">
              <strong name="countries_id-error_txt">{{ $errors->first('countries_id') }}</strong>
          </span>
      </div>

  </div>
  <div class="row">
      <div class="form-group col-6">
          <label>Price</label> {!! Form::number('price',$offer->price,['class'=>'form-control form-input','placeholder'=>'50$','id'=>'price','required'=>'required']) !!}
          {{-- <small class="help-block"></small> --}}
          <span class="invalid-feedback" name="name-feedback">
              <strong name="price-error_txt">{{ $errors->first('price') }}</strong>
          </span>
      </div>

      <div class="form-group col-6">
          <label>Minimum Number Of peices</label> {!! Form::number('minimum_of_entities',$offer->minimum_of_entities,['class'=>'form-control form-input','placeholder'=>'50$','id'=>'minimum_of_entities','required'=>'required']) !!}
          {{-- <small class="help-block"></small> --}}
          <span class="invalid-feedback" name="name-feedback">
              <strong name="minimum_of_entities-error_txt">{{ $errors->first('minimum_of_entities') }}</strong>
          </span>
      </div>

  </div>
  <div class="form-group">
      <label>Delivery Time</label>
      <div id="clockpicker" class="input-group clockpicker form-input p-0" data-placement="top" data-align="top">
      <input type="text" class="form-control" name="delivery_time" value="{{$offer->delivery_time}}" required>
          <span class="input-group-addon input-group-text">
              <i class="mdi mdi-clock"></i>
          </span>
          <span class="invalid-feedback" name="name-feedback">
              <strong name="from-error_txt">{{ $errors->first('delivery_time') }}</strong>
          </span>
         
      </div>
      
  </div>
  <div class="row">
      <div class="form-group col-6">
          <label>Starting From</label>
          <div id="datepicker-popup-start" class="input-group date datepicker form-input p-0">
              <input type="text" class="form-control" name="from" value="{{$offer->from->format('Y-m-d')}}" required>
              <div class="input-group-addon input-group-text">
                  <span class="mdi mdi-calendar"></span>
              </div>
              <span class="invalid-feedback" name="name-feedback">
                  <strong name="from-error_txt"></strong>
              </span>
              
          </div>
          
      </div>
      <div class="form-group col-6">
          <label>End in</label>
          <div id="datepicker-popup-end" class="input-group date datepicker form-input p-0">
              <input type="text" class="form-control" name="to" value="{{$offer->to->format('Y-m-d')}}" required>
              <div class="input-group-addon input-group-text">
                  <span class="mdi mdi-calendar"></span>
              </div>
              <span class="invalid-feedback" name="name-feedback">
                  <strong name="from-error_txt"></strong>
              </span>
              
          </div>
          
      </div>
  </div>
  <hr>
  <div class="data-sheet">
      <h4 class="card-title">Data sheet</h4>
      <p class="card-description">offer materials</p>
      <div class="form-inline repeater">
          @foreach (json_decode($offer->materials,true) as $option)
          <div data-repeater-list="group-a">
              <div data-repeater-item class="d-md-flex mb-2">
                  <label class="sr-only" for="inlineFormInputGroup1">Key</label>
                  <div class="input-group mb-2 mr-md-2 mb-md-0">
                      <div class="input-group-prepend">
                          <span class="input-group-text">
                                @
                              </span>

                      </div>
                    <input type="text" name="materials_keys[][keys]" value="{{$option['keys']}}" class="form-control form-control-sm" >

                  </div>
                  <label class="sr-only" for="inlineFormInputGroup2">Value</label>
                  <div class="input-group mb-2 mr-md-2 mb-md-0">
                      <div class="input-group-prepend">
                          <span class="input-group-text">
                                @
                              </span>
                      </div>
                      <input type="text" name="materials_keys[][values]" value="{{$option['values']}}" class="form-control form-control-sm" >

                  </div>
                  <button data-repeater-delete type="button" class="btn btn-danger btn-xs icon-btn ml-2">
                    <i class="mdi mdi-delete"></i>
                  </button>
              </div>
          </div> 
          @endforeach
          
          <button data-repeater-create type="button" class="btn btn-info btn-xs icon-btn ml-2 mb-2">
                <i class="mdi mdi-plus"></i>
              </button>
      </div>
  </div>
  <div class="form-group">
      <div class="custom-file">
          <input type="file" name="photos[]" multiple accept='image/*' class="custom-file-input" id="customFile">
          <label class="custom-file-label" for="customFile">Upload offer images</label>
          <span class="invalid-feedback" name="name-feedback">
              <strong name="from-error_txt"></strong>
          </span>
      </div>
  </div>
  <div class="form-group">
  <textarea rows="3" class="form-control form-input" placeholder="OFFER DESCRIPTION"  name="description">{{$offer->description}}</textarea>
      <small class="help-block"></small>
  </div>
  <!--
      <div class="form-group">
          <input type="file" class="dropify" data-max-file-size="2000kb" name="photo" />
          <small class="help-block"></small>
      </div>
  -->
  <button type="submit" class="btn btn-primary btn-rounded btn-sm btn-gred" id="submit_edit_offer">Submit</button>
  <button class="btn btn-primary btn-rounded btn-sm">Cancel</button>
</form>