@extends('front.main') @section('header_after')

<link rel="stylesheet" href="/front_assets/node_modules/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="/front_assets/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" /> @endsection @section('content')

<div class="col-md-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Offer</h4>
            <p class="card-description">
                add new Offer
            </p>
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li name="error_txt">{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form class="forms-sample" id="add_new_offer_form" action="/offers" method="post" files="true" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="exampleInputName1">Offer Tittle</label> {!! Form::text('title',null,['class'=>'form-control','id'=>'exampleInputName1']) !!}
                </div>
                <div class="form-group">
                    <label>Offer tags</label> {!! Form::select('tags[]',\App\Tag::all()->pluck('name','id'), null, ['class' => 'js-example-basic-multiple','multiple'=>'multiple','style'=>'width:100%']) !!}
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label>Offer Country</label> {!! Form::select('countries_id',\App\Country::all()->pluck('name','id'), null, ['class' => 'form-control form-input']) !!}
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label>Price</label> {!! Form::number('price',null,['class'=>'form-control form-input','placeholder'=>'50$']) !!}
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label>Minimum Number Of peices</label> {!! Form::number('minimum_of_entities',null,['class'=>'form-control form-input','placeholder'=>'50$']) !!}
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label>Delivery Time</label> {!! Form::text('delivery_time',null,['class'=>'form-control form-control-sm','placeholder'=>'50$']) !!}

                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label>Starting From</label>
                    <input class="form-control form-control-sm" name="from" data-inputmask="'alias': 'date'" placeholder="From" />
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <label>End in</label>
                    <input class="form-control form-control-sm" name="to" data-inputmask="'alias': 'date'" placeholder="to" />
                    <small class="help-block"></small>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-body">
                                        <h4 class="card-title">Data sheet</h4>
                                        <p class="card-description">offer materials</p>
                                        <div class="form-inline repeater">
                                            <div data-repeater-list="group-a">
                                                <div data-repeater-item class="d-md-flex mb-2">
                                                    <label class="sr-only" for="inlineFormInputGroup1">Key</label>
                                                    <div class="input-group mb-2 mr-md-2 mb-md-0">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                              @
                                                            </span>
                                                        </div>
                                                        <input type="text" name="materials_keys[][keys]" class="form-control form-control-sm" id="inlineFormInputGroup1" placeholder="eg:Color">
                                                    </div>
                                                    <label class="sr-only" for="inlineFormInputGroup2">Value</label>
                                                    <div class="input-group mb-2 mr-md-2 mb-md-0">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                              @
                                                            </span>
                                                        </div>
                                                        <input type="text" name="materials_keys[][values]" class="form-control form-control-sm" id="inlineFormInputGroup2" placeholder="eg:Red">
                                                    </div>
                                                    <button data-repeater-delete type="button" class="btn btn-danger btn-sm icon-btn ml-2">
                                                      <i class="mdi mdi-delete"></i>
                                                    </button>
                                                </div>
                                            </div>
                                            <button data-repeater-create type="button" class="btn btn-info btn-sm icon-btn ml-2 mb-2">
                                              <i class="mdi mdi-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <textarea type="text" rows="3" class="form-control form-input" placeholder="OFFER DESCRIPTION" name="description"></textarea>
                    <small class="help-block"></small>
                </div>
                <div class="form-group">
                    <input type="file" class="dropify" data-max-file-size="2000kb" name="photo" />
                    <small class="help-block"></small>
                </div>
                <button type="submit" class="btn btn-success mr-2">Submit</button>
                <button class="btn btn-light">Cancel</button>
            </form>
        </div>
    </div>
</div>

@endsection