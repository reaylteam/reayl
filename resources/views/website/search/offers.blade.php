@extends('front.main') @section('content')
<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <!-- Page Content -->
                    <div class="col-lg-12 col-md-12 col-sm-12 px-xs-0">
                        <div class="container px-xs-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="browse">
                                        <h6 class="mb-3 ml-2">BROWSE COMPANIES & OFFERS :</h6>
                                        <form action="/search/results/filter/" method="GET">
                                            <div class="row">
                                                <div class="col-5 input-group find-group">
                                                    <div class="input-group-prepend text-center">
                                                        <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                    </div>
                                                    <input type="text" value="{{$search_key_word}}" class="form-control border-0 search-top" placeholder="Search..." aria-label="Username" name="search_key_word" aria-describedby="basic-addon1">
                                                </div>
                                                <div class="col-2 input-group find-group category-select mx-2">
                                                    <select class="form-control js-example-basic-single" name="country">
                                                          <option value=""></option>
                                                           @foreach(\App\Country::all() as $country)
                                                            <option value="{{$country->code}}" {{$country->id == $country_id ? 'selected':''}}>{{$country->name}}</option>
                                                           @endforeach   
                                                    </select>
                                                </div>
                                                <div class="col-2 input-group find-group category-select">
                                                    <select class="form-control js-example-basic-single-3" name="category">
                                                           <option value="all">All Categories</option>
                                                           @foreach(\App\Category::all() as $category)
                                                                <option value="{{$category->id}}">{{$category->title}}</option>
                                                           @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-2 text-right ml-auto">
                                                    <input type="submit" class="btn btn-primary btn-rounded border-0 btn-lg btn-gred" value="Search" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p class="m-2"><cite class="text-muted">{{$offers->count()}}</cite> <i>Results</i> at <span>Sports</span></p>
                                </div>
                            </div>
                            <div class="row companies-list my-2">
                                <div class="col-12">
                                    <ul class="row nav nav-tabs tab-solid  tab-solid-primary" role="tablist">
                                        <li class="col-2 nav-item text-center p-0">
                                            <a class="nav-link active" id="companies_tab" data-toggle="tab" href="#companies_result" role="tab" aria-controls="companies_result" aria-selected="true">
                                                <img class="img-xs rounded-circle border" src="/front_assets/images/brands/anonymous-company.png" alt="">
                                                Companies
                                            </a>
                                        </li>
                                        <li class="col-2 nav-item text-center p-0">
                                            <a class="nav-link" id="offers_tab" data-toggle="tab" href="#offers_result" role="tab" aria-controls="offers_result" aria-selected="false">
                                                <img class="img-xs rounded-circle border" src="/front_assets/images/products/anonymous-offer.jpg" alt="">
                                                Offers
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-12">
                                    <div class="tab-content tab-content-solid w-100">
                                        <div class="tab-pane fade show active" id="companies_result" role="tabpanel" aria-labelledby="companies_tab">
                                            <h3 class="text-center">Companies</h3>
                                            <div class="row">
                                                <!-- companies list item -->
                                                @foreach($offers->companies as $company)
                                                @foreach($company as $company)
                                                    <!-- company item -->
                                                    <div class="list d-flex align-items-center col-lg-3 col-md-6 col-sm-12 my-3">
                                                        <a href="#" class="d-block mb-0">
                                                            <img class="img-lg rounded border" src="/front_assets/images/brands/anonymous-company.png" alt="">
                                                        </a>
                                                        <div class="wrapper w-100 ml-2">
                                                            <a href="#" class="d-block mb-0">
                                                                <h5 class="mb-0"><b>{{$company->name}}</b></h5>
                                                            </a>
                                                            <p class="mb-0"><small class="text-muted">{{\App\User::find($company->id)->posts->count()}} Post, {{\App\User::find($company->id)->following->count()}} Following</small></p>
                                                            <p class="mb-0"><small class="text-muted">{{$company->address}}</small></p>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more followbtn" company-id="">Follow</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- /company item -->
                                                    @endforeach
                                                @endforeach
                                        
                                                <!-- /companies list item -->
                                            </div>
                                            <!-- Load more -->
                                            <div class="my-3">
                                                <div class="clearfix"></div>
                                                <div class="container text-center">
                                                    <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred"> <i class="fa fa-spinner fa-pulse"></i>load more</button>
                                                </div>
                                            </div>
                                            <!-- /Load more -->
                                        </div>
                                        <div class="tab-pane fade" id="offers_result" role="tabpanel" aria-labelledby="offers_tab">
                                            <h3 class="text-center">Offers</h3>
                                            <div class="row">
                                                <!-- Offers list item -->
                                                
                                                @foreach($offers as  $offer)
                                                    
                                                    <!--offer item-->
                                                    <div class="list d-flex align-items-center col-lg-3 col-md-4 col-sm-6 my-3">
                                                        <a href="#" class="d-block mb-0" name="offer_detail" id="offer_detail">
                                                           <img class="img-lg rounded border" src="/front_assets/images/products/anonymous-offer.jpg" alt="" name="offer_img" id="offer_img">
                                                        </a>
                                                        <div class="wrapper w-100 ml-2">
                                                            <a href="#" class="d-block mb-0" name="offer_detail" id="offer_detail">
                                                                <h5 class="mb-0"><b>{{$offer->title}}</b></h5>
                                                            </a>
                                                            <p class="mb-0"><small class="text-muted">$ {{$offer->price}}</small></p>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                <div class="d-flex align-items-center">

                                                                    <span class="badge badge-success">Available</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/offer item-->
                                                @endforeach
                                                
                                                <!-- /Offers list item -->
                                            </div>
                                            <!-- Load more -->
                                            <div class="my-3">
                                                <div class="clearfix"></div>
                                                <div class="container text-center">
                                                    <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred"> <i class="fa fa-spinner fa-pulse"></i>load more</button>
                                                </div>
                                            </div>
                                            <!-- /Load more -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Page Content -->
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial -->
    </div>
    <!-- row-offcanvas ends -->
</div>

@endsection