@extends('front.main')
@section('content')

<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        
        <nav class="container-fluid bg-gray p-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-6 col-sm-12 text-md-left text-sm-center text-xs-center">
                        <h4 class="p-1">
                            Results
                            <small class="text-muted ml-3">
                            {{$count}}
                        </small>
                        </h4>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="input-group float-right">
                            <div class="input-group-prepend text-center">
                                <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <!-- Page Content -->
                    <div class="col-lg-9 col-md-12 col-sm-12 pr-4 px-xs-0">
                        <div class="container pr-4 px-xs-0">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="browse">
                                        <h6 class="mb-3 ml-2">BROWSE COMPANIES</h6>
                                        <form action="/search/results/" method="GET">
                                            <div class="container">
                                                <div class="input-group find-group float-right">
                                                    <div class="input-group-prepend text-center">
                                                        <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control border-0 search-top" placeholder="Search..." aria-label="Username" name="search_key_word" aria-describedby="basic-addon1">
                                                    <input type="submit" class="btn btn-primary btn-rounded border-0 btn-fw btn-gred" value="FIND user" />
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <p class="m-2"><cite class="text-muted">{{$count}}</cite> <i>Results</i></p>
                                </div>
                            </div>
                            {{-- <div class="row jobs-list my-2">
                            @foreach($jobs as $job)
                                <!-- jobs list item -->
                                <div class="list d-flex align-items-center col-lg-4 col-md-6 col-sm-12 my-3">
                                    <img class="img-lg rounded border" src="/front_assets/images/brands/udacity.png" alt="">
                                    <div class="wrapper w-100 ml-3">
                                        <a href="/companies/{{$user->id}}" class="d-block mb-0">
                                            <h4><b>{{$job->name}}</b></h4>
                                        </a>
                                        
                                    </div>
                                </div>
                                <!-- /jobs list item -->
                            @endforeach

                            </div> --}}
                            <h1>Offers</h1>
                            <div class="row jobs-list my-2">
                            @foreach($offers as $offer)
                                <!-- jobs list item -->
                                <div class="list d-flex align-items-center col-lg-4 col-md-6 col-sm-12 my-3">
                                    <img class="img-lg rounded border" src="/front_assets/images/brands/udacity.png" alt="">
                                    <div class="wrapper w-100 ml-3">
                                        <a href="/companies/{{$user->id}}" class="d-block mb-0">
                                            <h4><b>{{$offer->title}}</b></h4>
                                        </a>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex align-items-center">
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /jobs list item -->
                            @endforeach

                            </div>
                            {{-- <h1>Jobs</h1>
                            <div class="row jobs-list my-2">
                            @foreach($jobs as $job)
                                <!-- jobs list item -->
                                <div class="list d-flex align-items-center col-lg-4 col-md-6 col-sm-12 my-3">
                                    <img class="img-lg rounded border" src="/front_assets/images/brands/udacity.png" alt="">
                                    <div class="wrapper w-100 ml-3">
                                        <a href="/companies/{{$user->id}}" class="d-block mb-0">
                                            <h4><b>{{$job->title}}</b></h4>
                                        </a>
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex align-items-center">
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /jobs list item -->
                            @endforeach

                            </div> --}}

                            <!-- Newsfeeds -->
                            <div class="my-5">
                                <div class="clearfix"></div>
                                <!-- load more -->
                                <div class="container text-center">
                                    <a href="#" class="load-more mx-auto">
                                        <span>Load More Post</span>
                                        <i class="fa fa-ellipsis-h"></i>
                                        </a>
                                </div>
                            </div>
                            <!-- /Newsfeeds -->
                        </div>
                    </div>
                    <!-- /Page Content -->
                    <!-- Right Sidebar -->
                    <div class="right-sidebar col-lg-3 col-md-12 col-sm-12 pl-4 px-xs-0">
                        <!-- wedget -->
                        {{-- @include('website.partials.top_tags') --}}
                        <!-- /wedget -->
                        {{-- <hr class="p-2"> --}}
                        <!-- wedget -->
                        {{-- @include('website.partials.people_may_know') --}}
                    </div>
                    <!-- /Right Sidebar -->
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- row-offcanvas ends -->
</div>

@endsection