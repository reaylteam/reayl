<!-- container-scroller -->
    <!-- Edit Cover Modal start -->
    <div class="modal fade" id="edit_cover" tabindex="-1" role="dialog" aria-labelledby="edit_cover" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                    
                    {!! Form::open(array('url' => '/update/usercover/', 'method'=>'POST','files' => true,'id'=>'edit_cover_form'))!!}
                        <h3 class="text-center mb-3">EDIT YOUR COVER PICTURE</h3>
                        {!! Form::file('cover',['class'=>'dropify','id'=>'cover_input']) !!}
                        

                        <div class="text-center mt-3">
                            <button type="submit" disabled class="btn btn-primary btn-rounded btn-fw btn-gred px-5" id="cover_submit">Save</button>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- Edit Cover Modal Ends -->
