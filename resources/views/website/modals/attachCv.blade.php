<!-- container-scroller -->
<!-- Edit profile Modal start -->
<div class="modal fade" id="attach_cv" tabindex="-1" role="dialog" aria-labelledby="edit_my_profile" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" name="close_btn">
          <span aria-hidden="true">&times;</span>
        </button>
                {!! Form::open(array('url' => '/user/apply/attach_cv/', 'method'=>'POST','files' => true,'id'=>'edit_profile_form'))!!}
                    <h3 class="text-center mb-3">Upload Cv</h3>
                    <input type="file" name="cv" class="dropify" data-max-file-size="30kb" />
                    {!! Form::hidden('job_id',$job->id) !!}
                    <div class="text-center mt-3">
                        <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5" name="submit_btn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit profile Modal Ends -->