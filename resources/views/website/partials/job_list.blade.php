<div class="wedget">
    <div class="row">
        <h5 class="mb-2 col">JOBS LIST</h5>
        <div class="dropdown show">
            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                <h6 class="dropdown-header">Settings</h6>
                <a class="dropdown-item" href="#">Action</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col jobs-list ml-2">
            @foreach($job_list as $job)
                <!-- jobs list item -->
                <div class="list d-flex align-items-center pl-2 pt-3">
                    
                    <img class="img-sm rounded-circle border" src="/front_assets/images/Brands/dove.gif" alt="">
                    <div class="wrapper w-100 ml-3">
                        <a href="{{URL::to('jobs/showDeatile/'.$job->id.'/'.$job->title)}}" class="d-block mb-0" name="job_link"><b>{{$job->title}}</b></a>
                        <small class="text-muted">{{$job->location}}</small>
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="d-flex align-items-center">
                                <p class="mb-0" name="job-level_link">{{$job->seniority_level}}</p>
                            </div>
                            <small class="text-muted ml-auto" name="job-salary_txt">{{$job->salary}}</small>
                        </div>
                    </div>
                </div>
                <!-- /jobs list item -->
            @endforeach
        </div>
    </div>
</div>