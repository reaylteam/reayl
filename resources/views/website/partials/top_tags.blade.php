<div class="wedget">
    <h5 class="mb-2">TOP TAGS</h5>
    <div class="tags">
        @foreach($top_tags as $tag)
            <a class="badge badge-primary badge-pill tag-item" href="{{URL::to('/tags/search/'.$tag->id)}}" name="tag_link" id="tag_link_{{$tag->id}}">{{$tag->name}}</a>
        @endforeach
        <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more" >More +</button>
    </div>
</div>

