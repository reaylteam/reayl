<div class="col-10 mx-auto row p-4 px-xs-0 align-items-center">
            <div class="col-12">
                <h4 class="mb-2" name="top-companies_txt">
                    LATEST OFFERS
                </h4>
            </div>
        </div>
<!--Offers list-->
<div class="col-10 mx-auto row companies-list mb-3">
    @foreach($new_offers as $offer)
        <!--offer-list-item-->
        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
            <div class="card text-center">
                <div class="avatar">
                    <a href="{{URL::to('/offerDetailes/'.$offer->id)}}" name="offer-detail_link">
                    <img class="img-lg rounded border" src="/front_assets/images/products/anonymous-offer.jpg" alt="" name="offer-image">
                </a>
                    <h6 class="d-block my-2" name="offer-title">{{$offer->title}}</h6>
                    <small class="d-block" name="offer-price">{{$offer->price}} Egp, {{$offer->company->name}} </small>
                </div>
            </div>
        </div>
        <!--/offer-list-item-->
    @endforeach
    <div class="col-12 text-center" id="loadMoreBtn">
            <div class="clearfix"></div>
            <div class="container text-center" id="loadMoreBtn">
               <a href="{{URL::to('offers')}}" class="btn btn-primary btn-rounded btn-md btn-gred"> View all</a>
            </div>
        </div>
</div>
<!--/Offers list-->