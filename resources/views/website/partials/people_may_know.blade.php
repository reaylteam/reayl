@php
    if(\Auth::check()){
        $intrests        = \Auth::user()->intrests->pluck('id');
        $people_may_know = \App\UserInterest::whereIn('intrest_id',$intrests)->get()->pluck('user')->unique();
        if($people_may_know->count() < 1)
            $people_may_know = \App\User::orderByRaw('RAND()')->take(4)->get();
        }
    else
        $people_may_know = \App\User::orderByRaw('RAND()')->take(4)->get();
@endphp
<div class="wedget">
    <div class="row">
        <h5 class="mb-2 col">PEOPLE YOU MAY KNOW</h5>
        {{-- <div class="dropdown show">
            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                <h6 class="dropdown-header">Settings</h6>
                <a class="dropdown-item" href="#">Action</a>
            </div>
        </div> --}}
    </div>
    <div class="row people-list m-2">
        @if(\Auth::check())
            @foreach($people_may_know as $user)
                <!-- people list item -->
                <div class="people-item m-1">
                <a href="{{URL::to('/showProfile/'.$user->level.'/'.$user->id.'/'.$user->name)}}" name="user-profile_link">
                    <img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle" name="user-profile_img">
                </a>
                </div>

                <!-- /people list item -->
            @endforeach
        @else
            @foreach(\App\User::all()->take(4) as $user)
                <!-- people list item -->
                <div class="people-item m-1">
                <a href="{{URL::to('/showProfile/'.$user->level.'/'.$user->id.'/'.$user->name)}}" name="user-profile_link">
                    <img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle">
                </a>
                </div>

                <!-- /people list item -->
            @endforeach
        @endif
        
        {{-- <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more-circle" onclick="location.href = '/all/users/';">More +</button> --}}
    </div>
</div>
<!-- /wedget -->