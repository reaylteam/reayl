<div class="wedget">
    <div class="row">
        <h5 class="mb-2 col">NEW OFFERS</h5>
        <div class="dropdown show">
            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                <h6 class="dropdown-header">Settings</h6>
                <a class="dropdown-item" href="#">Action</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="jobs-list ml-2">
            @foreach($new_offers as $offer)
                <!-- jobs list item -->
                <div class="list d-flex align-items-center pl-2 pt-3">

                    <img class="img-md rounded border" src="/front_assets/images/brands/Natural-Language-Processing-Definition.jpg" alt="" name="offer_img">
                    <div class="wrapper w-100 ml-1">
                        <a href="{{URL::to('/offerDetailes/'.$offer->id)}}" class="d-block mb-0" name="offer-detail_link"><b>{{$offer->title}}</b></a>
                        <small class="text-muted" name="offer-price_txt">Price $ {{$offer->price}}</small>
                        <div class="d-flex justify-content-between align-items-center">
                           
                        </div>
                    </div>
                </div>
                <!-- /jobs list item -->
            @endforeach
        </div>
    </div>
</div>