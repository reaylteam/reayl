@extends('front.main')
@section('content')
<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        
        <nav class="container-fluid bg-gray p-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-md-6 col-sm-12 text-md-left text-sm-center text-xs-center">
                        <h4 class="p-1">
                            {{$type}}
                            <small class="text-muted ml-3">
                            {{count($users)}} {{$type}}
                        </small>
                        </h4>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12">
                        <div class="input-group float-right">
                            <div class="input-group-prepend text-center">
                                <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="content-wrapper">
            <div class="container">
                <div class="row">
                    <!-- Page Content -->
                    <div class="col-lg-9 col-md-12 col-sm-12 pr-4 px-xs-0">
                        <div class="container pr-4 px-xs-0">
                            
                            <div class="row jobs-list my-2">
                            @foreach($users as $user)
                                <!-- jobs list item -->
                                <div class="list d-flex align-items-center col-lg-4 col-md-6 col-sm-12 my-3">
                                    <img class="img-lg rounded border" src="/front_assets/images/brands/udacity.png" alt="">
                                    <div class="wrapper w-100 ml-3">
                                        {{-- <a href="/companies/{{$company->id}}" class="d-block mb-0"> --}}
                                            <h4><b>{{$user->name}}</b></h4>
                                        {{-- </a> --}}
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex align-items-center">
                                            @if(\Auth::check())
                                            <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more followbtn" company-id="{{$user->id}}">{{\Auth::user()->checkfollow($user->id)?'UnFollow':'Follow'}}</button>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /jobs list item -->
                            @endforeach
                            </div>
                            
                        </div>
                    </div>
                    <!-- /Page Content -->
                    <!-- Right Sidebar -->
                    <div class="right-sidebar col-lg-3 col-md-12 col-sm-12 pl-4 px-xs-0">
                        <!-- wedget -->
                        <div class="wedget">
                            <h5 class="mb-2">TOP TAGS</h5>
                            <div class="tags">
                                <a class="badge badge-primary badge-pill tag-item" href="#">UI/UX Design</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Food</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Graphic</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Graphic Design</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Brands</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">UI/UX Design</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Food</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Graphic</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Graphic Design</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Brands</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">UI/UX Design</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Food</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Design</a>
                                <a class="badge badge-primary badge-pill tag-item" href="#">Brands</a>
                                <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more">More +</button>
                            </div>
                        </div>
                        <!-- /wedget -->
                        <hr class="p-2">
                        <!-- wedget -->
                        <div class="wedget">
                            <div class="row">
                                <h5 class="mb-2 col">PEOPLE YOU MAY KNOW</h5>
                                <div class="dropdown show">
                                    <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                                        <h6 class="dropdown-header">Settings</h6>
                                        <a class="dropdown-item" href="#">Action</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row people-list m-2">
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#">
            <img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle">
        </a>
                                </div>
                                <!-- /people list item -->
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#"><img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                </div>
                                <!-- /people list item -->
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#"><img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                </div>
                                <!-- /people list item -->
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#"><img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                </div>
                                <!-- /people list item -->
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#"><img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                </div>
                                <!-- /people list item -->
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#"><img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                </div>
                                <!-- /people list item -->
                                <!-- people list item -->
                                <div class="people-item m-1">
                                    <a href="#"><img src="images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                </div>
                                <!-- /people list item -->
                                <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more-circle">More +</button>
                            </div>
                        </div>
                        <!-- /wedget -->
                    </div>
                    <!-- /Right Sidebar -->
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <!-- partial -->
    </div>
    <!-- row-offcanvas ends -->
</div>

@endsection