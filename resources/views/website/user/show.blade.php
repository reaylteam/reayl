@extends('front.main')
@section('content')
<div class="container-fluid page-body-wrapper">
            <div class="row row-offcanvas row-offcanvas-right">
                <!--navbar header for mobile-->
                <nav class="sidebar sidebar-offcanvas d-lg-none" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item nav-profile">
                            <div class="nav-link">
                                <div class="profile-image">
                                    <!--                              <img src="/front_assets/images/faces/face1.jpg" alt="image">-->
                                    <span class="span-sm rounded-circle mr-1">{{$user->name[0]}}</span>
                                </div>
                                <div class="profile-name">
                                    <p class="name">
                                        {{$user->name}}
                                    </p>
                                    <p class="designation">
                                        {{$user->level}}
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link" href="/">
                            <i class="fa fa-home mr-2"></i>
                          Home
                        </a>
                        </li>
                        <li class="nav-item">
                        <a class="nav-link" href="{{url('posts')}}">
                            <i class="fa fa-pencil-square-o mr-2"></i>
                          Posts
                        </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                            <i class="fa fa-building mr-2"></i>
                          Companies
                        </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                            <i class="fa fa-handshake-o mr-2"></i>
                          Jobs
                        </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#user-pages" aria-expanded="false" aria-controls="user-pages">
                            <i class="icon-user menu-icon"></i>
                            <span class="menu-title">Settings</span>
                          </a>
                            <div class="collapse" id="user-pages" style="">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link" href="{{URL::to('profile/'.$user->id.'/edit')}}"> <i class="mr-2 fa fa-cog"></i>Edit profile </a></li>
                                    <li class="nav-item"> <a class="nav-link" href="#"> <i class="mr-2 fa fa-sign-out"></i>Sign out </a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#language" aria-expanded="false" aria-controls="language">
                            <i class="icon-globe menu-icon"></i>
                            <span class="menu-title">Language</span>
                          </a>
                            <div class="collapse" id="language">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link" href="#"><i class="mr-2 flag-icon flag-icon-gb"></i>
                                English</a></li>
                                    <li class="nav-item"> <a class="nav-link" href="#"><i class="mr-2 flag-icon flag-icon-sa"></i>
                                Arabic</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>
                <!--/navbar header for mobile-->
                <nav class="container-fluid bg-gray p-2">
                    <div class="container">
                        <div class="row">
                            <div class="d-flex col-lg-7 col-md-8 col-sm-12 text-md-left text-sm-center text-xs-center">
                                <h4 class="d-flex p-1 mb-0">
                                    {{$user->name}}
                                    <small class="d-flex text-muted ml-3 my-auto mx-xs-0">
                                        <i> . </i>
                                        <div id="connection" class="m-1">
                                            <a href="/followers"><span id="followers">{{count($user->follower)}}</span> Followers</a>
                                        </div>
                                        <i> . </i>
                                        <div id="connection" class="m-1">
                                           <a href="/followings"> Following <span id="followers">{{count($user->following)}}</span></a>
                                        </div>                                     
                                    </small>
                                </h4>
                            </div>
                            {{-- <div class="col-lg-2 col-md-4 col-sm-12">
                                <div class="text-lg-right text-md-right text-sm-center text-xs-center my-xs-2">
                                    <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5">Follow</button>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12">
                                <div class="input-group float-right">
                                    <div class="input-group-prepend text-center">
                                        <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                                    </div>
                                    <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </nav>
                <div class="profile-about">
                    @if($cover->first())
                        <div class="cover-img" style="background-image: url({{ $cover->first()->getUrl() }})">
                    @else
                        <div class="cover-img" style="background-image: url('/fdfd')">
                    @endif
                    @if(Auth::id()==$user->id)
                        <a data-toggle="modal" data-target="#edit_cover" class="edit-cover">
                            <i class="fa fa-camera-retro"></i>
                            <span class="d-xs-none">Edit Cover Picture</span>
                        </a>
                    @endif
                    </div>
                    <div class="profile-img">
                        
                    @if($profile->first())
                        <img src="{{URL::to($profile->first()->getUrl())}}" class="rounded-circle" alt="image">
                    @else
                        <img src="/front_assets/images/faces/face1.jpg" class="rounded-circle" alt="image">
                    @endif
                    @if(Auth::id()==$user->id)
                        <a data-toggle="modal" data-target="#edit_my_profile" class="edit-profile">
                            <i class="fa fa-camera-retro"></i>
                        </a>
                    @endif
                    </div>
                    <div class="container mt--3">
                        <div class="row text-center">
                            <div class="col-lg">
                                <div class="social-icons text-center">
                                    <a href="{{ $user->individual_user_info->facebook_url }}"><i class="fa fa-facebook bg-facebook"></i></a>
                                    <a href="{{ $user->individual_user_info->twitter_url }}"><i class="fa fa-twitter bg-twitter"></i></a>
                                    <a href="{{ $user->individual_user_info->linkedin_url }}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                    <a href="{{ $user->individual_user_info->behance_url }}"><i class="fa fa-behance bg-behance"></i></a>
                                    <a href="{{ $user->individual_user_info->drobble_url }}"><i class="fa fa-dribbble bg-dribbble"></i></a>
                                </div>
                                <div class="text-center mt-3">
                                    <span class="d-block text-muted my-2">
                                      <i class="fa fa-globe"></i>
                                      <span>www.example.com</span>
                                    </span>
                                    <span class="d-block text-muted my-2">
                                      <i class="fa fa-map-marker text-gray"></i>
                                      <span>Egypt , Giza , Almohandsin</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="mx-auto">
                                    <a href="{{URL::to('myprofile/edit')}}" class="btn btn-light btn-rounded btn-fw mb-4">Edit Profile</a>
                                </div>
                                <p class="text-muted">{{\Auth::user()->individual_user_info?\Auth::user()->individual_user_info->about_me:''}}</p>
                            </div>
                            <div class="col-lg">
                                <div class="py-2 px-4 d-flex text-center">
                                    <p class="mb-0">Front End Developer</p>
                                    <small class="text-muted ml-auto ">30-60 USD</small>
                                </div>
                                <div class="py-2 px-4 text-center">
                                    <p class="mb-0">7 Years of Experience</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <!-- Page Content -->
                            <div class="col-lg-9 col-md-12 col-sm-12 pr-4 px-xs-0">
                                <div class="container pr-4  px-xs-0">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="create-post">
                                                <form class="create-post-form">
                                                    <textarea class="form-control body_contnet" rows="4" placeholder="Write your post..."></textarea>
                                                </form>
                                                <div class="d-flex justify-content-between align-items-center p-3">
                                                    <div class="mr-auto">
                                                        <a class="add-img pr-2" href="#"><i class="fa fa-photo"></i></a>
                                                        <a class="add-face pr-2" href="#"><i class="fa fa-smile-o"></i></a>
                                                    </div>
                                                    <div class="ml-auto">
                                                        <input type="submit" class="btn btn-primary btn-rounded border-0 btn-fw btn-gred post_now" value="POST NOW" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Newsfeeds -->
                                    <div class="my-5" id="posts">
                                        <!-- Post -->
                                        @foreach ($posts as $post)
                                            <div class="newsfeed border-bottom pb-2 my-4">
                                                <div class="row">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 text-sm-center text-xs-center">
                                                        <a href="#">
                                                          
                                                            @if($post->user->profile())
                                                                <img class="img-lg rounded border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                                                            @else
                                                                <img class="img-lg rounded border" src="/front_assets/images/faces/face1.jpg" alt="">
                                                            @endif
                                                        </a>
                                                    </div>
                                                    <div class="col-lg-10 col-md-10 col-sm-12">
                                                        <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 text-xs-center">{{$post->user->name}}</h6>
                                                        <p class="text-muted text-xs-center">Since {{$post->duration}}</p>
                                                        <p class="my-2">{{$post->body}}</p>
                                                        @if($post->user->profile())
                                                                <img class="img-lg rounded border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                                                        @else
                                                                <img class="img-lg rounded border" src="/front_assets/images/faces/face1.jpg" alt="">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="container newsfeed-footer d-flex justify-content-between align-items-center pt-5">
                                                    <div class="d-flex align-items-center mr-auto">
                                                        <a class="like-btn" href="#">
                                                            @if(Auth::check())
                                                                <span class="text-muted mr-1" name="span-container-of-like-shpe"><i class="fa fa-heart" name="like-heart-shape" style="color:{{  $post->liked()?'#7e4cf9':'' }};"></i></span>
                                                            @else
                                                                <span class="text-muted mr-1" name="span-container-of-like-shpe" data-toggle="modal" data-target="#sign_in">
                                                                        <i class="fa fa-heart" name="like-heart-shape" style="color:{{  $post->liked()?'#7e4cf9':'' }};"></i>
                                                                </span>
                                                            @endif
                                                            <span class="text-muted">{{$post->likeCount}} Likes</span>
                                                          </a>
                                                    </div>
                                                    {{-- <div class="d-flex align-items-center ml-auto mr-3">
                                                        <a class="share-btn" href="#">
                                                            <span class="text-muted mr-1"><i class="fa fa-share"></i></span>
                                                            <span class="text-muted"> Shares</span>
                                                          </a>
                                                    </div> --}}
                                                    <div class="d-flex align-items-center">
                                                        <a class="comment-btn" href="#">
                                                        <span class="text-muted mr-1"><i class="fa fa-comments"></i></span>
                                                        <span class="text-muted">{{$post->comments->count()}} Comments</span>
                                                      </a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        
                                        <!-- /Post -->
                                        <div class="clearfix"></div>
                                        <!-- load more -->
                                        <div class="container text-center">
                                            <a href="#" class="load-more mx-auto">
                                                <span>Load More Post</span>
                                                <i class="fa fa-ellipsis-h"></i>
                                              </a>
                                        </div>
                                    </div>
                                    <!-- /Newsfeeds -->
                                </div>
                            </div>
                            <!-- /Page Content -->
                            <!-- Right Sidebar -->
                            <div class="right-sidebar col-lg-3 col-md-12 col-sm-12 pl-4  px-xs-0">
                                <!-- wedget -->
                                <div class="wedget">
                                    <div class="row">
                                        <h5 class="mb-2 col">JOBS LIST</h5>
                                        <div class="dropdown show">
                                            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                                            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                <h6 class="dropdown-header">Settings</h6>
                                                <a class="dropdown-item" href="#">Action</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col jobs-list ml-2">
                                            <!-- jobs list item -->
                                            <div class="list d-flex align-items-center pl-2 pt-3">
                                                <img class="img-sm rounded-circle border" src="/front_assets/images/Brands/dove.gif" alt="">
                                                <div class="wrapper w-100 ml-3">
                                                    <a href="#" class="d-block mb-0"><b>New Approaches to Swarm...</b></a>
                                                    <small class="text-muted">Giza Company</small>
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="d-flex align-items-center">
                                                            <p class="mb-0">Front End Developer</p>
                                                        </div>
                                                        <small class="text-muted ml-auto">30-60 USD</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /jobs list item -->
                                            <!-- jobs list item -->
                                            <div class="list d-flex align-items-center pl-2 pt-3">
                                                <img class="img-sm rounded-circle border" src="/front_assets/images/Brands/amazon.jpg" alt="">
                                                <div class="wrapper w-100 ml-3">
                                                    <a href="#" class="d-block mb-0"><b>Helping people finding the best...</b></a>
                                                    <small class="text-muted">Mobarak Company</small>
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="d-flex align-items-center">
                                                            <p class="mb-0">UI & UX Designer</p>
                                                        </div>
                                                        <small class="text-muted ml-auto">30-60 USD</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /jobs list item -->
                                            <!-- jobs list item -->
                                            <div class="list d-flex align-items-center pl-2 pt-3">
                                                <img class="img-sm rounded-circle border" src="/front_assets/images/Brands/dove.gif" alt="">
                                                <div class="wrapper w-100 ml-3">
                                                    <a href="#" class="d-block mb-0"><b>New Approaches to Swarm...</b></a>
                                                    <small class="text-muted">Giza Company</small>
                                                    <div class="d-flex justify-content-between align-items-center">
                                                        <div class="d-flex align-items-center">
                                                            <p class="mb-0">Front End Developer</p>
                                                        </div>
                                                        <small class="text-muted ml-auto">30-60 USD</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /jobs list item -->
                                        </div>
                                    </div>
                                </div>
                                <!-- /wedget -->
                                <hr class="p-2">
                                <!-- wedget -->
                                <div class="wedget">
                                    <div class="row">
                                        <h5 class="mb-2 col">PEOPLE YOU MAY KNOW</h5>
                                        <div class="dropdown show">
                                            <a class="text-right col dropdown-toggle" href="#" id="dropdown_List" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-h"></i></a>
                                            <div class="dropdown-menu" aria-labelledby="dropdown_List" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -183px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                <h6 class="dropdown-header">Settings</h6>
                                                <a class="dropdown-item" href="#">Action</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row people-list m-2">
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#">
                                                    <img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle">
                                                </a>
                                        </div>
                                        <!-- /people list item -->
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#"><img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                        </div>
                                        <!-- /people list item -->
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#"><img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                        </div>
                                        <!-- /people list item -->
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#"><img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                        </div>
                                        <!-- /people list item -->
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#"><img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                        </div>
                                        <!-- /people list item -->
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#"><img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                        </div>
                                        <!-- /people list item -->
                                        <!-- people list item -->
                                        <div class="people-item m-1">
                                            <a href="#"><img src="/front_assets/images/faces/face1.jpg" alt="image" class="img-sm rounded-circle"></a>
                                        </div>
                                        <!-- /people list item -->
                                        <button type="button" class="btn btn-primary btn-rounded btn-xs btn-more-circle">More +</button>
                                    </div>
                                </div>
                                <!-- /wedget -->
                            </div>
                            <!-- /Right Sidebar -->
                        </div>
                    </div>
                </div>
                <div class="content-wrapper bg-gray">
                    <div class="container">
                        <!--Companies list-->
                        <div class="row companies-list">
                            <div class="col-12 p-3">
                                <h4 class="mb-2">
                                    YOU MAY LIKE
                                </h4>
                            </div>
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="/front_assets/images/brands/amazon.jpg" alt="">
                                          </a>
                                        <h6 class="d-block my-2">Amazone</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="/front_assets/images/brands/dove.gif" alt="">
                                          </a>
                                        <h6 class="d-block my-2">Dove</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="/front_assets/images/brands/servicam.jpg" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Servicam</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="/front_assets/images/brands/amazon.jpg" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Amazone</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="/front_assets/images/brands/dove.gif" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Dove</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="/front_assets/images/brands/servicam.jpg" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Servicam</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                        </div>
                        <!--/Companies list-->
                    </div>
                </div>
                <!-- content-wrapper ends -->
                <!-- partial -->
            </div>
            <!-- row-offcanvas ends -->
        </div>
        <!-- page-body-wrapper ends -->
        @include('website.modals.editProfile')
@endsection