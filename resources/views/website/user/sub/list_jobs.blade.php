<!-- partial -->
<div class="content-wrapper">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">jobs</h4>

      <div class="row">
        <div class="col-12">
          <div class="table-responsive">
            <table id="order-listing" class="table">
              <thead>
                <tr>
                    <th>#</th>
                    <th>Created On</th>
                    <th>title</th>
                    <th>price</th>
                    <th>job Requierment</th>
                    
                    
                    <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach(\Auth::user()->jobs as $key=>$job)
                  <tr>
                      <td>{{$key}}</td>
                      <td>{{$job->created_at->format('d-m-Y')}}</td>
                      <td>{{$job->title}}</td>
                      <td>{{$job->salary}}</td>
                      <td>{{$job->seniority_level}}</td>
                     
                      <td>
                        <a href="/jobs/delete/{{$job->id}}" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this job?');" name="delete-userjob_link">Delete</a>
                      </td>
                  </tr>
                @endforeach
                
              </tbody>
            </table>                    
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- content-wrapper ends -->