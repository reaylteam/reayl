@extends('front.main') @section('content') @if(session('status'))
<div class="card card-inverse-secondary mb-5">
    <div class="card-body">
        <p class="mb-4" name="success-session_txt">
            Well done! You successfully read this important alert message.
        </p>

    </div>
</div>
@endif
<!-- partial -->
<div class="content-wrapper pt-2">
    <ul class="row nav nav-tabs tab-solid tab-solid-primary" role="tablist">
        <li class="col-6 nav-item text-center">
            <a class="nav-link active" id="users_tab" data-toggle="tab" href="#users_content" role="tab" aria-controls="users_content" aria-selected="true">
                <h4>Users</h4>
            </a>
        </li>
        <li class="col-6 nav-item text-center">
            <a class="nav-link" id="offers_tab" data-toggle="tab" href="#offers_content" role="tab" aria-controls="offers_content" aria-selected="false">
                <h4>Offers</h4>
            </a>
        </li>
    </ul>
    <div class="tab-content tab-content-solid w-100">
        <div class="tab-pane fade show active" id="users_content" role="tabpanel" aria-labelledby="users_tab">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <h4 class="col-6 card-title">Users</h4>
                        <div class="col-6 text-right">
                            <a href="{{URL::to('/users/add-account/')}}" class="btn btn-primary btn-rounded btn-sm btn-gred" name="add-subuser_link" data-toggle="modal" data-target="#add_subuser">
                        <i class="fa fa-plus"></i>
                        Add subuser
                    </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table text-center table-dark table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Created On</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Published posts</th>
                                            <th>Published offers</th>

                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>0</td>
                                            <td>{{$user->created_at->format('d-m-Y')}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->posts->count()}}</td>
                                            <td>{{$user->offers->count()}}</td>
                                            <td>
                                                <i class="fa fa-user-circle"></i> Owner
                                            </td>

                                        </tr>
                                        @foreach($sub_users as $key=>$user)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$user->created_at->format('d-m-Y')}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->posts->count()}}</td>
                                            <td>{{$user->offers->count()}}</td>
                                            <td>
                                                <a href="/users/delete/{{$user->id}}" class="btn btn-danger btn-sm btn-rounded" onclick="return confirm('Are you sure you want to delete this user?');" name="delete-subuser_link">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="my-3">
                        <div class="clearfix"></div>
                        <div class="container text-center">
                            <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred"> <i class="fa fa-spinner fa-pulse"></i>Load more users</button>
                        </div>
                    </div> --}}
                </div>
            </div>
            <!-- Add subuser Modal start -->
            <div class="modal fade" id="add_subuser" tabindex="-1" role="dialog" aria-labelledby="add_subuser" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            
                            <form method="POST" action="/users/register" id="add_subuser_form">
                                @csrf

                                <h4>Add subuser</h4>
                                <div class="alert alert-fill-primary rounded-0 my-1" role="alert" id="check_user_in_system">
                                    <i class="fa fa-info-circle"></i> Make sure that user was registerd before.
                                  </div>
                                <div class="form-group">
                                    {{-- <label for="name" class="col-form-label text-md-right">{{ __('Name') }}</label>

                                    <input id="subuser_name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-input" name="name" value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong name="name-error_txt">{{ $errors->first('name') }}</strong>
                                    </span> @endif --}}
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                    <input id="user_email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-input" name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong name="email-error_txt">{{ $errors->first('email') }}</strong>
                                    </span> @endif
                                </div>

                                {!! Form::hidden('user_id',\Auth::user()->id) !!}

                                <div class="form-group mb-0 text-center">
                                    <button type="submit" class="btn btn-primary btn-gred btn-rounded btn-fw" name="regster_btn">
                                    {{ __('Add subuser') }}
                                </button>
                                {{-- <a href="#" data-toggle="modal" data-target="#subuser_pic" name="subuser_pic_link" id="subuser_pic_link" style="diplay:none">Subuser Picture</a> --}}
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Add subuser Modal Ends -->
            <!-- Reset Password Modal start -->
            <div class="modal fade" id="subuser_pic" tabindex="-1" role="dialog" aria-labelledby="subuser_pic" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-body p-3">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                            <div class="content">
                                <h3 class="col-12 font-weight-light text-center mb-3" id="sub_user_check_message">Are you mean this account?</h3>
                                <div class="auth">
                                    <div class="nav-item nav-profile">
                                      <div class="nav-link text-center">
                                        <div class="profile-image">
                                          <img class="img-lg rounded-circle border-1" src="/front_assets/images/faces/anonymous-user.png" alt="image" id="sub_user_image">
                                        </div>
                                        <div class="profile-name my-2">
                                          <h6 class="text-muted" id="sub_user_name">
                                            Account Name
                                          </h6>
                                          <input type="hidden" name="email" id="sub_user_found_email">
                                        </div>
                                        <div class="row">
                                            <div class="col text-right">
                                                <button type="submit" class="btn btn-primary btn-gred btn-rounded btn-md" name="regster_btn" id="yes_btn">
                                                    Yes
                                                </button>
                                            </div>
                                            <div class="col text-left">
                                                <button type="submit" class="btn btn-primary btn-gred btn-rounded btn-md" name="regster_btn" id="no_btn">
                                                    No
                                                </button>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Reset Password Modal Ends -->
        </div>
        <div class="tab-pane fade" id="offers_content" role="tabpanel" aria-labelledby="offers_tab">
            @include('website.user.sub.list_offers')
        </div>
    </div>
</div>
<!-- content-wrapper ends -->
@endsection