<!-- partial -->
<div class="card">
    <div class="card-body">
        <h4 class="card-title">Offers</h4>

        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="order-listing" class="table text-center table-dark table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Created On</th>
                                <th>title</th>
                                <th>price</th>

                                <th>Minimum Of Entities</th>


                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(\Auth::user()->offers as $key=>$offer)
                            <tr id="offer_{{$offer->id}}">
                                <td>{{$key}}</td>
                                <td>{{$offer->created_at->format('d-m-Y')}}</td>
                                <td>{{$offer->title}}</td>
                                <td>{{$offer->price}}</td>
                                <td>{{$offer->minimum_of_entities}}</td>

                                <td>
                                    <a href="/offerDetailes/{{$offer->id}}" class="btn btn-light btn-sm btn-rounded" name="show-offer_link">Show</a>
                                    <a href="#" id="edit_offer_btn" offerID="{{$offer->id}}"  class="btn btn-info btn-sm btn-rounded" name="edit-offer_link"><i class="fa fa-edit"></i>Edit</a>
                                    <a href="#" class="btn btn-danger btn-sm btn-rounded delete_offer" name="delete-offer_link" offerID="{{$offer->id}}" userID="{{$offer->user_id}}" data-toggle="modal" data-url="/offers/delete/{{$offer->id}}" data-id="{{$offer->id}}" data-target="#custom-width-modal">Remove</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="my-3">
            <div class="clearfix"></div>
            <div class="container text-center">
                <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred"> <i class="fa fa-spinner fa-pulse"></i>Load more offers</button>
            </div>
        </div>
    </div>
</div>
<!-- Delete offer link Modal start -->
<div class="modal fade" id="remove_offer" tabindex="-1" role="dialog" aria-labelledby="remove_offer" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                <form method="get" action="/offers/delete">
                    <div class="form-group text-center">
                        <h3>Are you sure?</h3>
                    </div>
                    <input type="hidden" name="id">
                    <div class="form-group mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary" name="submit">
                                   Submit
                                </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete offer link Modal Ends -->

<!-- Delete offer link Modal start -->
<div class="modal fade remove_offer" id="custom-width-modal"  tabindex="-1" role="dialog" aria-labelledby="remove_offer" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body p-3">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                    <form method="POST" action="#" class="remove_offer_form">
                        @csrf
                        <div class="form-group text-center">
                            <h3>Are you sure?</h3>
                        </div>
                        <div class="form-group mb-0">
                            <div class="col-md-12 text-center">
                                <button  class="btn btn-primary submit_query" name="submit">
                                       Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Delete offer link Modal Ends -->
<!-- content-wrapper ends -->

<!-- Edit Offer Modal start -->
<div class="modal fade" id="edit_offer" tabindex="-1" role="dialog" aria-labelledby="edit_offer" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                <div class="content">
                    <h3 class="col-12 font-weight-light text-center mb-3">Edit offer</h3>
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="container" id="offer_edit_model">

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Edit Offer Modal Ends -->