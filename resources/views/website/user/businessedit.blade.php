@extends('front.main') @section('header_after')
<link rel="stylesheet" href="/front_assets/toaster/css/jquery.toast.css"> @endsection @section('content')
<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        <!--navbar header for mobile-->

        <!--/navbar header for mobile-->
        <nav class="container-fluid bg-gray p-2">
            <div class="container">
                <div class="row">
                    <div class="d-flex col-lg-7 col-md-8 col-sm-12 text-md-left text-sm-center text-xs-center">
                        <h4 class="d-flex p-1 mb-0" name="username_txt">
                            {{$user->name}}
                            <small class="d-flex text-muted ml-3 my-auto mx-xs-0">
                                
                                <div id="connection" class="m-1">
                                    <a href="/followers" name="user-followers_link"><span id="followers">{{count($user->follower)}}</span> Followers</a>
                                </div>
                                <i> . </i>
                                <div id="connection" class="m-1">
                                   <a href="/followings" name="user-following_link"> Following <span id="followers">{{count($user->following)}}</span></a>
                                </div>                                        
                            </small>
                        </h4>
                    </div>
                    {{--
                    <div class="col-lg-2 col-md-4 col-sm-12">
                        <div class="text-lg-right text-md-right text-sm-center text-xs-center my-xs-2">
                            <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5">Follow</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <div class="input-group float-right">
                            <div class="input-group-prepend text-center">
                                <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div> --}}
                </div>
            </div>
        </nav>
        {!! Form::hidden('lat',\Auth::user()->lat,['id'=>'lat']) !!} {!! Form::hidden('lang',\Auth::user()->lang,['id'=>'lang']) !!}
        <div class="profile-about">
            @if($user->cover())
                <div class="cover-img" style="background-image: url({{ $user->cover()->getUrl() }})">
            @else
                <div class="cover-img" style="background-image: url('/fdfd')">
            @endif 
                    <a data-toggle="modal" data-target="#edit_cover" class="edit-cover" name="coverpicture-edit_link">
                    <i class="fa fa-camera-retro"></i>
                    <span class="d-xs-none">Edit Cover Picture</span>
                </a>
                </div>
                <div class="profile-img">
                    @if($user->profile())
                        <img class="rounded-circle" src="{{ \Auth::user()->profile()->getUrl() }}">
                    @else
                        <img class="rounded-circle" src="/front_assets/images/faces/anonymous-user.png">
                    @endif
                    {{-- <img src="{{URL::to($profile->first()->getUrl())}}" class="rounded-circle" alt="image" name="user-profile_img"> @else
                    <img src="/front_assets/images/faces/face1.jpg" class="rounded-circle" alt="image" name="default-profile_img"> @endif --}}
                    <a data-toggle="modal" data-target="#edit_my_profile" class="edit-profile" name="profilepicture-edit_link">
                    <i class="fa fa-camera-retro"></i>
                </a>
                </div>
                <div class="container mt--3">
                    {!! Form::open(['url'=>url('companies/'.$user->id),'method'=>'put' ,'id'=>'edit_profile']) !!}

                    <div class="row text-left">

                        <div class="col-lg order-lg-2 order-md-1 px-4">
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Company Name</label>
                                <input type="text" class="form-control form-input" placeholder="Enter your name" name="company_name" value="{{\Auth::user()->name}}" />
                            </div>
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Company Location</label>
                                {!! Form::select('countries_id',\App\Country::all()->pluck('name','id'), null, ['class' => 'form-control form-input','id'=>'countries_id' ]) !!}
                            </div>
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Company Bio</label>
                                <textarea placeholder="Enter your Vission" name="bio" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->bio:''}}" class="form-control form-input" id="" cols="30" rows="12"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Company Website</label>
                                <input type="text" class="form-control form-input" placeholder="www.example.com" name="website" id="website" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->website:''}}"/>
                            </div>
                        </div>
                        <div class="col-lg order-lg-3 order-md-2 px-4">
                            <!--
                            <div class="form-group edit-profile-category">
                                <label for="" class="ml-2 text-muted">Company Category</label>
                                <select class="form-control js-example-basic-single-4 form-input">
                                    <option value=""></option>  
                                    <option value="asd">All Categories</option>
                                    <option value="dsa">category1</option>
                                </select>
                            </div>
                            -->
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Company Email</label>
                                <input type="email" class="form-control form-input" placeholder="info@company.com" name="bussiness_email" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->bussiness_email:''}}" />
                            </div>
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Company Phone</label>
                                <input type="text" class="form-control form-input" placeholder="+xx xxx xxxx xxxx" name="phone" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->phone:''}}" />
                            </div>
                            <div class="form-group">
                               <label for="" class="ml-2 text-muted">Facebook link</label>
                                <input type="text" class="form-control form-input" name="facebook" id="facebook" placeholder="Facebook" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->facebook:''}}">
                            </div>
                            <div class="form-group">
                               <label for="" class="ml-2 text-muted">Twitter link</label>
                                <input type="text" class="form-control form-input" name="twitter" id="twitter" placeholder="Twitter" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->twitter:''}}">
                            </div>
                            <div class="form-group">
                               <label for="" class="ml-2 text-muted">LinkedIn link</label>
                                <input type="text" class="form-control form-input" name="linkedin" id="linkedin" placeholder="LinkedIn" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->linkedin:''}}">
                            </div>

                        </div>
                        <div class="col-lg order-lg-1 order-md-3 grid-margin">
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Map Address</label>
                                    <div class="map-container">
                                        <div id="captor-map-theme" class="google-map"></div>
                                    </div>
                            </div>
                            <div class="form-group pt-4">
                                <label for="" class="ml-2 text-muted">Address</label>
                                <input type="text" class="form-control form-input" placeholder="Enter your Address" name="address" value="{{(\Auth::user()->business_user_info)?\Auth::user()->business_user_info->address:''}}" />
                            </div>
                        </div>



                    </div>
                    <div class="row">
                        <div class="col-lg"></div>
                        <div class="col-lg-5 mx-auto">
                            <div class="d-flex justify-content-between align-items-center px-4 py-3">
                                <div class="mr-auto">
                                    <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5">Cancel</button>
                                </div>
                                <div class="ml-auto">
                                    <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                    {!! Form::close()!!}
                </div>
            </div>

        </div>
        <!-- row-offcanvas ends -->
    </div>
    @include('website.modals.editProfile') @endsection @section('footer_after')
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6yohSe89WiHJXhZCUA6wSNQnCEzySQVc&amp;callback=initMap"></script>
    <script src="/front_assets/toaster/js/jquery.toast.js"></script>
    <script src="/front_assets/js/google-maps.js"></script>
    @endsection