@extends('front.main') @section('header_after')@endsection @section('content')


<div class="profile-about">
    @if($user->cover())
    <div class="cover-img" style="background-image: url({{ $user->cover()->getUrl() }})">
        @else
        <div class="cover-img" style="background-image: url('/fdfd')">
            @endif @if(Auth::id()==$user->id)
            <a data-toggle="modal" data-target="#edit_cover" class="edit-cover">
                <i class="fa fa-camera-retro"></i>
                <span class="d-xs-none">Edit Cover Picture</span>
            </a> @endif
        </div>
        <div class="profile-img">
            @if($user->profile())
            <img src="{{  $user->profile()->getUrl() }}" class="rounded-circle" alt="image" name="user-profile_img"> @else
            <img src="/front_assets/images/brands/anonymous-company.png" class="rounded-circle" alt="image" name="default-profile_img"> @endif @if(Auth::id()==$user->id)
            <a data-toggle="modal" data-target="#edit_my_profile" class="edit-profile">
                <i class="fa fa-camera-retro"></i>
            </a> @endif
        </div>
        <div class="container mt--3">
            <div class="row text-center">
                <div class="col-lg">

                </div>

                <div class="col-lg-7 company-info">
                    <div class="mx-auto">
                    <h2 class="text-dark">{{ $user->business_user_info->company_name}}</h2>
                    </div>
                    <div class="text-center px-5">
                        <span class="text-muted my-2">
                                  <i class="fa fa-globe"></i>
                                  <span>{{ $user->business_user_info->website}}</span>
                        </span>
                    </div>
                    <p class="text-muted mb-1">{{ $user->business_user_info->bio}}</p>
                    <span class="d-block text-muted my-2">
                                  <i class="fa fa-map-marker text-gray"></i>
                                  <span>{{ $user->business_user_info->address}}</span>
                    </span>
                    <div class="social-icons text-center">
                        <a href="{{ $user->business_user_info->facebook}}"><i class="fa fa-facebook bg-facebook"></i></a>
                        <a href="{{ $user->business_user_info->twitter}}"><i class="fa fa-twitter bg-twitter"></i></a>
                        <a href="{{ $user->business_user_info->linkedin}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                        {{-- <a href="{{ $user->business_user_info->address}}"><i class="fa fa-behance bg-behance"></i></a>
                        <a href="{{ $user->business_user_info->address}}"><i class="fa fa-dribbble bg-dribbble"></i></a> --}}
                    </div>
                    @if(Auth::id()==$user->id)
                    <div class="d-flex justify-content-between align-items-center px-4 py-2">
                        <div class="mx-auto">
                            <a href="#" class="btn btn-primary btn-gred btn-rounded btn-fw" data-toggle="modal" data-target="#create_offer" name="createOffer_link">Add New Offer</a>
                        </div>
                    </div>
                    @endif
                    {{-- <div class="d-flex justify-content-between align-items-center px-4 py-2">
                        <div class="mx-auto">
                            <a href="#" class="btn btn-primary btn-gred btn-rounded btn-fw" data-toggle="modal" data-target="#create_offer" name="createOffer_link">Follow me</a>
                        </div>
                    </div> --}}
                </div>
                <div class="col-lg">
                </div>
            </div>
            <hr>
        </div>
    </div>
    <div class="content-wrapper py-0">
        <div class="container">
            <div class="row">
                <!-- Page Content -->
                <div class="col-12">
                    <ul class="row nav nav-tabs tab-solid tab-solid-primary" role="tablist">
                        <li class="col-6 nav-item text-center">
                            <a class="nav-link active" id="posts_tab" data-toggle="tab" href="#posts_content" role="tab" aria-controls="posts_content" aria-selected="true">
                                Posts
                            </a>
                        </li>
                        <li class="col-6 nav-item text-center">
                            <a class="nav-link" id="offers_tab" data-toggle="tab" href="#offers_content" role="tab" aria-controls="offers_content" aria-selected="false">
                                Offers
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 px-xs-0">
                    <div class="tab-content tab-content-solid w-100">
                        <div class="tab-pane fade show active" id="posts_content" role="tabpanel" aria-labelledby="posts_tab">
                            <div class="container px-xs-0">
                                @if(Auth::id()==$user->id)
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="create-post">
                                            <form class="create-post-form">
                                                <textarea class="form-control body_contnet" rows="4" placeholder="Write your post..."></textarea>
                                            </form>
                                            <div class="d-flex justify-content-between align-items-center p-3">
                                                <div class="mr-auto">
                                                    <a class="add-img pr-2" href="#"><i class="fa fa-photo"></i></a>
                                                    <a class="add-face pr-2" href="#"><i class="fa fa-smile-o"></i></a>
                                                </div>
                                                <div class="ml-auto">
                                                    <input type="submit" class="btn btn-primary btn-rounded border-0 btn-fw btn-gred post_now" value="POST NOW" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!-- Newsfeeds -->
                                <div class="my-5" id="posts">
                                    <!-- Post -->
                                    @foreach ($user->posts as $post)
                                    <div class="newsfeed border-bottom pb-2 my-4">
                                        <div class="row">
                                            <div class="col-lg-1 col-md-2 col-sm-12 text-sm-center text-xs-center">
                                                <a href="#">
                                                    @if($post->user->profile())
                                                        <img class="img-lg rounded border" src="{{URL::to($post->user->profile()->getUrl())}}" alt="">
                                                    @else
                                                        <img class="img-lg rounded border" src="/front_assets/images/faces/face1.jpg" alt="">
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="col-lg-11 col-md-10 col-sm-12 pl-3">
                                                <h6 class="mr-auto mb-0 mt-sm-2 mt-xs-2 text-xs-center">{{$post->user->name}}</h6>
                                                <p class="text-muted text-xs-center mb-1">Since {{$post->duration}} </p>
                                                <p class="my-2">{{$post->body}}</p>
                                            </div>
                                        </div>
                                        @endforeach

                                        <!-- /Post -->
                                    </div>
                                    <!-- Load more -->
                                    {{-- <div class="my-3">
                                        <div class="clearfix"></div>
                                        <div class="container text-center">
                                            <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred"> <i class="fa fa-spinner fa-pulse"></i>Load more posts</button>
                                        </div>
                                    </div> --}}
                                    <!-- /Load more -->
                                    <!-- /Newsfeeds -->
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="offers_content" role="tabpanel" aria-labelledby="offers_tab">
                            <div class="row">
                                <!-- Offers list item -->
                                @foreach ($user->offers as $offer)
                                    <!--offer item-->
                                <div class="list d-flex align-items-center col-lg-3 col-md-4 col-sm-6 my-3">
                                    <a href="/offerDetailes/{{ $offer->id }}" class="d-block mb-0" name="offer_detail" id="offer_detail">
                                        @if($offer->images()->first())
                                                    <img class="img-lg rounded border" src="{{ $offer->images()->first()->getUrl() }}" alt="" name="offer_img" id="offer_img">
                                                   
                                        @else
                                        <img class="img-lg rounded border" src="/front_assets/images/products/anonymous-offer.jpg" alt="" name="offer_img" id="offer_img">
                                                    
                                        @endif
                                    </a>
                                    <div class="w-100 ml-2">
                                        <a href="/offerDetailes/{{ $offer->id }}" class="d-block mb-0" name="offer_detail" id="offer_detail">
                                            <h5 class="mb-0"><b>{{ $offer->title}}</b></h5>
                                        </a>
                                        {{-- <p class="mb-0"><small class="text-muted">FEE 0</small></p> --}}
                                        {{-- <p class="mb-0"><small class="text-muted">15 Post, 154 Following</small></p> --}}
                                        <div class="d-flex justify-content-between align-items-center">
                                            <div class="d-flex align-items-center">
                                            <span class="badge badge-success">{{ (date("Y-m-d")>$offer->to)?'Not Available':'Available' }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--/offer item-->
                                @endforeach
                                
                                
                                <!--/offer item-->
                                <!-- /Offers list item -->
                            </div>
                            <!-- Load more -->
                            {{-- <div class="my-3">
                                <div class="clearfix"></div>
                                <div class="container text-center">
                                    <button type="button" class="btn btn-primary btn-rounded btn-sm btn-gred"> <i class="fa fa-spinner fa-pulse"></i>Load more offers</button>
                                </div>
                            </div> --}}
                            <!-- /Load more -->
                        </div>
                    </div>
                    <!-- /Page Content -->
                </div>
            </div>
        </div>
        <!-- partial -->
    </div>
</div>
{{-- <div class="content-wrapper bg-gray">
    <div class="container">
        <!--Companies list-->
        <div class="row companies-list">
            <div class="col-12 p-3">
                <h4 class="mb-2">
                    YOU MAY LIKE
                </h4>
            </div>
           
        </div>
        <!--/Companies list-->
    </div>
</div> --}}
<!-- content-wrapper ends -->

<!-- row-offcanvas ends -->
<!-- Create Offer Modal start -->
<div class="modal fade" id="create_offer" tabindex="-1" role="dialog" aria-labelledby="create_offer" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                <div class="content">
                    <h3 class="col-12 font-weight-light text-center mb-3">Add new offer</h3>
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="container">

                            <form class="forms-sample" id="add_new_offer_form" action="/offers" method="post" files="true" enctype="multipart/form-data" >
                                @csrf
                                <div class="form-group">
                                    <label for="exampleInputName1">Offer Tittle</label> 
                                    {!! Form::text('title',null,['class'=>'form-control form-input','id'=>'title','required'=>'required']) !!}
                                    <span class="invalid-feedback" name="name-feedback">
                                        <strong name="title-error_txt">{{ $errors->first('title') }}</strong>
                                    </span>
                                </div>
                                <div class="row">
                                    <div class="form-group col-6 offer-select-category">
                                        <label for="offer_category">Offer Category</label>
                                        <select class="form-control js-example-basic-single-3 form-input" name="category_id" id="category_id" required>
                                            @foreach(\App\Category::all() as $category)
                                               <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endforeach
                                        </select>
                                        <span class="invalid-feedback" name="name-feedback">
                                            <strong name="category_id-error_txt">{{ $errors->first('category_id') }}</strong>
                                        </span>
                                    </div>

                                    <div class="form-group col-6">
                                        <label>Offer Country</label> 
                                        {!! Form::select('countries_id',\App\Country::all()->pluck('name','id'), null, ['class' => 'form-control form-input','id'=>'countries_id' ,'required'=>'required']) !!}
                                        {{-- <small class="help-block"></small> --}}
                                        <span class="invalid-feedback" name="name-feedback">
                                            <strong name="countries_id-error_txt">{{ $errors->first('countries_id') }}</strong>
                                        </span>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Price</label> {!! Form::number('price',null,['class'=>'form-control form-input','placeholder'=>'50$','id'=>'price','required'=>'required']) !!}
                                        {{-- <small class="help-block"></small> --}}
                                        <span class="invalid-feedback" name="name-feedback">
                                            <strong name="price-error_txt">{{ $errors->first('price') }}</strong>
                                        </span>
                                    </div>

                                    <div class="form-group col-6">
                                        <label>Minimum Number Of peices</label> {!! Form::number('minimum_of_entities',null,['class'=>'form-control form-input','placeholder'=>'50$','id'=>'minimum_of_entities','required'=>'required']) !!}
                                        {{-- <small class="help-block"></small> --}}
                                        <span class="invalid-feedback" name="name-feedback">
                                            <strong name="minimum_of_entities-error_txt">{{ $errors->first('minimum_of_entities') }}</strong>
                                        </span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label>Delivery Time</label>
                                    <div id="clockpicker" class="input-group clockpicker form-input p-0" data-placement="top" data-align="top">
                                        <input type="text" class="form-control" name="delivery_time" required>
                                        <span class="input-group-addon input-group-text">
                                            <i class="mdi mdi-clock"></i>
                                        </span>
                                        <span class="invalid-feedback" name="name-feedback">
                                            <strong name="from-error_txt">{{ $errors->first('delivery_time') }}</strong>
                                        </span>
                                       
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Starting From</label>
                                        <div id="datepicker-popup-start" class="input-group date datepicker form-input p-0">
                                            <input type="text" class="form-control" name="from" required>
                                            <div class="input-group-addon input-group-text">
                                                <span class="mdi mdi-calendar"></span>
                                            </div>
                                            <span class="invalid-feedback" name="name-feedback">
                                                <strong name="from-error_txt"></strong>
                                            </span>
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="form-group col-6">
                                        <label>End in</label>
                                        <div id="datepicker-popup-end" class="input-group date datepicker form-input p-0">
                                            <input type="text" class="form-control" name="to" required>
                                            <div class="input-group-addon input-group-text">
                                                <span class="mdi mdi-calendar"></span>
                                            </div>
                                            <span class="invalid-feedback" name="name-feedback">
                                                <strong name="from-error_txt"></strong>
                                            </span>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <hr>
                                <div class="data-sheet">
                                    <h4 class="card-title">Data sheet</h4>
                                    <p class="card-description">offer materials</p>
                                    <div class="form-inline repeater">
                                        <div data-repeater-list="group-a">
                                            <div data-repeater-item class="d-md-flex mb-2">
                                                <label class="sr-only" for="inlineFormInputGroup1">Key</label>
                                                <div class="input-group mb-2 mr-md-2 mb-md-0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                              @
                                                            </span>

                                                    </div>
                                                    <input type="text" name="materials_keys[][keys]" class="form-control form-control-sm" id="inlineFormInputGroup1" placeholder="eg:Color">

                                                </div>
                                                <label class="sr-only" for="inlineFormInputGroup2">Value</label>
                                                <div class="input-group mb-2 mr-md-2 mb-md-0">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                              @
                                                            </span>
                                                    </div>
                                                    <input type="text" name="materials_keys[][values]" class="form-control form-control-sm" id="inlineFormInputGroup2" placeholder="eg:Red">

                                                </div>
                                                <button data-repeater-delete type="button" class="btn btn-danger btn-xs icon-btn ml-2">
                                                      <i class="mdi mdi-delete"></i>
                                                    </button>
                                            </div>
                                        </div>
                                        <button data-repeater-create type="button" class="btn btn-info btn-xs icon-btn ml-2 mb-2">
                                              <i class="mdi mdi-plus"></i>
                                            </button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="custom-file">
                                        <input type="file" name="photos[]" multiple accept='image/*' class="custom-file-input" id="customFile">
                                        <label class="custom-file-label" for="customFile">Upload offer images</label>
                                        <span class="invalid-feedback" name="name-feedback">
                                            <strong name="from-error_txt"></strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea rows="3" class="form-control form-input" placeholder="OFFER DESCRIPTION" name="description"></textarea>
                                    <small class="help-block"></small>
                                </div>
                                <!--
                                    <div class="form-group">
                                        <input type="file" class="dropify" data-max-file-size="2000kb" name="photo" />
                                        <small class="help-block"></small>
                                    </div>
                                -->
                                <button type="submit" class="btn btn-primary btn-rounded btn-sm btn-gred">Submit</button>
                                <button class="btn btn-primary btn-rounded btn-sm">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Create Offer Modal Ends -->

@include('website.modals.editProfile') @endsection