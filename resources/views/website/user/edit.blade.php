@extends('front.main')
@section('content')
<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        <!--navbar header for mobile-->
        
        <!--/navbar header for mobile-->
        <nav class="container-fluid bg-gray p-2">
            <div class="container">
                <div class="row">
                    <div class="d-flex col-lg-7 col-md-8 col-sm-12 text-md-left text-sm-center text-xs-center">
                        <h4 class="d-flex p-1 mb-0">
                            {{$user->name}}
                            <small class="d-flex text-muted ml-3 my-auto mx-xs-0">
                                <div id="connection" class="m-1">
                                    <span id="connections">163</span> Connections
                                </div>
                                <i> . </i>
                                <div id="connection" class="m-1">
                                    <a href="/followers"><span id="followers">{{count($user->follower)}}</span> Followers</a>
                                </div>
                                <i> . </i>
                                <div id="connection" class="m-1">
                                   <a href="/followings"> Following <span id="followers">{{count($user->following)}}</span></a>
                                </div>                                     
                            </small>
                        </h4>
                    </div>
                    {{-- <div class="col-lg-2 col-md-4 col-sm-12">
                        <div class="text-lg-right text-md-right text-sm-center text-xs-center my-xs-2">
                            <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5">Follow</button>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12 col-sm-12">
                        <div class="input-group float-right">
                            <div class="input-group-prepend text-center">
                                <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                            </div>
                            <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                        </div>
                    </div> --}}
                </div>
            </div>
        </nav>
        <div class="profile-about">
            <div class="cover-img" style="background-image: url(images/faces/cover.jpg)">
                <a data-toggle="modal" data-target="#edit_cover" class="edit-cover">
                    <i class="fa fa-camera-retro"></i>
                    <span class="d-xs-none">Edit Cover Picture</span>
                </a>
            </div>
            <div class="profile-img">
                <img src="/front_assets/images/faces/face1.jpg" class="rounded-circle" alt="image">
                <a data-toggle="modal" data-target="#edit_my_profile" class="edit-profile">
                    <i class="fa fa-camera-retro"></i>
                </a>
            </div>
            <div class="container mt--3">
                {!! Form::open(['url'=>url('profile/'.$user->id),'method'=>'put' ,'id'=>'edit_profile']) !!}
               
                    <div class="row text-left">
                        <div class="col-lg order-lg-1 order-md-2 px-4">
                            <div class="form-group">
                            <input type="text" class="form-control form-input" name="facebook_url" placeholder="Facebook" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->facebook_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" name="twitter_url" placeholder="Twitter" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->twitter_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" placeholder="Linkedin" name="linkedin_url" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->linkedin_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" placeholder="Behance" name="behance_url" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->behance_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" placeholder="Dribble" name="drobble_url" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->drobble_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" placeholder="GitHub" name="github_url" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->github_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" placeholder="Website" name="website_url" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->website_url:''}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control form-input" placeholder="Location" name="location" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->location:''}}"/>
                            </div>
                        </div>
                        <div class="col-lg order-lg-2 order-md-1 px-4">
                            <div class="form-group mb-4">
                                <label for="" class="ml-2 text-muted">First Name</label>
                                <input type="text" class="form-control form-input" placeholder="Enter your name" name="first_name" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->first_name:''}}"/>
                            </div>
                            <div class="form-group mb-4">
                                <label for="" class="ml-2 text-muted">Last Name</label>
                                <input type="text" class="form-control form-input" placeholder="Enter your last name" name="last_name" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->last_name:''}}"/>
                            </div>
                            <div class="form-group mb-4">
                                <label for="" class="ml-2 text-muted">Professional Title</label>
                                <input type="text" class="form-control form-input" placeholder="Enter your job title" name="profissional_title" value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->profissional_title:''}}"/>
                            </div>
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">About Me</label>
                                <textarea type="text" rows="7" class="form-control form-input" name="about_me" placeholder="Enter a paragraph about you.." value="{{(\Auth::user()->individual_user_info)?\Auth::user()->individual_user_info->about_me:''}}"></textarea>
                            </div>
                        </div>
                        <div class="col-lg order-lg-3 order-md-3 px-4">
                            <div class="form-group">
                                <label for="" class="ml-2 text-muted">Intrests</label>
                                {!! Form::select('intrests[]',\App\Intrest::all()->pluck('name','id'), $intrests, ['class' => 'form-control form-input select2','multiple'=>'multiple']) !!}
                                
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg"></div>
                        <div class="col-lg-4 mx-auto">
                            <div class="d-flex justify-content-between align-items-center px-4 py-3">
                                <div class="mr-auto">
                                    <button type="submit" class="btn btn-light btn-rounded btn-fw px-4">Cancel</button>
                                </div>
                                <div class="ml-auto">
                                    <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5" name="save_btn">Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg"></div>
                    </div>
                {!! Form::close()!!}
                <hr>
            </div>
        </div>
        
    </div>
    <!-- row-offcanvas ends -->
</div>
@include('website.modals.editProfile')
@endsection