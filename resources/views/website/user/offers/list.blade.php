@extends('front.main') 
@section('content')
<div class="container-fluid page-body-wrapper">
    <div class="row row-offcanvas row-offcanvas-right">
        <div class="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">My Cart</h4>
                                <p class="card-description">
                                </p>
                                <div class="table-responsive">
                                    <table class="table text-center table-dark table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Thumbnail</th>
                                                <th>Offer Title</th>
                                                <th>Offer Owner</th>
                                                <th>Added Date</th>
                                                <th>Price</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach(\Auth::user()->cart as $key=>$item)
                                                <tr id="item_{{$item->id}}">
                                                    <td>{{++$key}}</td>
                                                    <td>
                                                        @if($item->offer->images()->first())
                                                            <img src="{{$item->offer->images()->first()->getUrl()}}" alt="" name="offer_img">
                                                        @else
                                                            <img src="/front_assets/images/products/anonymous-offer.jpg" alt="" name="offer_img">
                                                        @endif
                                                    </td>
                                                    <td>{{$item->offer->title}}</td>
                                                    <td>{{$item->offer->company->name}}</td>
                                                    <td>{{$item->created_at->format('Y-m-d')}}</td>
                                                    <td>{{$item->offer->price}}$</td>
                                                    <td>
                                                        <a href="{{URL::to('offerDetailes/'.$item->offer_id)}}" class="btn btn-light btn-sm btn-rounded" name="show-offer_link">Show</a>
                                                        <a href="#" class="btn btn-danger btn-sm btn-rounded delete_item" name="delete-offer_link" cartID="{{$item->offer_id}}" userID="{{$item->user_id}}" data-toggle="modal" data-url="{!! URL::route('cart-item-delete', $item->id) !!}" data-id="{{$item->id}}" data-target="#custom-width-modal">Remove</a>
                                                                                            

                                                    </td>
                                                </tr>
                                            @endforeach
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Delete offer link Modal start -->
<div class="modal fade remove_offer" id="custom-width-modal"  tabindex="-1" role="dialog" aria-labelledby="remove_offer" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
                <form method="POST" action="#" class="remove_offer_form">
                    @csrf
                    <div class="form-group text-center">
                        <h3>Are you sure?</h3>
                    </div>
                    <div class="form-group mb-0">
                        <div class="col-md-12 text-center">
                            <button  class="btn btn-primary submit_query" name="submit">
                                   Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Delete offer link Modal Ends -->
@endsection
