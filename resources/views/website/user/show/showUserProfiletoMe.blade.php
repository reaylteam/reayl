@extends('front.main')
@section('content')

<!--/navbar header for mobile-->
                <nav class="container-fluid bg-gray p-2">
                    <div class="container">
                        <div class="row">
                            <div class="d-flex col-lg-7 col-md-8 col-sm-12 text-md-left text-sm-center text-xs-center">
                                {{-- <h4 class="d-flex p-1 mb-0" name="user-name_txt">
                                    {{$user->name}}
                                    <small class="d-flex text-muted ml-3 my-auto">
                                        
                                        <div id="connection" class="m-1">
                                            <span id="followers" name="followers-count_txt">{{count($user->follower)}}</span> Followers
                                        </div>
                                        <i> . </i>
                                        <div id="connection" class="m-1">
                                           Following <span id="followers">{{count($user->following)}}</span>
                                        </div>                                        
                                    </small>
                                </h4> --}}
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-12">
                                <div class="text-lg-right text-md-right text-sm-center text-xs-center my-xs-2">
                                    <button type="submit" class="btn btn-primary btn-rounded btn-fw btn-gred px-5">Follow</button>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-12 col-sm-12">
                                <div class="input-group float-right">
                                    <div class="input-group-prepend text-center">
                                        <span class="input-group-text border-0 search-i" id="basic-addon1"><i class="fa fa-search"></i></span>
                                    </div>
                                    <input type="text" class="form-control border-0 search-top" placeholder="KEYWORD SEARCH" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div class="profile-about">
                	@if($cover->first())
                    	<div class="cover-img" style="background-image: url({{ $cover->first()->getUrl() }})"></div>
                    @else
                    	<div class="cover-img" style="background-image: url(/front_assets/images/brands/cover-1.jpg)"></div>
                    @endif
                    <div class="profile-img">
                    	@if($profile->first())
                        	<img src="{{ $profile->first()->getUrl() }}" class="rounded-circle" alt="image">
                        @else
                        	<img src="/front_assets/images/brands/udacity.png" class="rounded-circle" alt="image">
                        @endif
                    </div>
                    <div class="container mt--3">
                        <div class="row text-center">
                            <div class="col-lg order-lg-1 order-md-2">
                                <div class="social-icons text-center">
                                    <a href="#"><i class="fa fa-facebook bg-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter bg-twitter"></i></a>
                                    <a href="#"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                    <a href="#"><i class="fa fa-behance bg-behance"></i></a>
                                    <a href="#"><i class="fa fa-dribbble bg-dribbble"></i></a>
                                </div>
                                <div class="text-center mt-3">
                                    <span class="d-block text-muted my-2">
                                      <i class="fa fa-globe"></i>
                                      <span>www.udacity.com</span>
                                    </span>
                                    <span class="d-block text-muted my-2">
                                      <i class="fa fa-map-marker text-gray"></i>
                                      <span>Egypt , Giza , Almohandsin</span>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-5 order-lg-2 order-md-1">
                                <p class="text-muted">{{$user->individual_user_info->about_me}}</p>
                                <div class="d-flex justify-content-between align-items-center px-4 py-3">
                                    <div class="mr-auto">
                                        <button type="submit" onclick="document.location.href='/postsOf/{{$user->name}}/{{$user->id}}/all'" class="btn btn-light btn-rounded btn-fw px-lg-4">Show All Posts</button>
                                    </div>
                                    <div class="ml-auto">
                                        <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-lg-5">Hire me</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg order-lg-3 order-md-3">
                                <div class="tags">
                                	@foreach($user->tags as $tag)
                                    	<a class="badge badge-primary badge-pill tag-item" href="#">{{$tag->name}} <span>65</span></a>
                                    @endforeach
                                   
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <!-- Page Content -->
                            <div class="col-lg-9 col-md-12 col-sm-12 pr-4 px-xs-0">
                                <div class="container pr-4  px-xs-0">
                                    <!-- Newsfeeds -->
                                    <div class="mb-5">
                                        @include('website.user.posts.list')
                                        <div class="clearfix"></div>
                                        <!-- load more -->
                                        <div class="container text-center">
                                            <a href="#" class="load-more mx-auto">
                                                <span>Load More Post</span>
                                                <i class="fa fa-ellipsis-h"></i>
                                              </a>
                                        </div>
                                    </div>
                                    <!-- /Newsfeeds -->
                                </div>
                            </div>
                            <!-- /Page Content -->
                            <!-- Right Sidebar -->
                            <div class="right-sidebar col-lg-3 col-md-12 col-sm-12 pl-4 px-xs-0">
                                <!-- wedget -->
                                @include('website.partials.job_list')
                                <!-- /wedget -->
                                <hr class="p-2">
                                <!-- wedget -->
                                @include('website.partials.new_offers')
                                <!-- /wedget -->
                            </div>
                            <!-- /Right Sidebar -->
                        </div>
                    </div>
                </div>
                <div class="content-wrapper bg-gray">
                    <div class="container">
                        <!--Companies list-->
                        <div class="row companies-list">
                            <div class="col-12 p-3">
                                <h4 class="mb-2">
                                    YOU MAY LIKE
                                </h4>
                            </div>
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                                          </a>
                                        <h6 class="d-block my-2">Amazone</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                              <img  class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                                          </a>
                                        <h6 class="d-block my-2">Dove</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Servicam</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="images/brands/amazon.jpg" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Amazone</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="images/brands/dove.gif" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Dove</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                            <!--company-list-item-->
                            <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
                                <div class="card text-center bg-gray">
                                    <div class="avatar">
                                        <a href="#">
                                      <img class="mb-4 img-lg rounded border" src="images/brands/servicam.jpg" alt="">
                                  </a>
                                        <h6 class="d-block my-2">Servicam</h6>
                                        <small class="d-block text-muted">Food, Pizza</small>
                                        <small class="d-block text-muted">15 Post, 154 Following</small>
                                    </div>
                                </div>
                            </div>
                            <!--/company-list-item-->
                        </div>
                        <!--/Companies list-->
                    </div>
                </div>
                <!-- content-wrapper ends -->
@endsection
