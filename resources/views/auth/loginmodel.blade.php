<!-- Modal -->
<div class="modal fade" id="loginmodeltest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content border-0">
            <div class="modal-body p-0">
                <div class="content">
                    <div class="col-12">
                        <ul class="row nav nav-tabs tab-solid  tab-solid-primary" role="tablist">
                            <li class="col-6 nav-item text-center p-0">
                                <a class="nav-link active" id="login_tab1" data-toggle="tab" href="#login_cont1" role="tab" aria-controls="login_cont1" aria-selected="true"><i class="fa fa-sign-in"></i>Login</a>
                            </li>
                            <li class="col-6 nav-item text-center p-0">
                                <a class="nav-link" id="register_tab1" data-toggle="tab" href="#register_cont1" role="tab" aria-controls="register_cont1" aria-selected="false"><i class="fa fa-user-plus"></i>Register</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <div class="tab-content tab-content-solid w-100">
                            <div class="tab-pane fade show active" id="login_cont1" role="tabpanel" aria-labelledby="login_tab1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="auth auth-form-light text-left p-2">
                                                    <h2 class="text-center">Login</h2>
                                                    <p class="text-center text-muted mb-1">Choose one of the following sign in methods.</p>
                                                    <div class="text-center">
                                                        <a href="{{url('/auth/facebook/login')}}"><i class="fa fa-facebook bg-facebook"></i></a>
                                                        <a href="{{url('/auth/twitter/login')}}"><i class="fa fa-twitter bg-twitter"></i></a>
                                                        <a href="{{url('/auth/linkedin/login')}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                                        <a href="{{url('/auth/google/login')}}"><i class="fa fa-google-plus bg-google"></i></a>
                                                    </div>
                                                    <h4 class="text-center my-1">OR</h4>
                                                    <ul id="error"></ul>
                                                    <p class="text-center text-muted mb-1">Login with your email address</p>
                                                    <div id="errors"></div>
                                                    <form method="POST" class="pt-1" id="loginForm" action="{{ route('login') }}">
                                                        @csrf
                                                        <div class="col-lg order-lg-2 order-md-1 px-4">
                                                            <div class="form-group">
                                                                <input type="email" class="col-12 form-input" id="email" name="email" placeholder="Email address">
                                                                <i class="fa fa-envelope"></i> @if ($errors->has('email'))
                                                                <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">{{ $errors->first('email') }}</strong>
                                            </span> @endif
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="password" class="col-12 form-input" id="password" name="password" placeholder="Password">
                                                                <i class="fa fa-eye"></i> @if ($errors->has('password'))
                                                                <span class="invalid-feedback" name="password-feedback">
                                                <strong name="password-error_txt">{{ $errors->first('password') }}</strong>
                                            </span> @endif
                                                            </div>
                                                            <div class="mt-2 text-center">
                                                                <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5" id="LoginUser" name="LoginUser">Login</button>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <a class="mx-auto" href="{{ route('password.request') }}">
                                                                    Forgot Your Password?
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="register_cont1" role="tabpanel" aria-labelledby="register_tab1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="auth auth-form-light text-left p-2">
                                            <h2 class="text-center">Register</h2>
                                            <p class="text-center text-muted mb-1">Choose one of the following sign up methods.</p>
                                            <div class="text-center">
                                                <a href="{{url('/auth/facebook/register')}}"><i class="fa fa-facebook bg-facebook"></i></a>
                                                <a href="{{url('/auth/twitter/register')}}"><i class="fa fa-twitter bg-twitter"></i></a>
                                                <a href="{{url('/auth/linkedin/register')}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                                <a href="{{url('/auth/google/register')}}"><i class="fa fa-google-plus bg-google"></i></a>
                                            </div>
                                            <h4 class="text-center my-1">OR</h4>
                                            <p class="text-center text-muted mb-1">Sign up with your email address</p>
                                            <div id="regErrors">

                                            </div>
                                            <form method="POST" action="{{ route('register') }}" class="pt-1" id="registerForm">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="text" class="col-12 form-input" name="name" id="name" placeholder="User Name">
                                                    <i class="fa fa-user"></i>
                                                    <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">error message</strong>
                                            </span>
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" class="col-12 form-input" name="email" id="Regemail" placeholder="Email address">
                                                    <i class="fa fa-envelope"></i>
                                                    <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">error message</strong>
                                            </span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="col-12 form-input" name="password" id="Regpassword" placeholder="Password">
                                                    <i class="fa fa-eye reg"></i>
                                                    <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">error message</strong>
                                            </span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="col-12 form-input" name="password_confirmation" id="password_confirmation" placeholder="Confirm password">
                                                    <i class="fa fa-eye"></i>
                                                    <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">error message</strong>
                                            </span>
                                                </div>
                                                <div class="pl-2">
                                                    <span class="text-muted">
                                                    <input type="checkbox" name="agreed" class="">
                                                    I accept terms and conditions
                                                    <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">error message</strong>
                                            </span>
                                                  </span>
                                                </div>
                                                <div class="mt-2 text-center">
                                                    <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5">Register</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
