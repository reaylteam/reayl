@extends('layouts.app') 
@section('content')
<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4 mx-auto text-center">
                <h2 class="col-12 text-center mb-3">Password Recovery</h2>
                <div class="auth">
                    <h5 class="text-muted">Welcome back Mohammed</h5>
                    <p class="text-muted">In the fields below, Enter your new password:</p>
                    <form action="#" class="col-12 text-center">
                        <div class="form-group">
                            <input type="password" class="col-12 form-input" placeholder="Enter your new password" />
                            <i class="fa fa-eye reg"></i>
                            <span class="invalid-feedback" name="email-feedback">
                                <strong name="email-error_txt">error message</strong>
                            </span>
                        </div>
                        <div class="form-group">
                            <input type="password" class="col-12 form-input" placeholder="Confirm your new password" />
                            <i class="fa fa-eye reg"></i>
                            <span class="invalid-feedback" name="email-feedback">
                                <strong name="email-error_txt">error message</strong>
                            </span>
                        </div>
                        <button type="submit" class="btn btn-primary btn-rounded  btn-md btn-gred">Reset</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection