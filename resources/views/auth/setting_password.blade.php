@extends('front.main') @section('content')
<div class="container">
    <div class="row justify-content-center">
    
        <div class="col-md-4 mx-auto text-center">
            <h2 class="col-12 text-center mb-3">Secure Account</h2>
            <div class="auth">
                <div class="profile-image">
                    <img class="img-lg rounded-circle border-1" src="/front_assets/images/faces/anonymous-user.png" alt="image" id="sub_user_image">
                </div>
                <h5 class="text-muted">Welcome back <strong>{{$user->name}}</strong></h5>
                <p class="text-muted">In the fields below, Enter your password:</p>
                <form method="POST" action="/user/setting/password" class="col-12 text-center">
                    @csrf

                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <div class="form-group">
                        <input type="password" name="password" class="col-12 form-input" placeholder="Enter your password" />
                        <i class="fa fa-eye reg"></i>
                        <span class="invalid-feedback" name="email-feedback">
                                <strong name="email-error_txt">error message</strong>
                            </span>
                    </div>
                    <div class="form-group">
                        <input type="password"  name="password_confirmation" class="col-12 form-input" placeholder="Confirm your password" />
                        <i class="fa fa-eye reg"></i>
                        <span class="invalid-feedback" name="email-feedback">
                                <strong name="email-error_txt">error message</strong>
                            </span>
                    </div>
                    <button type="submit" class="btn btn-primary btn-rounded  btn-md btn-gred">Reset</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection