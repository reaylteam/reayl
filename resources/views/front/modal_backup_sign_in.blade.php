  <link rel="stylesheet" media="screen" href="/front_assets/css/screen.css">

 <!-- Sign in Modal start -->
  <div class="modal fade" id="sign_in" tabindex="-1" role="dialog" aria-labelledby="sign_in" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                <div class="row">
                    <h3 class="col-12 font-weight-light text-center mb-2">SIGN UP FOR FREE.</h3>
                    <div class="col-6 grid-margin stretch-card">
                        <div class="row w-100">
                            <div class="col-lg-12">
                                <div class="auth auth-form-light text-left p-2">
                                    <h2 class="text-center">Login</h2>
                                    <p class="text-center text-muted mb-1">Choose one of the following sign in methods.</p>
                                    <div class="text-center">
                                        <a href="{{url('/auth/facebook')}}"><i class="fa fa-facebook bg-facebook"></i></a>
                                        <a href="{{url('/auth/twitter')}}"><i class="fa fa-twitter bg-twitter"></i></a>
                                        <a href="{{url('/auth/linkedin')}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                        <a href="{{url('/auth/google')}}"><i class="fa fa-google-plus bg-google"></i></a>
                                    </div>
                                    <h4 class="text-center my-1">OR</h4>
                                    <ul id="error"></ul>
                                    <p class="text-center text-muted mb-1">Login with your email address</p>
                                    <div id="errors"></div>
                                    <form method="POST" class="pt-1" id="loginForm" action="{{ route('login') }}">
                                       @csrf
                                       <div class="col-lg order-lg-2 order-md-1 px-4">
                                           <div class="form-group">
                                            <input type="email" class="col-12 form-input" id="email" name="email" placeholder="Email address">
                                            <i class="fa fa-envelope"></i>
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback" name="email-feedback">
                                                <strong name="email-error_txt">{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="col-12 form-input" id="password" name="password" placeholder="Password">
                                            <i class="fa fa-eye"></i>
                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback" name="password-feedback">
                                                <strong name="password-error_txt">{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                        <div class="mt-2 text-center">
                                            <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5"  id="LoginUser" name="LoginUser">Login</button>
                                        </div>
                                       </div>                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 grid-margin stretch-card border-left">
                        <div class="row w-100">
                            <div class="col-lg-12">
                                <div class="auth auth-form-light text-left p-2">
                                    <h2 class="text-center">Register</h2>
                                    <p class="text-center text-muted mb-1">Choose one of the following sign up methods.</p>
                                    <div class="text-center">
                                        <a href="{{url('/auth/facebook')}}"><i class="fa fa-facebook bg-facebook"></i></a>
                                        <a href="{{url('/auth/twitter')}}"><i class="fa fa-twitter bg-twitter"></i></a>
                                        <a href="{{url('/auth/linkedin')}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                        <a href="{{url('/auth/google')}}"><i class="fa fa-google-plus bg-google"></i></a>
                                    </div>
                                    <h4 class="text-center my-1">OR</h4>
                                    <p class="text-center text-muted mb-1">Sign up with your email address</p>
                                    <div id="regErrors">
                                        
                                    </div>
                                    <form method="POST" action="{{ route('register') }}" class="pt-1" id="registerForm">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" class="col-12 form-input" name="name" id="name" placeholder="User Name">
                                        </div>
                                       
                                        <div class="form-group">
                                            <input type="email" class="col-12 form-input" name="email" id="Regemail" placeholder="Email address">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="col-12 form-input" name="password" id="Regpassword" placeholder="Password">
                                             <i class="fa fa-eye reg"></i>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="col-12 form-input" name="password_confirmation" id="password_confirmation" placeholder="Confirm password">
                                             <i class="fa fa-eye"></i>
                                        </div>
                                        <div class="mt-2 w-100 mx-auto text-left">
                                            <small class="text-muted">
                                                    <input type="checkbox" name="agreed" class="">
                                                    I accept terms and conditions
                                                  </small>
                                        </div>
                                        <div class="mt-2 text-center">
                                            <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5">Register</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <!-- Sign in Modal Ends -->
@section('footer_after')

@include('front/validate_files')
<script type="text/javascript">
  var loginForm = $("#loginForm");
  loginForm.submit(function(e){
    e.preventDefault();
    var formData = loginForm.serialize();

    $.ajax({
        url:'/login',
        type:'POST',
        data:formData,
        success:function(data){
            window.location = '/';
        },
        error: function (data) {
            var errors = data.responseJSON;
            console.log(errors);
            errorsHtml = '<div class="alert alert-danger"><ul>';
            if(errors.email)
                    errorsHtml += '<li>' + errors.email + '</li>'; 

            $.each( errors.errors, function( key, value ) {
                errorsHtml += '<li>' + value + '</li>'; 

            });
            errorsHtml += '</ul></di>';
            
        $( '#errors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
    }
});
});
  //signup logic
  var signUpForm = $("#registerForm");
  signUpForm.submit(function(e){
    e.preventDefault();
    var formData = signUpForm.serialize();

    $.ajax({
        url:'/register',
        type:'POST',
        data:formData,
        success:function(data){
            window.location = '/';
        },
        error: function (data) {
            var errors = data.responseJSON;
            console.log(errors);
            errorsHtml = '<div class="alert alert-danger"><ul>';

            $.each( errors, function( key, value ) {
                errorsHtml += '<li>' + value + '</li>'; 

            });
            errorsHtml += '</ul></di>';
            
        $( '#regErrors' ).html( errorsHtml ); //appending to a <div id="form-errors"></div> inside form
    }
});
});

</script>
@endsection