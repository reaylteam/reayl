 <nav class="sidebar sidebar-offcanvas d-lg-none" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item nav-profile">
                            <div class="nav-link">
                                <div class="profile-image">
                                    <!--<img src="images/faces/face1.jpg" alt="image">-->
                                    <span class="span-sm rounded-circle mr-1">M</span>
                                </div>
                                <div class="profile-name">
                                    <p class="name">
                                        Mohamed Mamdouh
                                    </p>
                                    <p class="designation">
                                        Normal User
                                    </p>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                            <i class="fa fa-pencil-square-o mr-2"></i>
                          Offers
                        </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                            <i class="fa fa-building mr-2"></i>
                          Companies
                        </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#user-pages" aria-expanded="false" aria-controls="user-pages">
                            <i class="icon-user menu-icon"></i>
                            <span class="menu-title">Settings</span>
                          </a>
                            <div class="collapse" id="user-pages" style="">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link" href="#"> <i class="mr-2 fa fa-cog"></i>Edit profile </a></li>
                                    <li class="nav-item"> <a class="nav-link" href="#"> <i class="mr-2 fa fa-sign-out"></i>Sign out </a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#language" aria-expanded="false" aria-controls="language">
                            <i class="icon-globe menu-icon"></i>
                            <span class="menu-title">Language</span>
                          </a>
                            <div class="collapse" id="language">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link" href="#"><i class="mr-2 flag-icon flag-icon-gb"></i>
                                English</a></li>
                                    <li class="nav-item"> <a class="nav-link" href="#"><i class="mr-2 flag-icon flag-icon-sa"></i>
                                Arabic</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </nav>