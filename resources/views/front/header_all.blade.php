<div class="container-fluid top-banner">
    @include('front.ajaxerror')
            
            <div class="row row-offcanvas row-offcanvas-right">
                <!--navbar header for mobile-->
                <nav class="sidebar sidebar-offcanvas d-lg-none" id="sidebar" name="user-navitem">
                    <ul class="nav" name="nav_list">
                    @if(\Auth::check())
                    
                    @if(\Auth::check())
                        @if(\Auth::user()->level == 'business')
                            @include('front.business_user_dropdown')
                        @else
                            @include('front.user_dropdown')
                        @endif
                    @else
                        <li class="nav-item dropdown d-lg-block d-md-none d-sm-none d-xs-none" name="login-navitem">
                                <a class="nav-link" href="#" data-toggle="modal" data-target="#sign_in" name="login-popup_link">
                                  Login
                                </a>
                        </li>
                    @endif
                        <li class="nav-item" name="posts-navitem">
                            <a class="nav-link" href="#" name="posts_link">
                            <i class="fa fa-pencil-square-o mr-2 {{Request::is('offers*')?'active':''}}" name="posts_icon"></i>
                          Offers
                        </a>
                        </li>
                        <li class="nav-item" name="companies-navitem">
                            <a class="nav-link" href="#" name="companies_link">
                            <i class="fa fa-building mr-2 {{Request::is('companies*')?'active':''}}" name="companies_icon"></i>
                          Companies
                        </a>
                        </li>
                        <li class="nav-item dropdown d-lg-flex d-md-none d-sm-none d-xs-none">
                        <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
                          <i class="fa fa-globe"></i>
                          Language
                        </a>
                        <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        
                            <a class="dropdown-item font-weight-medium" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                <i class="flag-icon {{  $properties['flag'] }}"></i>
                                {{ $properties['native'] }}
                            </a>
    
                            <div class="dropdown-divider"></div>
                        
                        @endforeach
                        </div>
                    </li>
                    </ul>
                </nav>
                <!--/navbar header for mobile-->
            </div>
            @yield('content')
        </div>