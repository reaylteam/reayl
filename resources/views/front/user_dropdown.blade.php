<li class="nav-item dropdown d-none d-lg-flex">
    <a class="nav-link dropdown-toggle" id="AccountDropdown" href="#" data-toggle="dropdown">
        @if(\Auth::user()->profile())
            <img class="img-xs rounded-circle" src="{{ \Auth::user()->profile()->getUrl() }}">
        @else
            <img class="img-xs rounded-circle" src="/front_assets/images/faces/anonymous-user.png">
        @endif
<!--        <span class="span-sm rounded-circle mr-1">{{\Auth::user()->name[0]}}</span>-->
         {{\Auth::user()->name}}
    </a>
    <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
        <a class="dropdown-item font-weight-medium" href="{{URL::to('/newsfeed')}}">
                                    <i class="fa fa-user"></i>
                                    Newsfeed
                                  </a>
        <a class="dropdown-item font-weight-medium" href="{{URL::to('/cart/list/'.\Auth::user()->id.'/'.\Auth::user()->name)}}">
                                    <i class="fa fa-shopping-cart"></i>
                                    My Cart
                                  </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item font-weight-medium" href="#" data-toggle="modal" data-target="#edit_my_profile" class="edit-profile" name="profilepicture-edit_link">
            <i class="fa fa-camera-retro"></i>
            Change photo
        </a>
        <div class="dropdown-divider"></div>

        <a class="dropdown-item font-weight-medium" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out"></i>
                                        {{ __('Logout') }}
                                    </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</li>
<li class="nav-item d-lg-block d-md-none d-sm-none d-xs-none">
    <a href="#" data-toggle="modal" data-target="#business_terms" name="business_terms_link" id="business_terms_link" class="btn btn-light btn-rounded btn-md border-0">I'm a Business</a>
</li>