<li class="nav-item dropdown d-none d-lg-flex" name="dropdown-profile_list">
    <a class="nav-link dropdown-toggle" id="AccountDropdown" href="#" data-toggle="dropdown" name="profile-settings_link">
        @if(\Auth::user()->profile())
            <img class="img-xs rounded-circle" src="{{ \Auth::user()->profile()->getUrl() }}">
        @else
            <img class="img-xs rounded-circle" src="/front_assets/images/brands/anonymous-company.png" alt="">
        @endif
<!--        <span class="span-sm rounded-circle mr-1" name="firtchar-username_txt">{{\Auth::user()->name[0]}}</span>-->
        {{\Auth::user()->name}}
    </a>
    <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown" >
      @if(\Auth::user()->parent)
        <a class="dropdown-item font-weight-medium" href="{{URL::to('/profile/'.\Auth::user()->parent->id.'/'.\Auth::user()->parent->name)}}" name="user-profile_link">
            <i class="fa fa-user" name="user-profile_icon"></i>
            My profile
          </a>
          <a class="dropdown-item font-weight-medium" href="{{URL::to('/users/'.\Auth::user()->parent->id.'/'.\Auth::user()->parent->name.'/list')}}" name="user-dashboard_link">
            <i class="fa fa-user" name="user-dashboard_icon"></i>
              Users Dashboard
          </a>
      @else
        <a class="dropdown-item font-weight-medium" href="{{URL::to('/profile/'.\Auth::user()->id.'/'.\Auth::user()->name)}}" name="user-profile_link">
            <i class="fa fa-user" name="user-profile_icon"></i>
            My profile
        </a>
        <a class="dropdown-item font-weight-medium" href="{{URL::to('/users/'.\Auth::user()->id.'/'.\Auth::user()->name.'/list')}}" name="user-dashboard_link">
            <i class="fa fa-user" name="user-profile_icon"></i>
              Business Dashboard
          </a>
      @endif
          
        <div class="dropdown-divider"></div>
        <a class="dropdown-item font-weight-medium" href="/myprofile/edit" name="edit-profile_link">
            <i class="fa fa-cog" name="edit-profile_icon"></i>
            Edit profile
          </a>
        <div class="dropdown-divider"></div>
        
          <a class="dropdown-item font-weight-medium" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();" name="logout_link">
                <i class="fa fa-sign-out" name="logout_iccon"></i>
                {{ __('Logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
    </div>
</li>