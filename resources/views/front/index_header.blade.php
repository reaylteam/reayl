{{-- <div class="alert alert-fill-success rounded-0 mb-0 p-2" role="alert" style="display:block">
    <i class="fa fa-exclamation-circle"></i> Well done! We successfully sent password reset link to your email, Click <a href="#">here</a> if there is an error!
    <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div> --}}

    @include('front.error')
   <div class="container-fluid top-banner">
    <!-- partial:navbar -->
    <nav class="navbar container-fluid col-lg-12 col-12 p-0  d-flex flex-row bg-transparent">
        <div class="navbar-menu-wrapper d-flex align-items-center container">
            <a class="navbar-brand brand-logo" href="/" name="home_link" id="home_link"><img src="/front_assets/images/logo.png" alt="logo" name="site-logo_img" id="site-logo_img" /></a>
            <ul class="navbar-nav navbar-nav-left ml-3 d-lg-flex d-md-none d-sm-none d-xs-none">
                <li class="nav-item dropdown d-none d-lg-flex main-menu-item">
                    <a class="nav-link {{Request::is('/offers')?'active':''}}" href="{{url('offers')}}" name="offers_link" id="offers_link">
                          Offers
                        </a>
                </li>
                <li class="nav-item dropdown d-none d-lg-flex main-menu-item">
                    <a class="nav-link {{Request::is('companies')?'active':''}}" href="/companies" name="companies_link" id="companies_link">
                          Companies
                        </a>
                </li>
            </ul>
            <ul class="navbar-nav navbar-nav-right">
                <li class="nav-item dropdown d-lg-flex d-md-none d-sm-none d-xs-none">
                    <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown" name="languageDropdown_link">
                          <i class="fa fa-globe"></i>
                          Language
                        </a>
                    <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                        <a class="dropdown-item font-weight-medium" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" name="languageChange_link" id="languageChange_link_{{$properties['code']}}">
                                    <i class="flag-icon {{  ($properties['flag'])}}" name="language-flag_icon"></i>
                                    {{ $properties['native'] }}
                                </a>

                        <div class="dropdown-divider"></div>

                        @endforeach
                    </div>
                </li>
                @if(\Auth::check()) @if(\Auth::user()->level == 'business') @include('front.business_user_dropdown') @else @include('front.user_dropdown') @endif @else
                <li class="nav-item dropdown d-lg-block d-md-none d-sm-none d-xs-none">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#sign_in" name="loginPopup_link" id="loginPopup_link">
                        Login
                        </a>
                </li>
                @endif
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                  <span class="icon-menu"></span>
                </button>
        </div>
    </nav>
    <div class="row row-offcanvas row-offcanvas-right">
        <!--navbar header for mobile-->
        <nav class="sidebar sidebar-offcanvas d-lg-none" id="sidebar">
            <ul class="nav">
                @if(\Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="/profile" name="profile_link">
                            <i class="fa fa-sign-in mr-2"></i>
                            My Profiile
                            </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#sign_in" name="signinPopup_link">
                            <i class="fa fa-sign-in mr-2"></i>
                            Login
                            </a>
                </li>
                @endif
                <li class="nav-item">
                    <a class="nav-link" href="#">
                            <i class="fa fa-hand-o-right mr-2"></i>
                          Setup My Business
                        </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Request::is('offers')?'active':''}}" href="/offers" name="offers_link">
                            <i class="fa fa-pencil-square-o mr-2"></i>
                                Offers
                        </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{Request::is('companies')?'active':''}}" href="/companies" name="companies_link">
                            <i class="fa fa-building mr-2"></i>
                                Companies
                        </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="collapse" href="#language" aria-expanded="false" aria-controls="language" name="languageDropdown_link">
                        <i class="icon-globe menu-icon"></i>
                        <span class="menu-title">Language</span>
                      </a>
                    <div class="collapse" id="language">

                        <ul class="nav flex-column sub-menu">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                            <li class="nav-item">
                                <a class="nav-link" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" name="languageChange_link" id="languageChange_link_{{$properties['code']}}"><i class="mr-2 flag-icon {{  ($properties['flag'])}}"></i>
                                            {{$properties['native']}}
                                        </a>
                            </li>
                            @endforeach


                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
        <!--/navbar header for mobile-->
    </div>
    <!-- partial -->
    <div class="row top-banner-content py-lg-5 py-md-3 py-sm-2 py-xs-1">
        <div class="col-md-10 mx-auto">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="mb-lg-5 col-10 mx-auto text-white text-ibmp">
                        Following companies in your work feild help you to improve your skills and grow your business
                    </h1>
                    <div class="row">
                        <div class="col-10 mx-auto index-search my-4 px-0">
                            <form action="/search/results/">
                                <div class="input-group">
                                    <input type="text" class="form-control search-input" placeholder="Search for Industries, Companies and Offers" name="search_key_word" />
                                    <select class="form-control js-example-basic-single" style="width: 20%" name="country" name="country">
                                          <option ></option>
                                        @foreach(\App\Country::all() as $country)
                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                        @endforeach
                                        </select>
                                    <input type="submit" class="btn btn-primary border-0 btn-fw btn-gred" value="SEARCH" name="submit_search" id="submit_search">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row  d-lg-block d-md-block d-sm-none d-xs-none">
                <div class="col-10 mb-2 mx-auto pl-0">
                    <h4 class="text-white">OUR CATEGORIES</h4>
                </div>
                <div class="categories-list row">
                    <div class="col-10 mx-auto">
                        <div class="row">
                            @foreach(\App\Category::all()->take(12) as $category)
                                <div class="col-lg-3 col-md-3 my-1">
                                <a class="text-white font-weight-light" href="/search/results/?search_key_word=&country=&category={{ $category->slug}}">
                                    <img src="/front_assets/images/brands/anonymous-category.png" class="img-sm rounded-circle" alt="">
                                    {{ $category->title }}</a>
                                </div>
                            @endforeach    
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="container-fluid">
    <div class="col-10 mx-auto row p-4 px-xs-0 align-items-center">
        <div class="col-10">
            <h4 class="mb-2" name="top-companies_txt">
                TOP COMPANIES
                
            </h4>
        </div>
    </div>
    <!--Companies list-->
    <div class="col-10 mx-auto row companies-list">
        @foreach($top_companies as $company)
        <!--company-list-item-->
        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 grid-margin stretch-card">
            <div class="card text-center">
                <div class="avatar">
                    <a href="{{URL::to('/profile/'.$company->id.'/'.$company->name)}}" name="business-user_link">
                        @if($company->profile())
                            <img class="img-lg rounded border" src="{{ $company->profile()->getUrl() }}" alt="" name="business-user_img">
                        @else
                            <img class="img-lg rounded border" src="/front_assets/images/brands/anonymous-company.png" alt="" name="business-user_img">
                        @endif
                        </a>
                    <h6 class="d-block my-2" name="businessuser-name_txt">{{$company->name}}</h6>
                    <small class="d-block" name="business-user-followers-and-following-count_txt">{{$company->posts->count()}} Post, {{$company->following->count()}} Following</small>
                </div>
            </div>
        </div>
        <!--/company-list-item-->
        @endforeach
        <div class="col-12 text-center" id="loadMoreBtn">
            <div class="clearfix"></div>
            <div class="container text-center" id="loadMoreBtn">
               <a href="{{URL::to('companies')}}" class="btn btn-primary btn-rounded btn-md btn-gred"> View all</a>
            </div>
        </div>
    </div>
    <!--/Companies list-->
    @include('website.partials.latest_offers')
</div>