{{-- @include('auth.loginmodel') --}}
<!--footer-->
<footer class="container-fluid footer">
    <div class="row pb-5 text-white footer-top">
        <div class=" mx-auto">
            <div class="row">
                <div class="col-md-12">
                    <div class="row contact-details">
                        <div class="col-12 text-center mb-3">
                            <h4 name="contactus_txt">CONTACT US</h4>
                        </div>
                        <div class="col-12 text-center mb-3">
                            <span class="font-weight-normal" name="contactus-description_txt">If you have a question, we have an answer. If you need something, we'll help you. Contact us anytime.</span>
                        </div>
                        <div class="col-12 text-center mb-3">
                            <a href="mailto:test@example.com" class="btn btn-primary btn-rounded border btn-lg btn-gred" name="contactus_btn">SEND MESSAGE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row p-4 footer-middle">
        <div class="mx-auto text-center">
            <div class="container-fluid clearfix">
                <ul class="nav my-0" name="footer-links_list">
                    <!-- @foreach($static_pages as $page)
                        <li class="nav-item"><a class="nav-link" href="{{URL::to('site/pages/'.$page->id)}}">{{$page->name}}</a></li>
                    @endforeach -->
                    @php
                        $about               = 'about';
                        $help_center         = 'help_center';
                        $terms_and_condition = 'terms_and_condition';
                        $privacy_policy      = 'privacy_policy';

                    @endphp
                    <li class="nav-item"><a class="nav-link" id="about-page_link" name="about-page_link" href="{{URL::to('/site/pages/'.$about)}}">About</a></li>
                    <li class="nav-item"><a class="nav-link" id="help-center_link"name="help-center_link" href="{{URL::to('/site/pages/'.$help_center)}}">Help center</a></li>
                    <li class="nav-item"><a class="nav-link" id="terms_and_condition_link" name="terms_and_condition_link" href="{{URL::to('/site/pages/'.$terms_and_condition)}}">Terms</a></li>
                    <li class="nav-item"><a class="nav-link" id="privacy_policy_link" name="privacy_policy_link" href="{{URL::to('/site/pages/'.$privacy_policy)}}">Privacy policy</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row p-3 footer-bottom">
        <div class="container mx-auto">
            <div class="clearfix">
                <div class="p-3 d-block text-center text-sm-left d-sm-inline-block">
                    © 2018 <a href="/" name="website-name_txt">Reayl</a> - All rights reserved.
                </div>
                <div class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">
                    <ul class="nav m-0 social d-sm-inline-flex d-xs-inline-flex" name="social-icons_list">
                        <li class="nav-item" name="social_icons"><a class="nav-link" href="#" name="socil_link"><i class="fa fa-facebook"></i></a></li>
                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                        <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/footer-->