@include('front.error')
<!-- partial:navbar -->
<nav class="navbar container-fluid col-lg-12 col-12 p-0 fixed-top d-flex flex-row" name="navbar-section">
    <div class="navbar-menu-wrapper d-flex align-items-center container">
        <a class="navbar-brand brand-logo" href="/" name="siteurl_link"><img src="/front_assets/images/logo.png" alt="logo" name="site-logo_img" /></a>
        <ul class="navbar-nav navbar-nav-left" name="site-navitems_list">
            <li class="nav-item dropdown d-none d-lg-flex main-menu-item {{Request::is('posts')?'active':''}}" name="post-navitem">
                <a class="nav-link" href="{{url('offers')}}" name="posts_link">
                  Offers
                </a>
            </li>
            <li class="nav-item dropdown d-none d-lg-flex main-menu-item {{Request::is('companies*')?'active':''}}" name="companies-navitem">
                <a class="nav-link" href="/companies" name="companies_link">
                  Companies
                </a>
            </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right ml-auto mr-xs-2" name="languageDropdown">
            <li class="nav-item dropdown d-lg-flex d-md-none d-sm-none d-xs-none">
                <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
                  <i class="fa fa-globe"></i>
                  Language
              </a>
              <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)

                <a class="dropdown-item font-weight-medium" rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                     <i class="flag-icon {{  ($properties['flag'])}}"></i> 
                    {{ $properties['native'] }}
                </a>

                <div class="dropdown-divider"></div>

                @endforeach
            </div>
        </li>
            @if(\Auth::check())
                @if(\Auth::user()->level == 'business')
                    @include('front.business_user_dropdown')
                @else
                    @include('front.user_dropdown')
                @endif
            @else
                <li class="nav-item dropdown d-lg-block d-md-none d-sm-none d-xs-none" name="login-navitem">
                        <a class="nav-link" href="#" data-toggle="modal" data-target="#sign_in" name="login-popup_link">
                          Login
                        </a>
                </li>
            @endif
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center px-xs-0" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
    </div>
</nav>
<!-- partial -->
