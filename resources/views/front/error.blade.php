@if (session('status'))
<div class="alert alert-fill-success rounded-0 mb-0 p-2" role="alert" style="display:block">
    <i class="fa fa-exclamation-circle"></i> {{ session('status') }}

    <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
@if (session('warning'))

<div class="alert alert-fill-danger rounded-0 mb-0 p-2" role="alert" style="display:block">
    <i class="fa fa-exclamation-circle"></i> {{ session('warning') }}

    <button type="button" class="close text-white" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
