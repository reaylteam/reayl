<!DOCTYPE html>
<html lang="en">
<head>
	@include('front/head')
	@yield('header_after')
</head>
<body class="horizontal-menu">   
	<div class="container-scroller landing-page">
		@include('front/index_header')
		<div class="row row-offcanvas row-offcanvas-right">
				@include('front/mobile_nav')
				@yield('content')
			</div>
		@include('front/footer')
    </div>
		@include('front/modal')
		@include('front/mainjs')
		@yield('footer_after')
	</body>
	</html>