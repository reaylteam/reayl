    <!-- plugins:js -->
    <script src="/front_assets/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="/front_assets/plugins/popper.js/dist/umd/popper.min.js"></script>
    <script src="/front_assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/front_assets/plugins/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
    <script src="/front_assets/plugins/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/front_assets/plugins/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <script src="/front_assets/plugins/select2/dist/js/select2.min.js"></script>
    <script src="/front_assets/plugins/dropify/dist/js/dropify.min.js"></script>
    <!-- plugins:js -->

    <script src="/front_assets/node_modules/jquery.repeater/jquery.repeater.min.js"></script>
    <script src="/front_assets/node_modules/owl-carousel-2/owl.carousel.min.js"></script>
    <script src="/front_assets/node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
    <script src="/front_assets/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="/front_assets/node_modules/clockpicker/dist/jquery-clockpicker.min.js"></script>
<!-- /plugins:js -->
    <script src="/front_assets/js/off-canvas.js"></script>
    <script src="/front_assets/js/dropify.js"></script>
    <script src="/front_assets/js/modal-demo.js"></script>
    <script src="/front_assets/js/wizard.js"></script>
    <script src="/front_assets/js/select2.js"></script>
    <script src="/front_assets/js/social.js"></script>
    <script src="/front_assets/js/owl.js"></script>
    <script src="/front_assets/js/rating.js"></script>
    <script src="/front_assets/js/form-repeater.js"></script>
    
    <!-- Custom js for this page-->
    

    <script>
        var user_id = {!! (\Auth::check())?\Auth::user()->id:'' !!};
    </script>
    <!-- End custom js for this page-->
    <script type="text/javascript">
    var loginForm = $("#loginForm");
    $(document).on('submit', '#loginForm', function(e) { 
        e.preventDefault();
        var formData = loginForm.serialize();
        var that=this;
        $('input+small').text('');
        $('input').parent().removeClass('has-error');
        $.ajax({
            url: '/login',
            type: 'POST',
            data: formData,
            dataType: "json"
        }) 
        .done(function(data) {
            location.reload();
        })
        .fail(function(data) {
            var errors = data.responseJSON;
                // console.log(errors);
                errorsHtml = '<div class="alert alert-danger"><ul>';
                if (errors.email)
                    errorsHtml += '<li>' + errors.email + '</li>';

                $.each(errors.errors, function(key, value) {
                    errorsHtml += '<li>' + value + '</li>';

                });
                errorsHtml += '</ul></di>';

                $('#errors').html(errorsHtml);
        });
    });
    //signup logic
    var signUpForm = $("#registerForm");
    $(document).on('submit', '#registerForm', function(e) {
        e.preventDefault();
        var formData = signUpForm.serialize();
        var that=this;
        $.ajax({
            url: '/register',
            type: 'POST',
            data: formData,
            dataType: "json"
        }) 
        .done(function(data) {
            location.reload();
        })
        .fail(function(data) {
            // console.log('reg here',data.responseJSON);
            $(".invalid-feedback").css('display','none');
            $.each(data.responseJSON, function (key, value) {
                var input = $(that).find('input[name=' + key + ']');
                // console.log('span',$(input).next().next().children())
                $(input).next().next().children().text(value)
                $(input).next().next().css('display','block')
            });
        });
    });
</script>
