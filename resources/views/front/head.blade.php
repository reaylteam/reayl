<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title name="site-title_txt">Reayl - Home</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- plugins:css -->
    <link rel="stylesheet" href="/front_assets/plugins/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="/front_assets/plugins/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="/front_assets/plugins/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
    <link rel="stylesheet" href="/front_assets/plugins/select2/dist/css/select2.min.css" />
    <link rel="stylesheet" href="/front_assets/plugins/select2-bootstrap-theme/dist/select2-bootstrap.min.css" />
    <link rel="stylesheet" href="/front_assets/plugins/jquery-tags-input/dist/jquery.tagsinput.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="/front_assets/plugins/font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/front_assets/plugins/dropify/dist/css/dropify.min.css" />
    <link rel="stylesheet" href="/front_assets/node_modules/owl-carousel-2/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="/front_assets/node_modules/owl-carousel-2/assets/owl.theme.default.min.css" />
    <link rel="stylesheet" href="/front_assets/node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css" />
    <link rel="stylesheet" href="/front_assets/node_modules/jquery-bar-rating/dist/themes/fontawesome-stars-o.css" />
    <link rel="stylesheet" type="text/css" href="/front_assets/css/statistics.css">
    <link rel="stylesheet" href="/front_assets/node_modules/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="/front_assets/node_modules/clockpicker/dist/jquery-clockpicker.min.css" />
    <link rel="stylesheet" href="/front_assets/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="/front_assets/css/style.css">
    <!-- Add 'style-rtl' file for right to left layout -->
    <!--<link rel="stylesheet" href="css/style-rtl.css">-->
    <!-- endinject -->
    <link rel="shortcut icon" href="/front_assets/images/favicon.png" />
</head>