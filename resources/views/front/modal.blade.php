<!-- Sign in Modal start -->
<div class="modal fade" id="sign_in" tabindex="-1" role="dialog" aria-labelledby="sign_in" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content border-0">
            <div class="modal-body p-0">
                <div class="content">
                    <div class="col-12">
                        <ul class="row nav nav-tabs tab-solid  tab-solid-primary" role="tablist">
                            <li class="col-6 nav-item text-center p-0">
                                <a class="nav-link active" id="login_tab" data-toggle="tab" href="#login_cont" role="tab" aria-controls="login_cont" aria-selected="true"><i class="fa fa-sign-in"></i>Login</a>
                            </li>
                            <li class="col-6 nav-item text-center p-0">
                                <a class="nav-link" id="register_tab" data-toggle="tab" href="#register_cont" role="tab" aria-controls="register_cont" aria-selected="false"><i class="fa fa-user-plus"></i>Register</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12">
                        <div class="tab-content tab-content-solid w-100">
                            <div class="tab-pane fade show active" id="login_cont" role="tabpanel" aria-labelledby="login_tab">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="auth auth-form-light text-left p-2">
                                                    <h2 class="text-center">Login</h2>
                                                    <p class="text-center text-muted mb-1">Choose one of the following sign in methods.</p>
                                                    <div class="text-center">
                                                        <a href="{{url('/auth/facebook')}}"><i class="fa fa-facebook bg-facebook"></i></a>
                                                        <a href="{{url('/auth/twitter')}}"><i class="fa fa-twitter bg-twitter"></i></a>
                                                        <a href="{{url('/auth/linkedin')}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                                        <a href="{{url('/auth/google')}}"><i class="fa fa-google-plus bg-google"></i></a>
                                                    </div>
                                                    <h4 class="text-center my-1">OR</h4>
                                                    <p class="text-center text-muted mb-1">Login with your email address</p>
                                                    <div id="errors"></div>
                                                    <form method="POST" class="pt-1" id="loginForm" action="{{ route('login') }}">
                                                        @csrf
                                                        <div class="col-lg order-lg-2 order-md-1 px-4">
                                                           <span class="invalid-feedback" name="email-feedback">
                                                                        <strong name="email-error_txt">{{ $errors->first('email') }}</strong>
                                                                    </span>
                                                            <div class="form-group">
                                                                <input type="email" class="col-12 form-input" id="email" name="email" placeholder="Email address">
                                                                <i class="fa fa-envelope"></i>
                                                            </div>
                                                            <span class="invalid-feedback" name="password-feedback">
                                                                        <strong name="password-error_txt">{{ $errors->first('password') }}</strong>
                                                                    </span>
                                                            <div class="form-group">
                                                                <input type="password" class="col-12 form-input" id="password" name="password" placeholder="Password">
                                                                <i class="fa fa-eye"></i>
                                                            </div>
                                                            <div class="mt-2 text-center">
                                                                <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5" id="LoginUser" name="LoginUser">Login</button>
                                                            </div>
                                                            <div class="row mt-2">
                                                                <!--
                                                                <a class="mx-auto" href="{{ route('password.request') }}" >
                                                                    Forgot Your Password?
                                                                </a>
                                                                -->
                                                                <a class="mx-auto" href="#" data-toggle="modal" data-target="#reset_pass" name="resetPopup_link" id="resetPopup_link">
                                                                    Forgot Your Password?
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="register_cont" role="tabpanel" aria-labelledby="register_tab">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="auth auth-form-light text-left p-2">
                                            <h2 class="text-center">Register</h2>
                                            <p class="text-center text-muted mb-1">Choose one of the following sign up methods.</p>
                                            <div class="text-center">
                                                <a href="{{url('/auth/facebook')}}"><i class="fa fa-facebook bg-facebook"></i></a>
                                                <a href="{{url('/auth/twitter')}}"><i class="fa fa-twitter bg-twitter"></i></a>
                                                <a href="{{url('/auth/linkedin')}}"><i class="fa fa-linkedin bg-linkedin"></i></a>
                                                <a href="{{url('/auth/google')}}"><i class="fa fa-google-plus bg-google"></i></a>
                                            </div>
                                            <h4 class="text-center my-1">OR</h4>
                                            <p class="text-center text-muted mb-1">Sign up with your email address</p>
                                            <div id="regErrors">

                                            </div>
                                            <form method="POST" action="{{ route('register') }}" class="pt-1" id="registerForm">
                                                @csrf
                                                <div class="form-group">
                                                    <input type="text" class="col-12 form-input" name="name" id="name" placeholder="User Name">
                                                    <i class="fa fa-user"></i>
                                                    <span class="invalid-feedback" name="name-feedback">
                                                            <strong name="email-error_txt">{{ $errors->first('name') }}</strong>
                                                        </span>
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" class="col-12 form-input" name="email" id="Regemail" placeholder="Email address">
                                                    <i class="fa fa-envelope"></i>
                                                    <span class="invalid-feedback" name="email-feedback">
                                                            <strong name="email-error_txt">{{ $errors->first('email') }}</strong>
                                                        </span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="col-12 form-input" name="password" id="Regpassword" placeholder="Password">
                                                    <i class="fa fa-eye reg"></i>
                                                    <span class="invalid-feedback" name="password-feedback">
                                                            <strong name="email-error_txt">{{ $errors->first('password') }}</strong>
                                                        </span>
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="col-12 form-input" name="password_confirmation" id="password_confirmation" placeholder="Confirm password">
                                                    <i class="fa fa-eye"></i>
                                                    <span class="invalid-feedback" name="password_confirmation-feedback">
                                                            <strong name="email-error_txt">{{ $errors->first('password_confirmation') }}</strong>
                                                        </span>
                                                </div>
                                                <div class="pl-2">
                                                    <span class="text-muted">
                                                        <input type="checkbox" name="agreed" class="" />
                                                        <label for="agreed"><a href="/site/pages/privacy_policy"> I accept terms and conditions</a></label>
                                                        <span class="invalid-feedback" name="agreed-feedback">
                                                            <strong name="email-error_txt">{{ $errors->first('agreed') }}</strong>
                                                        </span>
                                                    </span>
                                                </div>
                                                <div class="mt-2 text-center">
                                                    <button type="submit" class="btn btn-primary btn-rounded  btn-fw btn-gred px-5">Register</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Sign in Modal Ends -->

<!-- Reset Password Modal start -->
<div class="modal fade" id="reset_pass" tabindex="-1" role="dialog" aria-labelledby="reset_pass" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                <div class="content">
                    <h3 class="col-12 font-weight-light text-center mb-3">Reset your password</h3>
                    <div class="auth">
                        <form action="{{ route('password.email') }}" method="post" id="reset_pass_form" class="col-12 text-center">
                            @csrf
                            <div class="form-group">
                                <input type="email" class="col-12 form-input rese_email" name="email" value="{{ $email ?? old('email') }}" required autofocus placeholder="Enter Your Email address" />
                                
                                <i class="fa fa-envelope"></i>
                                <span class="invalid-feedback" name="email-feedback">
                                <strong name="email-error_txt" class="email-error_txt">error message</strong>
                            </span>
                            </div>
                            <button type="submit" class="btn btn-primary btn-rounded  btn-md btn-gred">Send Password Reset Link</button>
                            <div class="alert alert-fill-success my-1 p-2 reset-success" role="alert" style="display: none;">
                                <i class="fa fa-exclamation-circle"></i> Well done! We successfully sent password reset link to your email
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Reset Password Modal Ends -->
<!-- Edit profile Modal start -->
<div class="modal fade" id="edit_my_profile" tabindex="-1" role="dialog" aria-labelledby="edit_my_profile" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button> 
               <form action="/editmyprofile" method="post" id="profileeditform" files="true">
                {{ csrf_field() }}
                <h3 class="text-center mb-3">EDIT YOUR PROFILE PICTURE</h3>
                <input type="file" name="profile" class="dropify" id="profile_input" data-max-file-size="5120kb" />
                <div class="text-center mt-3">
                    <button type="submit" id="profile_submit" disabled class="btn btn-primary btn-rounded btn-fw btn-gred px-5">Save</button>
                </div>
                <span class="invalid-feedback" name="agreed-feedback">
                    <strong name="email-error_txt" id="uploaderror_strong"></strong>
                </span>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Edit profile Modal Ends -->
<!-- Reset Password Modal start -->
<div class="modal fade" id="business_terms" tabindex="-1" role="dialog" aria-labelledby="business_terms" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body p-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                <div class="content">
                    <h3 class="col-12 font-weight-light text-center mb-3">Are you a Business!</h3>
                    <div class="col-12 p-3">
                        <h4>Terms & Conditions</h4>
                        <p>Reayl is A tech agency Looking to connect users to bussiness owners <br>
                            We can simply define Information Technology as “any technology through which we get information is called information technology”.

                            Often times, the term IT is applied to computers and computer-based systems. However, the roots of the word technology suggest that it is a “means” to an end”. For example, using a book of matches is a means to creating a fire. The end is fire itself. A bicycle is a means of transportation. The goal of bicycle riding is to reach a destination, and perhaps also to get some needed exercise.

                            Consequently, when we talk about the use of technology, we must always remember that it is a means, not an end in itself. Technology in the broadest sense is the application of modern communications and computing technologies to the creation, management and use of knowledge.

                            IT typically refers to equipment such as computers, data storage devices, networks and also communication devices.

                            Information Technology means the use of hardware, software, services and supporting infrastructure to manage and deliver information using voices, data and video.

                            To further define information technology and what should be included as far as the IT budget, the following information is provided.</p>
                        
                        <form action="/usertobusiness" method="GET" class="col-12 text-center">
                            <div class="form-group text-left">
                                <span class="text-muted">
                                    <input type="checkbox" name="agreed" class="" id="accept_terms">
                                    <label for="agreed"><a href="/site/pages/privacy_policy">I accept terms and conditions.</a></label>
                                    <span class="invalid-feedback" name="agreed-feedback">
                                        <strong name="email-error_txt"></strong>
                                    </span>
                                </span>
                            </div>
                            <button type="submit" class="btn btn-primary btn-rounded  btn-md btn-gred" id="submit_business" disabled>Proceed</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Reset Password Modal Ends -->

@section('footer_after') @include('front/validate_files') @endsection