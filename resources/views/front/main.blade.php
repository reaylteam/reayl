<!DOCTYPE html>
<html lang="en">
<head>
	@include('front/head')
	@yield('header_after')
</head>
<body class="horizontal-menu">   
	<div class="container-scroller landing-page">
		@include('front/header')
		<div class="container-fluid page-body-wrapper">
			<div class="row row-offcanvas row-offcanvas-right">
				@include('front/mobile_nav')
				<!-- SECTION -->
				@include('front/message')
				@yield('content')
			</div>
		</div>
		@include('front/footer')
	</div>

		<!-- /SECTION -->
		@include('front/modal')

		@include('front/mainjs')
		@yield('footer_after')
	</body>
	</html>