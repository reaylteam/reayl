@if ($paginator->hasPages())
<div class="pagiWrap">
    <div class="row">
        <div class="col-md-4 col-sm-4">
        </div>
        <div class="col-md-8 col-sm-8 text-right">
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-end">
            @if ($paginator->onFirstPage())
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">Previous</a>
                </li>
            @else
                <li class="page-item ">
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" tabindex="-1">Previous</a>
                </li>
            @endif
            @foreach ($elements as $element) 
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif 
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active"><a class="page-link">{{ $page }}</a></li>
                        @else
                            <li  class="page-item"><a  class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach 
            @if ($paginator->hasMorePages())
                <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a></li>
            @else
                <li class="page-item disabled"><span>Next</span></li>
            @endif 
            </ul>
          </nav>
        </div>
    </div>
</div>
@endif
                   
