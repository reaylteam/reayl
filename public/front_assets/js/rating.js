(function($) {
   'use strict';

    // Jquery Bar Rating Starts

    $(function() {
        function ratingEnable() {
            $('#average_review').barrating({
                theme: 'fontawesome-stars',
                showSelectedRating: false,
                deselectable: true,
                readonly: 'true'
            });
            $('#make_review').barrating({
                theme: 'fontawesome-stars',
                showSelectedRating: false
            });
        }

        function ratingDisable() {
            $('select').barrating('destroy');
        }

        $('.rating-enable').click(function(event) {
            event.preventDefault();

            ratingEnable();

            $(this).addClass('deactivated');
            $('.rating-disable').removeClass('deactivated');
        });

        $('.rating-disable').click(function(event) {
            event.preventDefault();

            ratingDisable();

            $(this).addClass('deactivated');
            $('.rating-enable').removeClass('deactivated');
        });

        ratingEnable();
    });


    // Jquery Bar Rating Ends

})(jQuery);
