(function ($) {
    'use strict';
    var form = $("#sign_in_form");
    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: "slideLeft",
        onFinished: function (event, currentIndex) {
            alert("Submitted!");
        }
    });

})(jQuery);
