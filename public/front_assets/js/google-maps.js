'use strict';

function initMap() {

  var latitude   = parseFloat(document.getElementById('lat').value);
  var langtitude = parseFloat(document.getElementById('lang').value);

  
  //Map location

  var MapLocation = {
    lat: latitude,
    lng: langtitude
  };

  // Map Zooming

  var MapZoom = 14;


  // Basic Map
  
  var iconBase = '../../images/sprites/';
  
  
  // Captor Theme

  var CaptorThemeMap = new google.maps.Map(document.getElementById('captor-map-theme'), {
    zoom: MapZoom,
    center: MapLocation,
    styles: [{
        "featureType": "water",
        "stylers": [{
          "color": "#0e171d"
        }]
      },
      {
        "featureType": "landscape",
        "stylers": [{
          "color": "#1e303d"
        }]
      },
      {
        "featureType": "road",
        "stylers": [{
          "color": "#1e303d"
        }]
      },
      {
        "featureType": "poi.park",
        "stylers": [{
          "color": "#1e303d"
        }]
      },
      {
        "featureType": "transit",
        "stylers": [{
            "color": "#182731"
          },
          {
            "visibility": "simplified"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.icon",
        "stylers": [{
            "color": "#f0c514"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels.text.stroke",
        "stylers": [{
            "color": "#1e303d"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [{
            "color": "#e77e24"
          },
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
          "color": "#94a5a6"
        }]
      },
      {
        "featureType": "administrative",
        "elementType": "labels",
        "stylers": [{
            "visibility": "simplified"
          },
          {
            "color": "#e84c3c"
          }
        ]
      },
      {
        "featureType": "poi",
        "stylers": [{
            "color": "#e84c3c"
          },
          {
            "visibility": "off"
          }
        ]
      }
    ]
  });

  //map marker
  var marker = new google.maps.Marker({
      position: MapLocation,
      draggable: false,
      map: CaptorThemeMap
   });

  google.maps.event.addListener(CaptorThemeMap, 'click', function(event) {
             
      console.log(event.latLng);

      placeMarker(event.latLng);

  });
  function placeMarker(location)
   {
        marker.setPosition(location);
        // var gps_lat =document.getElementById("gps_lat");
        // var gps_long =document.getElementById("gps_long");
        var lat =marker.getPosition().lat();
        var lang=marker.getPosition().lng();
        $.get( "/user/location/setup/"+lat+"/"+lang, function() {
          $.toast({
            heading: 'Success',
            text: 'Your Location Updated successfully',
            showHideTransition: 'slide',
            icon: 'success'
          });
        })
        .fail(function() {
          alert( "error" );
        });
        console.log(lat+'----'+lang);
   }
}