   // In this example, we center the map, and add a marker, using a LatLng object
   // literal instead of a google.maps.LatLng object. LatLng object literals are
   // a convenient way to add a LatLng coordinate and, in most cases, can be used
   // in place of a google.maps.LatLng object.

   var map;
   var marker;
   var infowindow;
   var position;
   function initialize() {
   var mapOptions = {
       zoom: 8,
       center: {lat: 25.207066, lng:55.273107 }
   };
   var gps_lat_Edit =document.getElementById("gps_lat");
   var gps_long_Edit =document.getElementById("gps_long");
   if(gps_lat_Edit.value == '' && gps_long_Edit.value == '')
   {
        position={lat: 25.207066, lng: 55.273107};
   }
   else
   {
       position={lat: parseFloat(gps_lat_Edit.value), lng: parseFloat(gps_long_Edit.value)};
   }
   //position={lat: 30.040106, lng: 31.242922};
   console.log(position);
   map = new google.maps.Map(document.getElementById('map'),
       mapOptions);

   marker = new google.maps.Marker({
       draggable: true,
       map: map
   });
   infowindow = new google.maps.InfoWindow({
   });
   google.maps.event.addListener(map, 'click', function(event) {
             placeMarker(event.latLng);
      });

   function placeMarker(location)
   {
        marker.setPosition(location);
        var gps_lat =document.getElementById("gps_lat");
        var gps_long =document.getElementById("gps_long");
        gps_lat.value =marker.getPosition().lat();
        gps_long.value =marker.getPosition().lng();
   }
   google.maps.event.addListener(marker, 'dragend', function() {
       var gps_lat =document.getElementById("gps_lat");
       var gps_long =document.getElementById("gps_long");
       gps_lat.value =marker.getPosition().lat();
       gps_long.value =marker.getPosition().lng();
   });
   if (navigator.geolocation) {
       navigator.geolocation.getCurrentPosition(function(position) {
       var pos = {
           lat: position.coords.latitude,
           lng: position.coords.longitude
       };

       infowindow.setPosition(pos);
       marker.setPosition(pos);
       map.setCenter(pos);
       }, function() {
       handleLocationError(true, infowindow, map.getCenter());
       });
   } else {
       // Browser doesn't support Geolocation
       handleLocationError(false, infowindow, map.getCenter());
   }
   function handleLocationError(browserHasGeolocation, infoWindow, pos) {
   infowindow.setPosition(position);
   marker.setPosition(position);
   map.setCenter(position);
   infoWindow.setContent(browserHasGeolocation ?
                           'Error: The Geolocation service failed.' :
                           'Error: Your browser doesn\'t support geolocation.');
       }
   }
   google.maps.event.addDomListener(window, 'load', initialize);
