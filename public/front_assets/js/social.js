
$(document).on('click', '.comment_event', function(e) {
	var current = this;
	var post_id = $(this).attr('postID');
	e.preventDefault();
	if($(this).prev().val()=='')
		return false;
	var comment_body = $(this).prev().val();
    var model        = $(this).attr('modelName');
    // console.log($(current).prev().val());
    // console.log();
	$.ajax({
            url:'/api/comments',
            type:'POST',
            data:{post_id:post_id,content:comment_body,model:model,user_id:user_id},
            success:function(result){
                
            	$(current).parent().parent().parent().parent().next().prepend(result);
                //$(".comments").prepend(result);
                $(current).prev().val('') ;
            },error:function(error){  

            }
    })
});
$(document).on('click','.followbtn',function(){
    var company_id=$(this).attr('company-id');
    var that=this;
    $.get('/followcompany/'+company_id,function(data){
        $(that).text(data);
        if($('#followers').length){
            if(data=='Follow')
                $('#followers').text(parseInt($('#followers').text())-1);
            else
                $('#followers').text(parseInt($('#followers').text())+1);
        }
        

    });
});
// $(document).ready(function() { $(".select2").select2(); });

$(document).on('click', '.like-btn', function(e) {
    var post_id = $(this).attr('PostId');
    var that = this;        
    $.ajax({
        url:'/api/likes',
        type:'POST',
        data:{post_id:post_id,user_id:user_id},
        success:function(result){
            
            $(that).find('span').next('span').text(result.post.likes+" Likes");
            if(result.liked){
                $(that).find('span').find('i').css('color','#7e4cf9');
            }else{
                $(that).find('span').find('i').css('color','#aab2bd');
            }
        },error:function(error){
            

        }
    })
    return false;
});
$(document).on('click', '.share-btn', function(e) {
    var post_id = $(this).attr('PostId');
    var that = this;        
    $.ajax({
        url:'/api/shares',
        type:'POST',
        data:{post_id:post_id,user_id:user_id},
        success:function(result){
            
            $(that).find('span').next('span').text("Shared");
            $(that).find('span').next('span').css('color','#7e4cf9 !important');
            
        },error:function(error){
            

        }
    })
    return false;
})
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//This is called when the first AJAX request is fired.
$(document).ajaxStart(function(event, jqXHR) {
    
    $("#ajaxSpinnerImage").show();
});
//This is called after all the AJAX request are stopped.
$(document).ajaxStop(function(event, jqXHR) {
    //Hide spinner
    $("#ajaxSpinnerImage").hide();
});

$(".post_now").on('click',function(){

    var body = $(".body_contnet").val();
    $.ajax({
        url:'/api/posts',
        type:'POST',
        data:{body:body,user_id:user_id},
        success:function(result){
            $("#posts").prepend(result);
            $(".body_contnet").val('');
        },error:function(error){
            alert('Post Must be at least 20 characters');

        }
    })
});
$(".add-new-offer").on('click',function(e){
    e.preventDefault();
    var data = new FormData($(this)[0]);
    $.ajax({
        url:'/offers',
        type:'POST',
        data:$('#add_new_offer_form').serialize(),
        //data:data,
        dataType: "json",
        success:function(result){
            location.reload();
        },error:function(data){
                $.each(data.responseJSON.errors, function (key, value) {
                    var input = $('#add_new_offer_form').find('input[name=' + key + ']');
                    $(input).next('small').text(value);
                    $(input).parent().addClass('has-error');
                });

        }
    })
});
$(".post_new_job").on('click',function(e){
    e.preventDefault();
    $.ajax({
        url:'/jobs',
        type:'POST',
        data:$('#add_post_job_form').serialize(),
        dataType: "json",
        success:function(result){
            location.reload();
        },error:function(data){
                $.each(data.responseJSON.errors, function (key, value) {
                    var input = $('#add_post_job_form').find('input[name=' + key + ']');
                    $(input).next('small').text(value);
                    $(input).parent().addClass('has-error');
                });

        }
    })
});

$(document).on('click', '#review_btn', function(e) {
    e.preventDefault();
    var current = this;
    var comment = $("#review_body").val();
    $("#review_body").val('');
    if(comment=='')
        return false;
    var rating_type = $("#rateable_type").val();
    var rating      = $("#make_review").val();
    var item_id     = $("#item_id").val();
    $.ajax({
            url:'/review/item',
            type:'POST',
            data:{comment:comment,rating_type:rating_type,rating:rating,item_id:item_id},
            success:function(result){
                // console.log(result);
                // location.reload();
                if(result){
                    if($('#user_'+user_id)){
                        $('#user_'+user_id).remove();
                        $('#review_div').append(result);
                    }
                    else{
                        $('#review_div').append(result);
                    }
                    
                        $('.user_rating').barrating({
                            theme: 'fontawesome-stars',
                            showSelectedRating: true,
                            deselectable: false,
                            readonly: 'true'
                        });
                }
               
            },error:function(error){  

            }
})
});
$(document).on('keyup', '.comment-body-input', function(e) {
    var code = e.which; 
    if(code==13){
        e.preventDefault();
        
        $('#loginPopup_link').trigger() ;
    }
});

$(document).on('change','#profile_input',function(){
    $('#profile_submit').prop("disabled", false);
});

$(document).on('submit', '#profileeditform', function(e) {  
    e.preventDefault();
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: new FormData(this),
        contentType: false,
        processData:false
    })
    .done(function(data) {
    location.reload();
    })
    .fail(function(data) {
        $('#uploaderror_strong').text(data.responseJSON.message);
        $('#uploaderror_strong').parent().css("display", "block");
    });
});

$(document).on('change','#cover_input',function(){
    $('#cover_submit').prop("disabled", false);
});

$(document).on('submit', '#edit_cover_form', function(e) {  
    e.preventDefault();
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: new FormData(this),
        contentType: false,
        processData:false
    })
    .done(function(data) {
    location.reload();
    })
    .fail(function(data) {
        $('#uploaderror_strong').text(data.responseJSON.message);
        $('#uploaderror_strong').parent().css("display", "block");
    });
});


$(document).on('submit', '#add_new_offer_form', function(e) {
    e.preventDefault();
    var that=this;
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: new FormData(this),
        contentType: false,
        processData:false
    }) 
    .done(function(data) {
        location.reload();
    })
    .fail(function(data) {
        // console.log()
        $.each(data.responseJSON.errors, function (key, value) {
            if(key=='delivery_time' || key=='from' || key=='to'){
                var input = $(that).find('input[name=' + key + ']');
                $(input).next().next().children().text(value)
                $(input).next().next().css('display','block')
            }
            else if(key=='photos'){
                alert('Please Uploade Photos')

            }
            else{
                var input = $(that).find('input[name=' + key + ']');
                $(input).next().children().text(value)
                $(input).next().css('display','block')
            }
                
        });
    });
});

$(document).on('change', '#accept_terms', function(e) {
    if(this.checked) {
        $('#submit_business').prop("disabled", false);
    }
    else{
        $('#submit_business').prop("disabled", true);

    }
});
$(document).on('change keyup','#user_email',function(){
    $('#check_user_in_system').html('<i class="fa fa-info-circle"></i> Make sure that user was registerd before.');
    $('#check_user_in_system').css("background-color" ,"#03a9f3");    
});
$(document).on('submit', '#add_subuser_form', function(e) {  
    e.preventDefault();
    $.ajax({
        method: $(this).attr('method'),
        url: $(this).attr('action'),
        data: new FormData(this),
        contentType: false,
        processData:false
    })
    .done(function(data) {
        // console.log(data);
        $('#sub_user_image').attr('src',data.image);
        $('#sub_user_name').text(data.name);
        $('#sub_user_found_email').val(data.email);
        // $('#subuser_pic_link').trigger();
        $('#subuser_pic').modal('show');
    // location.reload();
    })
    .fail(function(data) {
        $('#check_user_in_system').text(data.responseJSON.error);
        $('#check_user_in_system').css("background-color" ,"red");
    });
});
$(document).on('click', '#yes_btn', function(e) {
        e.preventDefault();
        $.ajax({
            method: 'Post',
            url: '/registersubuser',
            data: {email:$('#sub_user_found_email').val()}
            
        })
        .done(function(data) {
            // console.log(data);
    
            alert('Add Successfully.');
            location.reload(); 
        })
        .fail(function(data) {
            // console.log(data);
            alert('User Not Found !.');
            $('#subuser_pic').modal('hide'); 
            $('#add_subuser').modal('show');
    
        });
    });

$(document).on('click', '.add_to_cart', function(e) {  
    e.preventDefault();
    $.ajax({
        url:'/cart/add/offer/'+$(this).attr('offerID'),
        method: 'GET',
        success:function(data)
        {
            $(".alert-fill-success").show();

        },error:function(error)
        {
            $(".alert-fill-danger").show();
        }
    })
});

$(document).on('click','.delete_item',function(e){
    e.preventDefault();
    var id = $(this).attr('data-id');
    var url = $(this).attr('data-url');
    $(".remove_offer_form").attr("action",url);
    $('body').find('.remove_offer_form').append('<input name="_method" type="hidden" value="DELETE">');
    $('body').find('.remove_offer_form').append('<input name="id" type="hidden" value="'+ id +'">');
    $('.submit_query').click(function(e) {
        e.preventDefault();
        $.ajax({
            url:'/cart/'+id,
            method: 'DELETE',
            success:function(data)
            {
                $('.remove_offer').modal('hide');
                $("#item_"+id).remove();

            },error:function(error)
            {
                $(".alert-fill-danger").show();
            }
        })
    });
});

$(document).on('click','.delete_offer',function(e){
    e.preventDefault();
    var id = $(this).attr('data-id');
    var url = $(this).attr('data-url');
    $(".remove_offer_form").attr("action",url);
    $('body').find('.remove_offer_form').append('<input name="_method" type="hidden" value="DELETE">');
    $('body').find('.remove_offer_form').append('<input name="id" type="hidden" value="'+ id +'">');
    $('.submit_query').click(function(e) {
        e.preventDefault();
        $.ajax({
            url:'/offers/delete/'+id,
            method: 'get',
            success:function(data)
            {
                $('.remove_offer').modal('hide');
                $("#offer_"+id).remove();

            },error:function(error)
            {
                $(".alert-fill-danger").show();
            }
        })
    });
});

$(document).on('submit', '#reset_pass_form', function(e) {  
    e.preventDefault();
    var that = this;
    $.ajax({
        url: '/password/email',
        method: 'post',
        data: new FormData(this),
        contentType: false,
        processData:false
    })
    .done(function(data) {
        var input = $(that).find('input[name=email]');
        $(input).next().css('display','none');
        $(".reset-success").css('display','block');
        $(".rese_email").val('');
    //location.reload();
    })
    .fail(function(data) {
        console.log(data.responseJSON.message);
        var input = $(that).find('input[name=email]');
        $(input).next().children().text(data.responseJSON.message);
        $(input).next().css('display','block');
        $(".email-error_txt").html(data.responseJSON.errors.email);
        $(".invalid-feedback").css('display','block');
    });
});
$(document).on('click','#edit_offer_btn',function(e){
    $.get('/offers/'+$(this).attr('offerID')+'/edit',function(data){
        
        $('#offer_edit_model').html();
        $('#offer_edit_model').html(data);
        $('#edit_offer').modal('show');
        initalizationclockdate();

    });
});

$(document).on('submit','#edit_offer_form',function(e){
    e.preventDefault();
    var that=this;
    $.ajax({
        url: $(this).attr('action'),
        type: $(this).attr('method'),
        data: new FormData(this),
        contentType: false,
        processData:false
    }) 
    .done(function(data) {
        // console.log(data)
        
        location.reload();
    })
    .fail(function(data) {
        // console.log()
        $.each(data.responseJSON.errors, function (key, value) {
            if(key=='delivery_time' || key=='from' || key=='to'){
                var input = $(that).find('input[name=' + key + ']');
                $(input).next().next().children().text(value)
                $(input).next().next().css('display','block')
            }
            else if(key=='photos'){
               alert('Please Uploade Photos')
            }
            else{
                var input = $(that).find('input[name=' + key + ']');
                $(input).next().children().text(value)
                $(input).next().css('display','block')
            }
                
        });
    });
});
