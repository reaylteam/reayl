(function($) {
  'use strict';

    if($(".js-example-basic-single-4").length){
    $(".js-example-basic-single-4").select2({
        placeholder: "Select category"
    });
  }
  if($(".js-example-basic-single-3").length){
    $(".js-example-basic-single-3").select2({
        placeholder: "Select category"
    });
  }
    if($(".js-example-basic-single-2").length){
    $(".js-example-basic-single-2").select2({
        placeholder: "Select category"
    });
  }
    if($(".js-example-basic-single").length){
    $(".js-example-basic-single").select2({
        placeholder: "Select country"
    });
  }
  if($(".js-example-basic-multiple").length){
    $(".js-example-basic-multiple").select2();
  }
})(jQuery);
