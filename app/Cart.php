<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table    = 'cart';
    
    protected $fillable = ['user_id','offer_id'];

    public function offer()
    {
    	return $this->belongsTo('App\Offer','offer_id');
    }
}
