<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Business_profile extends Model
{
    //
    protected $fillable = [
        'phone',
        'company_name',
        'bio',
        'linkedin',
        'facebook',
        'twitter',
        'website',
        'bussiness_email',
        'address',
        'country_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
