<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    public $table = 'jobs';
    protected $appends  = array('duration');
    public $fillable = [
        'title',
        'description',
        'location',
        'seniority_level',
        'user_id',
        'skills_rquired'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function getDurationAttribute(){
        return $this->created_at->diffForHumans();
    }
    public function applications_count(){
        return $this->hasMany('App\Application','job_id');
    }
}
