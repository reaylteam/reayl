<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $appends  = array('top');

    public function categorizable()
    {
        return $this->morphTo();
    }
    public function categorizableCount(){
    	return $this->hasMany('App\Categorizable','category_id');
    }
    public function getTopAttribute(){
        return $this->categorizableCount->count();
    }
    public function offers(){
        return $this->morphedByMany('App\Offer','categorizable');
    }
    public function jobs(){
        return $this->morphedByMany('App\Job','categorizable');
    }
}
