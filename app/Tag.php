<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable = [
        'name', 'slug', 'description',
    ];

    protected $appends  = array('top');

    public function taggable()
    {
        return $this->morphTo();
    }
    public function taggableCount(){
    	return $this->hasMany('App\Taggable','tag_id');
    }
    public function getTopAttribute(){
        return $this->taggableCount->count();
    }
    public function offers(){
        return $this->morphedByMany('App\Offer','taggable');
    }
    public function jobs(){
        return $this->morphedByMany('App\Job','taggable');
    }
}
