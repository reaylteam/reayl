<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Static_page extends Model
{
    //
    protected $table   ='static_pages';  
    protected $fillable=['name','slug','content'];
}
