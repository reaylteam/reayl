<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Individual_user extends Model
{
    //
    protected $fillable = [
        'facebook_url', 
        'twitter_url', 
        'linkedin_url',
        'behance_url',
        'drobble_url',
        'github_url',
        'website_url',
        'location',
        'first_name',
        'last_name',
        'profissional_title',
        'about_me'
    ];
}
