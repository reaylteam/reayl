<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Offer extends Model implements HasMedia
{
    //
    use Rateable;
    use HasMediaTrait;

    public $table = 'offers';
    public $fillable = [
        'title',
        'description',
        'offer_package',
        'delivery_time',
        'minimum_of_entities',
        'from',
        'to',
        'materials',
        'user_id',
        'price',
        'countries_id',
        'category_id'
    ];
    protected $appends  = array('duration');
    protected $dates = ['from','to'];

    public function company(){
    	return $this->belongsTo('App\User','user_id');
    }
    public function getDurationAttribute(){
    	return $this->created_at->diffForHumans();
    }
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function files(){
        return $this->morphToMany('App\File', 'fileable')->withPivot('is_profile');
    }
    public function reviews(){
        return $this->ratings;
    }
    public function avgrating(){
        if(count($this->reviews()))
            return round (($this->reviews()->sum('rating'))/(count($this->reviews())));
        else 
            return 0;
    }
    public function images(){
        return $this->getMedia('offer-photos');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }
    public function country(){
        return $this->belongsTo('App\Country','countries_id');
    }
}
