<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //
    public function __construct(){
    	return $this->middleware('auth');
    }
    public function reviewItem(Request $request){
    	$input           = $request->all();
    	$offer           = \App\Offer::find($input['item_id']);
    	if(!$offer)
			return 'false';
		$reviewcheck=\willvincent\Rateable\Rating::where('rateable_type','App\Offer')->where('user_id',\Auth::user()->id)->where('rateable_id',$offer->id)->first();
		if($reviewcheck){
			$reviewcheck->rating=$input['rating'];
			$reviewcheck->comment = $input['comment'];
			$reviewcheck->save();
			$review =$reviewcheck;
		}
		else{
			$rating          = new \willvincent\Rateable\Rating;
			$rating->rating  = $input['rating'];
			$rating->user_id = \Auth::user()->id;
			$rating->comment = $input['comment'];
			$review          =$offer->ratings()->save($rating);
		}
		
		if($review){
			$review->username=$review->user->name;
			$review->profile=($review->user->profile())?$review->user->profile()->getUrl():'/front_assets/images/faces/anonymous-user.png';
			$review->createdat=$review->created_at->diffForHumans();
			// return $review;
			return view('website.reviews.review',compact('review'));
		}else
			return 'false';
    }
}
