<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Mail\FinishRegisteration;

use Illuminate\Support\Facades\Validator;
class BusinessUserSubusersManagementController extends Controller
{
    //
    public function list_users($user_id){
        $user      = User::find($user_id);
        if($user->level=='business'){
            $sub_users =$user->sub_users;
    	    return view('website.user.sub.list',compact('user','sub_users'));
        }
        else{
            return back();
        }
    	
    }
    public function delete($id){
    	
    	$user   = User::find($id);
        if($user && $user->user_id==\Auth::user()->id){
            $user->user_id=NULL;
            $user->level='user';
            $user->save();
    		// session()->flash('status', 'deleted sucssfully');

        }
        

    	// else
    	// 	session()->flash('danger', trans('website.error'));
    	return redirect()->back();
    }
    public function registerUser(){
        return view('website.user.sub.create');
    }
    public function register(Request $request){
        $data = $request->all();
        $validation = $request->validate([
            'email' => 'required|email',
            'user_id'=>'required'
        ]);
        if($data['email']==\Auth::user()->email){
            return response()->json(['error'=>'You can\'t Add yourself !'],400);

        }
        
        $user = User::where('email', $data['email'])->first();
        if($user){
            // $user->user_id=$data['user_id'];
            // $user->level='sub-user';
            // $user->save();
            // \Mail::to($user->email)->send(new FinishRegisteration($user));
            // session()->flash('success','done');
            // return response()->json(['message'=>'Add Successfully',''],200);
            return response()->json(['message'=>'User Found','email'=>$user->email,'image'=>($user->profile())?$user->profile()->getUrl():'/front_assets/images/faces/anonymous-user.png','name'=>$user->name],200);


        }
        else{
            session()->flash('danger','erorr');

            return response()->json(['error'=>'User Not Registered on system !'],400);

        }

    }
    public function completeRegister($email){
        $verifyUser = User::where('email', $email)->first();
        return view('website.user.complete_register',compact('verifyUser'));
    }

    public function usertobusiness(){
        $user=\Auth::user();
        $user->level='business';
        $user->save();
        return redirect('/myprofile/edit');
    }

    public function registersubuser(Request $request){
        $data = $request->all();
        
        $user = User::where('email', $data['email'])->first();
        if($user){
            $user->user_id=\Auth::user()->id;
            $user->level='sub-user';
            $user->save();
            session()->flash('success','done');
            return response()->json(['message'=>'Add Successfully',''],200);
        }
        else{
            session()->flash('danger','erorr');

            return response()->json(['error'=>'User Not Registered on system !'],400);

        }
    }
}
