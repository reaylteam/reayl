<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\Events\NewComment;
class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input   = $request->all();
        $comment = Comment::create(['content'=>$input['content'],'user_id'=>$input['user_id']]);
        $result  = $input['model']::find($input['post_id'])->comments()->attach($comment);
        // broadcast(new NewComment($comment))->toOthers();

        return view('website.posts.comment',compact('comment'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($model,$show,$id,$test)
    {
        //
        return $model;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    public function updateComment($id,$content){
       // $comment  = Comment::find($id);
        //$result   = $comment::update('content'=>$content);
        $result = Comment::where('id', $id)->update(array('content' => $content));

        return $result;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }
    public function list_comments(Request $request){

        $input = $request->all();
        $video_comments = $input['model']::find($input['id'])->comments;
        foreach ($video_comments as $comment) {
            foreach ($comment->replies as $reply) {
                $reply['parent']=$comment->id;
                $video_comments->push($reply);
            }
            
             $video_comments ->merge($comment->replies);
        }
        // foreach ($video_comments as $key => $value) {
        //     $video_comments = array_add(['upvote_count' => ($value->likeCount)]);
        // }
        //$comments = $input['model']::with('comments')->get();
        return $video_comments;
    }
    public function like_comment(Request $request){
        $input     = $request->all();
        $comment   = Comment::find($input['comment']);
        if($comment->liked())
            $result  = $comment->unlike();
        else
            $result  = $comment->like();
        $counter = $comment->likeCount;
        $result  = array();
        $result[0] = $comment->id;
        $result[1] = $comment->likeCount;
        return $result;
    }
    public function dislike_comment(){
        $input     = $request->all();
        $comment   = Comment::find($input['comment']);
        $result  = $comment->unlike();
        $counter = $comment->likeCount;
        $result  = array();
        $result[0] = $comment->id;
        $result[1] = $comment->likeCount;
        return $result;
    }
    public function Deletecomments($id){

        $comment = Comment::findOrFail($id);
        if(!empty($comment->replies)){
            foreach($comment->replies as $reply)
            $reply->delete();
        }

        $result = $comment->delete();
        if($result)
            return "deleted";
        else
            return "failed";
    }
    public function update_comment(Request $request){

        $input = $request->all();

        $result = Comment::where('id', $input['commentJSON']['id'])->update(array('content' => $input['commentJSON']['content'],'modified'=>'1'));
        $comment = Comment::with('user')->find($input['commentJSON']['id']);
        return $comment;
    }
}
