<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Controllers\Controller;
use App\Post;
use DB;
use Illuminate\Support\Facades\Auth;


class LikesControllerApi extends Controller
{
    //

    
    public function store(Request $request){
        $input = $request->all(); 
        $post  = Post::find($input['post_id']);
        $data  = array();
        if($post->liked($input['user_id'])){
            $post->unlike($input['user_id']);
            $data['liked'] = false;
        }
        else{
            $like = $post->like($input['user_id']);
            //event(new \App\Events\NewLike($post));
            $data['liked']  = true; 

        }
        $data['post'] = $post;

    	return $data;
    }
    public function update(Request $request,$id){
    	$post = Post::find($id);

    	$result = $post->like($request->user_id);
    	return $result;
    }
}
