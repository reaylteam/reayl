<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\User;
use App\Business_profile;
use App\Application;
class JobsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $jobs=Job::all();
        $companies=Business_profile::all();
        return view('website.jobs.jobs',compact('jobs','companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required|string',
            'location'=>'required',
            'seniority_level'=>'required',
			'skills_rquired'=>'required'
		]);
		$input=$request->all();
		$input['user_id']=\Auth::user()->id;
		
		$job=Job::create($input);
		
		return $job;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function comapnyjobs($id){
        $user=User::find($id);
        if(!$user){
            return back();
        }
        $jobs=$user->jobs;
        $companies=Business_profile::all();
        return view('website.jobs.jobs',compact('jobs','companies'));
    }
    public function JobDeatile($id,$name){
        $job = Job::where('id',$id)->where('title',$name)->first();
        $first_four_jobs = Job::where('user_id',$job->user_id)->orderByRaw('RAND()')->take(4)->get();
        if(!$job)
            abort('404');
        return view('website.jobs.show',compact('job','first_four_jobs'));
    }
    public function apply(Request $request){
        $input         = $request->all();
        $user          = \Auth::user();
        $old_applicant = Application::where('job_id',$input['job_id'])->where('user_id',\Auth::user()->id)->first();
        if($old_applicant){
            \Session::flash('message', 'U already applied');
            return redirect()->back();
        } 
        if(!$user)
            return redirect()->back();
        if(isset($input['cv'])){
            $profiles = $user->getMedia('cvs');
            if($profiles->count())
                $profiles[0]->delete();
            $user->addMedia($request->file('cv'))->toMediaCollection('cv');
        }
        $input['status']='pending';
        $input['user_id']=\Auth::user()->id;
        $application = Application::create($input);
        return redirect()->back();
    }
    public function myApplications($user_id,$user_name){
        $applications = Application::where('user_id',$user_id)->get();
        return view('website.user.applications.list',compact('applications'));
    }

     public function delete($id){
        $job   = Job::find($id);
        $result =$job->delete();
        if($result)
            session()->flash('status', 'deleted sucssfully');

        else
            session()->flash('danger', trans('website.error'));
        return redirect()->back();
    }
}
