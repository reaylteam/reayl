<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
class TagsController extends Controller
{
    //
    public function searchTag($id){
    	$tag    = Tag::find($id);
    	$jobs   = $tag->jobs;
    	$offers = $tag->offers;
    	$count  = $jobs->count()+$offers->count();
    	return view('website.search.tag',compact('jobs','offers','count'));
    }
}
