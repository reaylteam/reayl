<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Static_page;
class StaticPagesController extends Controller
{
    //
    public function show($id){
    	$page = Static_page::find($id);
    	if(!$page)
    		abort(404);
    	return view('website.static_pages.show',compact('page'));
    }

    public function getPage($page_name){

    	if( $page_name     == 'about' )

    		return view('website.static_pages.about');

    	elseif( $page_name == 'help_center' )

    		return view('website.static_pages.help_center');

    	elseif( $page_name == 'terms_and_condition')

    		return view('website.static_pages.terms_and_condition');

    	elseif( $page_name == 'privacy_policy' )

    		return view('website.static_pages.privacy_policy');
    	else
    		
    		abort(403);
    }
}
