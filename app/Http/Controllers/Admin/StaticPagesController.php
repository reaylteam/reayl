<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Static_page;
use App\DataTables\StaticPagesDatatable;
class StaticPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StaticPagesDatatable $admin)
    {
        //
        return $admin->render('admin.static_pages.index',['title'=>'Static Pages dashboard']);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.static_pages.create', ['title' => trans('admin.add')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $this->validate(request(),
            [
                'name'     => 'required',
                'description'=>''
                
            ], [], [
                'name'     => trans('admin.name'),
           
        ]);
        $data['slug'] = str_slug($data['name']);
        Static_page::create($data);
        session()->flash('success', trans('admin.record_added'));
        return redirect(aurl('static_pages'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $page  = Static_page::find($id);
        if(!$page)
            abort(404);
        $title = trans('admin.edit');
        return   view('admin.static_pages.edit', compact('page', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $this->validate(request(),
            [
                'name'     => 'required',
                
                'slug'    => 'required|unique:static_pages,slug,'.$id,
            ], [], [
                'name'     => trans('admin.name'),
                'slug'    => trans('admin.slug'),
               
            ]);
        $data['slug'] = str_slug($data['name']);
        Static_page::where('id', $id)->update($data);
        session()->flash('success', trans('admin.updated_record'));
        return redirect(aurl('static_pages'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id) {
        Static_page::find($id)->delete();
        session()->flash('success', trans('admin.deleted_record'));
        return redirect(aurl('static_pages'));
    }
}
