<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Category;
use \App\Job;
use \App\Tag;
use \App\User;
use \App\Post;
class LandingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $first_four_jobs = Job::orderByRaw('RAND()')->take(4)->get();
        $tags            = Tag::take(10)->get();

        if(\Auth::check()){
            $user = \Auth::user();
            if($user->parent)
                $user=User::where('id',$user->parent->id)->first();
            $posts = collect();
            $followings = $user->following;
            foreach ($followings as $key => $following) {
                foreach ($following->posts as $post) {
                    $posts->push($post);
                }
            }
            
        }else{
            $posts = Post::orderByRaw('RAND()')->take(10)->get();

        }
        return view('welcome',compact('first_four_jobs','tags','posts','top_companies','user')); 
    }
    public function list_all_users(){
        return $users = User::all();
        return view('website.user.list_all_users',compact('users'));
    }
}
