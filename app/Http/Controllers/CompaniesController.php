<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Follow;
use App\Business_profile;
class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies=\App\User::where('level','business')->paginate(8);
        $companiescount=\App\User::where('level','business')->get()->count();
        return view('website.companies.index',compact('companies','companiescount'));
    }

    public function loadDataAjax(Request $request){
        $input     = $request->all();
        $companies = \App\User::where('id','>',$input['id'])->where('level','business')->limit(8)->get();
        return view('website.companies.listAjaxData',compact('companies')); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $company=\App\User::find($id);
        return view('website.companies.show',compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            
            
        ]);
        $user=\Auth::user();
        $input=$request->all();
        $input['user_id']=$user->id;
        unset($input['_method']);
        unset($input['_token']);
        $individual_user=Business_profile::where('user_id',$user->id)->first();
        if($individual_user){
            $individual_user->update($input);
        }
        else{
            Business_profile::create($input);
        }
        return redirect('profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function follow($id){
        $company = \App\User::find($id);
        $check=Follow::where('follower_id',\Auth::user()->id)->where('following_id',$id)->first();
        if($check){
            $check->delete();
            return "Follow";
        }
        else{
            Follow::create(['follower_id'=>\Auth::user()->id,
                            'following_id'=>$id]);
        return "UnFollow";
        }
                        
        // \Auth::user()->following()->attach($company)
    }
}
