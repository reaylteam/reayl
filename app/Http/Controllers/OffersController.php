<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Offer;
use App\File;
use Flash;
class OffersController extends Controller
{
	public function CompanyOffers($id){
		$offers  = Offer::where('user_id',$id)->get();
		return view('website.companies.offers',compact('offers'));
	}
	public function offerDetailes($id){
		$offer   = Offer::find($id);
		$options = json_decode($offer->materials,true);
		$likeoffers=Offer::where('category_id',$offer->category_id)->where('country_id',$offer->country_id)->take(6);
		
		$images = $offer->getMedia('offer-photos');
		if($offer){
			
			return view('website.companies.offer_deatiles',compact('likeoffers','offer','images','options'));
		}
		else
			return redirect()->back();
		
	}
	public function create(){
		return view('website.offers.create');
	}
	public function store(Request $request){
		$this->validate($request, [
            'title' => 'required|string',
			'price'=>'required',
			'photos'=>'required|array|between:1,20',
			'photos.*'=>'image',
			'from'=>'required|date',
			'to'=>'required|date|after_or_equal:from',
			'delivery_time'=>'required',
			'countries_id'=>'required',
			'category_id'=>'required',

		]);
		$input=$request->all();

		$input['user_id']=\Auth::user()->id;
		$input['materials']=json_encode($input['group-a']);
		$input['from']=\Carbon\Carbon::parse($request['from'])->format('Y-m-d');
		$input['to']=\Carbon\Carbon::parse($request['to'])->format('Y-m-d');

		unset($input['_token']);
		$offer=Offer::create($input);
		if ($request->hasFile('photos')) {
            // $file = $request->file('photo');
            // $fileid=File::upload([$file],'uploads');
			// $offer->files()->attach(File::where('id',$fileid[0])->get()->first(),['is_profile'=>1]);
			// for($i=0;$i<count($input['photos']);$i++){
				$offer->addMultipleMediaFromRequest(['photos'])->each(function ($fileAdder) {
					$fileAdder->toMediaCollection('offer-photos');
				});

			// }
        }
		return "done";
	}
	public function index(){
		$offers    = Offer::whereDate('to','>=',\Carbon\Carbon::now())->paginate(16);
		$offercount= Offer::whereDate('to','>=',\Carbon\Carbon::now())->count();
		return view('website.companies.offers',compact('offers','offercount'));
	}
	public function edit($id){
		$offer = Offer::find($id);
		if(!$offer)
			abort(404);
		return view('website.offers.edit',compact('offer'));
	}
	public function update(Request $request, $id)
	{
		//
		$this->validate($request, [
            'title' => 'required|string',
			'price'=>'required',
			'from'=>'required|date',
			'to'=>'required|date|after_or_equal:from',
			'delivery_time'=>'required',
			'countries_id'=>'required',
			'category_id'=>'required',

		]);
		$input=$request->all();

		$input['materials']=json_encode($input['group-a']);
		$input['from']=\Carbon\Carbon::parse($request['from'])->format('Y-m-d');
		$input['to']=\Carbon\Carbon::parse($request['to'])->format('Y-m-d');

		unset($input['_token']);
		$offer=Offer::find($id);
		if($offer && $request->hasFile('photos')){
			$offer=Offer::where('id', $id)->first()->update($input);
			$offer=Offer::find($id);
			foreach($offer->images() as $image){
				$offer->deleteMedia($image->id);
			}
			
		}
		if ($request->hasFile('photos')) {
            // $file = $request->file('photo');
            // $fileid=File::upload([$file],'uploads');
			// $offer->files()->attach(File::where('id',$fileid[0])->get()->first(),['is_profile'=>1]);
			// for($i=0;$i<count($input['photos']);$i++){
				$offer->addMultipleMediaFromRequest(['photos'])->each(function ($fileAdder) {
					$fileAdder->toMediaCollection('offer-photos');
				});

			// }
        }
		return "done";
	}
	public function destroy($id)
    {
		//
		$offer   = Offer::find($id);
		if($offer && $offer->user_id==\Auth::user()->id){
			$offer->delete();
			Flash::success('Offer deleted successfully.');

			return back();
		}
		Flash::error('Offer not found');

		return back();
	}
	
	public function loadDataAjax(Request $request){
		$input     = $request->all();
        $offers    = \App\Offer::where('id','>',$input['id'])->limit(16)->get();
        return view('website.offers.listAjaxOfferData',compact('offers'));
	}

    public function allOffers($name,$id){
    	$offers  = Offer::where('user_id',$id)->get();
		return view('website.companies.offers',compact('offers'));  	
    }
    public function delete($id){
    	$offer   = Offer::find($id);
		if($offer && $offer->user_id==\Auth::user()->id){
			$result =$offer->delete();
			// session()->flash('status', 'deleted sucssfully');
			return response()->json(['message'=>'deleted sucssfully'],200);
			
		}
    		

    	else{
			return response()->json(['message'=>'Offer Not Found'],400);
    		session()->flash('danger', trans('website.error'));
			

		}
    }

}
