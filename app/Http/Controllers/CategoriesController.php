<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
class CategoriesController extends Controller
{
    //
    public function searchCategory($id){
    	$category    = Category::find($id);
    	$jobs   = $category->jobs;
    	$offers = $category->offers;
    	$count  = $jobs->count()+$offers->count();
    	return view('website.search.tag',compact('jobs','offers','count'));
    }
}
