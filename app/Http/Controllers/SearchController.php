<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Offer;
use App\Job;
use App\Business_profile;
use DB;
class SearchController extends Controller
{
    //
    public function search(){
        $search_key_word = $_GET['search_key_word'];
        $country_id      = $_GET['country'];
        if(isset($_GET['country']) && isset($_GET['search_key_word']) && isset($_GET['category'])){
            
            
            $category        = $_GET['category'];

            $offers          = $this->fetchCompaniesOffers($_GET['country'],$_GET['search_key_word']);

            if(empty($_GET['category'])){
                $companies       = $this->fetchCompaniesinCountry($_GET['country'],$_GET['search_key_word']);
                $count           = $companies->count()+$offers->count();    
                return view('website.search.index',compact('companies','offers','count','search_key_word','country_id'));
            }else{
                $count           = $offers->count();
                return view('website.search.offers',compact('offers','count','search_key_word','country_id','category'));

            }
        }else{

            $companies = $this->fetchCompaniesinCountry($_GET['country'],$_GET['search_key_word']);
            $offers    = $this->fetchCompaniesOffers($_GET['country'],$_GET['search_key_word']);
            $count     = $companies->count()+$offers->count();
            return view('website.search.index',compact('companies','offers','count','search_key_word','country_id'));
        }
    	
    }

    public function home_search(){
        

        $companies = $this->fetchCompaniesinCountry($_GET['country'],$_GET['search_key_word']);
        $offers    = $this->fetchCompaniesOffers($_GET['country'],$_GET['search_key_word']);
        $count     = $companies->count()+$offers->count();
        $search_key_word = $_GET['search_key_word'];
        $country_id = $_GET['country'];
        return view('website.search.index',compact('companies','offers','count','search_key_word','country_id'));
        
    }
    public function filter_search(){
        $search_key_word = $_GET['search_key_word'];
        $country_id = $_GET['country'];
        if(empty($_GET['category']))
        {
            $companies = $this->fetchCompaniesinCountry($_GET['country'],$_GET['search_key_word']);
            $offers    = $this->fetchCompaniesOffers($_GET['country'],$_GET['search_key_word']);
            $count     = $companies->count()+$offers->count();
            $search_key_word = $_GET['search_key_word'];
            $country_id = $_GET['country'];
            return view('website.search.index',compact('companies','offers','count','search_key_word','country_id'));
        }else{
            $offers = $this->fetchCompaniesOffers($_GET['country'],$_GET['search_key_word'],$_GET['category']);
            return view('website.search.offers_results',compact('offers','search_key_word','country_id'));
        }
        
    }

    public function fetchCompaniesinCountry( $country_id,$company_name,$category = null ){
        return $companies = DB::table('users')
                                ->join('business_profiles','users.id','=','business_profiles.id')
                                ->where('business_profiles.country_id','=',$country_id)
                                ->orWhere('users.name','like','%'.$company_name.'%')
                                ->take(4)->get();
    }

    public function fetchCompaniesOffers($country_id,$company_name,$category = null ){
        if($category == 'all')
            $offers = Offer::where('countries_id',$country_id)->orWhere('title','like','%'.$company_name.'%')->take(4)->get();
        else

            $offers = Offer::where('countries_id',$country_id)->orWhere('title','like','%'.$company_name.'%')->orWhere('category_id','=',$category)->take(4)->get();
        return $offers->where('to','>=',date('Y-m-d'));

    }

    public function searchOffers()
    {
        $offers = $this->fetchCompaniesOffers($_GET['country'],$_GET['search_key_word'],$_GET['category']);
        $offercount= $offers->count();
        $search_key_word = $_GET['search_key_word'];
        $country_id = $_GET['country'];
        $category_id        = $_GET['category'];
        return view('website.companies.offers',compact('offers','search_key_word','country_id','category_id','offercount'));
    }

    
}
