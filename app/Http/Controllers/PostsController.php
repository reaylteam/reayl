<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = collect();
        $followings=\Auth::user()->following;
        if(!count($followings)){
            $followings= \App\User::where('level','business')->take(6)->get()->sortByDesc('top');
        }
        foreach($followings as $follow){
            $posts = $posts->merge($follow->posts);
        }
        $posts=$posts->take(10);
        $posts =$posts->sortByDesc('created_at')->all();
        return view('website.posts.index',compact('posts'));
    }

    public function allPosts($user_name,$id){
        $posts = Post::where('user_id',$id)->get();
        return view('website.posts.index',compact('posts'));
    }

    public function loadDataAjax(Request $request){
        $input     = $request->all();
        $posts     = Post::where('id','>',$input['id'])->limit(10)->get();
        return view('website.posts.listAjaxData',compact('posts')); 
    }

    public function loadCommentsDataAjax(Request $request){
        $input     = $request->all();
        $post      = Post::find($input['id']);
        if(!$post)
            return;
        $comments  = $post->comments->where('id','>',$input['comment_id'])->take(5);
        $last      = $comments->last()->id;
        return view('website.posts.commentAjaxData',compact('comments','last'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
