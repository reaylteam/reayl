<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use App\Http\Controllers\Controller;
use App\User;
use App\Social_user;
use App\VerifyUser;
use App\Mail\VerifyMail;
use Session;
use App\Business_profile;
use App\Individual_user;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    // Some methods which were generated with the app
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */

    public function redirectToProvider($provider)
    {

        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that 
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        
        $user = Socialite::driver($provider)->user();
        $pathToFile = $user->avatar;
        $authUser = $this->findOrCreateUser($user, $provider,$pathToFile);
        $this->APIResult($authUser,$user);

        Auth::login($authUser, true);

        return redirect('/');
    }
    public function findOrCreateUser($user, $provider,$pathToFile)
    {

        if($provider == 'twitter')

            $authUser = User::where('email', $user->nickname)->first();
        else
            $authUser = User::where('email', $user->email)->first();

        if ($authUser) {
            return $authUser;
        }
        
        $user = User::create([
            'name'     => $user->name,
            'email'    => ($provider == 'twitter') ? $user->nickname :$user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'level'       => 'user',
            'status'  =>'Active',
            'verified'=>0
        ]);
        $this->attachDetaileRecord($user,'user');
        $user->addMediaFromUrl($pathToFile)->toMediaCollection('profiles');
        $user->provider = $provider;
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
        \Mail::to($user->email)->send(new VerifyMail($user));

        
        return $user;
    }
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if (\Carbon\Carbon::now()->greaterThan($verifyUser->created_at->addDay())) {
            $user = User::find($verifyUser->user->id);
            if($user)
                $user->delete();
            return redirect('/')->with('warning', "Sorry Link experied.");
        }
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();
                $status = "Your Account got activated succesfuuly";
                return view('auth.setting_password',compact('user'));
            }else{
                $status = "Your e-mail is already verified. You can now login.";
                return view('auth.setting_password',compact('user'));

            }
        }else{
            return redirect('/login')->with('warning', "Sorry your email cannot be identified.");
        }
        return redirect('/login')->with('status', $status);
    }

    public function APIResult($user){
        
    }


    public function attachDetaileRecord($user,$usertype){
        if($usertype == 'user')
            $result = $this->createUserRecord($user->id);
        else
            $result = $this->createBussinessRecord($user->id);
        return $result;
    }

    public function createUserRecord($id){
        $individual_user = new Individual_user;
        $individual_user->user_id=$id;
        $result          =$individual_user->save();
        return $result;
    }
    public function createBussinessRecord($id){
        $individual_user = new Business_profile;
        $individual_user->user_id=$id;
        
        $result          =$individual_user->save();
        return $result;
    }
    public function doLogin(){
        $rememberme = request('rememberme') == 1?true:false;
        if (user()->attempt(['email' => request('email'), 'password' => request('password')], $rememberme)) {
            return redirect('admin');
        } else {
            session()->flash('error', trans('admin.inccorrect_information_login'));
            return redirect(('login'));
        }
    }

    public function setting_password(Request $request)
    {
        $messages = [
            'password.regex' => 'It should contain alphabitc, numerals, and special character at least with one capital and one small letter',
        ];
        $this->validate($request, [
            'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|confirmed',
        ],$messages);

        $input = $request->all();
        $user  = User::find($input['user_id']);
        if(!$user)
            return back();
        $user->password = bcrypt($input['password']);
        $user->save();
        $status = 'Your password updated succesfuuly';
        return redirect('/')->with('status',$status);

    }
    
}