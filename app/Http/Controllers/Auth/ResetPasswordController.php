<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Response;
use Request;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $token        = Request::segment(3);
        $hashed_record= \App\Password_reset::where('token',\Hash::check('plain-text',$token))->first();

        if($hashed_record)
        {
            if (\Carbon\Carbon::now()->greaterThan($hashed_record->created_at)) {

                \DB::table('password_resets')->where('token',\Hash::check('plain-text',$token))->delete();
                return redirect('/')->with('warning', "Sorry Link experied.");
            }
            return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $hashed_record->email]
            );
        }
        else
        {
            return redirect('/');
        }

        

    }
}
