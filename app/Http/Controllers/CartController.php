<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use Response;
use App\User;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $user       = \Auth::user();
        $user_offers= $user->offers;
        return view('website.user.offers.list',compact('user_offers'));

    }

    public function addToCart($id)
    {
        $cart_item = Cart::where('offer_id',$id)->where('user_id',\Auth::user()->id)->first();
        if($cart_item)
            return Response::json(['status'=>'error'],500);
        else
        {
            $new_item = Cart::create([
                'user_id' => \Auth::user()->id,
                'offer_id'=> $id
            ]);
            return Response::json(['status'=>'success'],201);
        }

    }

    public function myOffers($user_id,$user_name)
    {
        $items = Cart::where('user_id',$user_id)->get();
        $user  = User::find($user_id);
        if(!$user)
            return redirect()->back();
        return view('website.user.offers.list',compact('items','user'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $item = Cart::find($id);
        if(!$item)
            return redirect()->back();
        $result = $item->delete();
        if($result)
             return Response::json(['status'=>'success'],200);
        else
            return Response::json(['status'=>'error'],500);
    }
}
