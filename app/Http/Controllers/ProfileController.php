<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Individual_user;
use App\Tag;
class ProfileController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index(){

        $user    = \Auth::user();
        $profile = $user->getMedia('profiles');
        $cover   = $user->getMedia('covers');
        $posts= \Auth::user()->posts;
        // $posts->merge(\Auth::user()->posts);
        // foreach(\Auth::user()->following as $follow){
        //     $posts->merge($follow->posts);
        // }
        // $posts=$posts->all();
        if(\Auth::user()->level == 'user')
            return view('website.user.show',compact('user','posts','profile','cover'));
        else
            return view('website.user.businessshow',compact('user','posts','profile','cover'));
    }
    
    public function ShowProfile($email){
        $user = User::where('email',$emil)->first();

        return view('website.user.show',compact('user'));
    }
    public function edit($id=null){
        $user=\Auth::user();
        $profile = $user->getMedia('profiles');
        $cover   = $user->getMedia('covers');
        $intrests=\Auth::user()->intrests->pluck('id');
        if(\Auth::user()->level == 'user')
            return view('website.user.edit',compact('user','intrests','profile','cover'));
        else
            return view('website.user.businessedit',compact('user','intrests','profile','cover'));
        
    }
    public function show($id,$name){
        return "WERwe";
        
    }
    public function update(Request $request, $id){
        $this->validate($request, [
            'first_name' => 'required|string',
            'last_name' => 'required|string'
        ]);
        $user=\Auth::user();
        $input=$request->all();
            
        if(isset($inputp['intrests'])){
            \Auth::user()->intrests()->detach(\Auth::user()->intrests->pluck('id'));
            \Auth::user()->intrests()->attach($input['intrests']);
        }
        $input['user_id']=$user->id;
        unset($input['_method']);
        unset($input['_token']);
        $user->name=$input['first_name']." ".$input['last_name'];
        $user->save();
        $individual_user=Individual_user::where('user_id',$user->id)->first();
        if($individual_user){
            $individual_user->update($input);
        }
        else{
            Individual_user::create($input);
        }
        return redirect()->back();
    }
    public function changeCoverPhoto(Request $request){
        $input  = $request->all();
        $user   = User::find($input['user_id']);
        if(!$user)
            return redirect()->back();
        if(isset($input['cover'])){
            $covers = $user->getMedia('covers');
            if($covers->count())
                $covers[0]->delete();
            $user->addMedia($request->file('cover'))->toMediaCollection('covers');
        }
        return redirect()->back();
    }
    public function changeProfilePhoto(Request $request){
        $input  = $request->all();
        $user   = User::find($input['user_id']);
        if(!$user)
            return redirect()->back();
        if(isset($input['profile'])){
            $profiles = $user->getMedia('profiles');
            if($profiles->count())
                $profiles[0]->delete();
            $user->addMedia($request->file('profile'))->toMediaCollection('profiles');
        }
        return redirect()->back();
    }
    public function followers(){
        $users=\Auth::user()->follower;
        $type="Followers";
        return view('website.user.follow',compact('users','type'));

    }
    public function followings(){
        $users=\Auth::user()->following;
        $type="Followings";
        return view('website.user.follow',compact('users','type'));
        
    }
    public function ProfileShow($id,$name){
        $user    = User::where('id',$id)->where('name',$name)->first();
        if(!$user){
            return back();
        }
        $jobs    = \App\Job::where('user_id',$user->id)->get();
        $count   = 0;
        foreach ($jobs as $key => $value) {
            $application = \App\Application::where('job_id',$value->id)->where('user_id',$user->id);
            if($application)
                $count++;
        }
        if(!$user)
            abort('404');
        $profile = $user->getMedia('profiles');
        $cover   = $user->getMedia('covers');
        $posts= $user->posts;
        // $posts->merge(\Auth::user()->posts);
        // foreach(\Auth::user()->following as $follow){
        //     $posts->merge($follow->posts);
        // }
        // $posts=$posts->all();

        if($user->level == 'user')
            return view('website.user.show.showUserProfiletoMe',compact('user','posts','profile','cover','count'));
        else
            return view('website.user.businessshow',compact('user','posts','profile','cover','count'));
    }

    public function publicProfile($type,$id,$name){
        if($type=='business')
            return $this->fireBusinessProfile($id,$name);
        else if($type=='user')
            return $this->fireUserProfile($id,$name);
        else
            abort(404);
    }
    public function fireBusinessProfile($id,$name){

        $user    = User::where('id',$id)->where('name',$name)->first();
        $profile = $user->getMedia('profiles');
        $cover   = $user->getMedia('covers');
        return view('website.user.show.showBusinessProfiletoMe',compact('user','profile','cover'));

    }
    public function fireUserProfile($id,$name){
        $user    = User::where('id',$id)->where('name',$name)->first();
        $profile = $user->getMedia('profiles');
        $cover   = $user->getMedia('covers');
        return view('website.user.show.showUserProfiletoMe',compact('user','profile','cover'));

    }

    public function setUpLocation($lat,$lang){
        $user      = \Auth::user();
        $user->lat = $lat;
        $user->lang= $lang;
        $saved     =$user->save();
        if($saved)
            return 'saved';
        else
            return 'error';
    }

    public function editmyprofile(Request $request){
        if($request->file('profile')){
            \Auth::user()->addMedia($request->file('profile'))->toMediaCollection('profiles');
            return response()->json(['message'=>'Uploaded successfully'],200);
        }
        else{
            return response()->json(['message'=>'Error Upload File.'],400);
        }
    }
    public function editmycover(Request $request){
        if($request->file('cover')){
            \Auth::user()->addMedia($request->file('cover'))->toMediaCollection('covers');
            return response()->json(['message'=>'Uploaded successfully'],200);
        }
        else{
            return response()->json(['message'=>'Error Upload File.'],400);
        }
    }
}
