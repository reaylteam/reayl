<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
class CheckActivation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //dd( \CRUDBooster::myId() );
        $current_user = \Auth::user();
        if (!($current_user->verified)) {
            abort(503);
        }
        return $next($request);
    }
}
