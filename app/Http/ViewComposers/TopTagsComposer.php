<?php


namespace App\Http\ViewComposers;

use Illuminate\View\View;

class TopTagsComposer{
	public function compose(View $view){
		$view->with('currentUser', \Auth::user());
		
	}
}