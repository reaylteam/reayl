<?php

namespace App\Providers;
use App\Post;
use App\Observers\PostObserver;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(255);
        Post::observe(PostObserver::class);
        $currentUser = View::share('currentUser', \Auth::user());
        $common = $this->commonVars($currentUser);
        View::share('top_categories',$common['top_categories']);
        View::share('top_tags',$common['top_tags']);
        View::share('top_companies',$common['top_companies']);
        View::share('job_list',$common['job_list']);
        View::share('users_u_may_know',$common['users_u_may_know']);
        View::share('new_offers',$common['new_offers']);
        View::share('static_pages',$common['static_pages']);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
    public function commonVars($currentUser){
        $top_tags                = \App\Tag::all()->sortByDesc('top')->take(5);
        $top_companies           = \App\User::where('level','business')->take(6)->get()->sortByDesc('top');
        $top_categories          = \App\Category::all()->sortByDesc('top')->take(5);
        $common = array();
        $common['top_tags']      =$top_tags;
        $common['top_companies'] =$top_companies;
        $common['top_categories']=$top_categories;
        $common['job_list']      = \App\Job::orderByRaw('RAND()')->take(4)->get();
        $common['users_u_may_know']=$this->peopleMayKnow($currentUser);
        $common['new_offers']    = \App\Offer::orderBy('created_at','desc')->take(6)->get();
        $common['static_pages']  = \App\Static_page::all();
        return $common;
    }
    public function peopleMayKnow($currentUser){


        if($currentUser){

            $intrests=$currentUser->intrests->pluck('id');
            return $users_u_may_know = \App\User::whereHas('intrests', function ($query) {
                $query->whereIn($query->intrest_id,$intrests);
            })->get();
        }else{
            return $users_u_may_know = \App\User::orderByRaw('RAND()')->take(4)->get();
        }
    }
}
