<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    //
    public $table = 'followings';

    public $fillable = [
        'follower_id',
        'following_id'
    ];
}
