<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intrest extends Model
{
    //
    public $table = 'intrests';
    public $fillable = [
        'name'
    ];

    public function users(){
        return $this->belongsToMany('App\User','user_intrests');
    }
}
