<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Permission\Traits\HasRoles;
use Yajra\Acl\Traits\HasRole;

class User extends Authenticatable implements HasMedia

{
    use Notifiable;
    use HasMediaTrait;
    use HasRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','level','user_id','verified'
    ];
    protected $appends  = array('top');
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }
    public function posts(){
        return $this->hasMany('App\Post');
    }
    public function individual_user_info(){ 
        return $this->hasOne('App\Individual_user');   
    }
    public function business_user_info(){
        return $this->hasOne('App\Business_profile');
    }

    public function follower()
    {
        return $this->belongsToMany('App\User','followings','following_id','follower_id');
    }
    public function following(){
        return $this->belongsToMany('App\User','followings','follower_id','following_id');
    }
    public function intrests(){
        return $this->belongsToMany('App\Intrest','user_intrests');
    }
    // public function tags(){
    //     return $this->belongsToMany('App\Tag','user_bussiness_has_tags');
    // }

    public function checkfollow($id){
        $check=Follow::where('follower_id',\Auth::user()->id)->where('following_id',$id)->first();
        if($check){
            return true;
        }
        else{
            return false;
        }
    }
    public function offers(){
        return $this->hasMany('App\Offer','user_id');
    }

    public function jobs(){
        return $this->hasMany('App\Job','user_id');
    }
    public function getRolesNames(){
        return $this->getRoleNames();
    }
    public function profile(){
        $profile = $this->getMedia('profiles');
        if($profile->count())
            return $profile[$profile->count()-1];
        else
            return false;
    }
    public function cover(){
        $cover = $this->getMedia('covers');
        if($cover->count())
            return $cover[$cover->count()-1];
        else
            return false;
    }
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
    public function sub_users(){
        return $this->hasMany(self::class,'user_id');
    }
    public function getTopAttribute(){
        return $this->posts->count()+$this->offers->count();
    }
    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany('App\Tag', 'taggable');
    }
    public function getProfile(){
        return $this->getMedia('profiles');
    }
    public function parent(){
        return $this->belongsTo(self::class,'user_id');
    }

    public function cart()
    {
        return $this->hasMany('App\Cart','user_id');
    }

}
