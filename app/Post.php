<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sassnowski\LaravelShareableModel\Shareable\Shareable;
use Sassnowski\LaravelShareableModel\Shareable\ShareableInterface;

class Post extends Model implements ShareableInterface
{
    //
    use Shareable;    

    use \Conner\Likeable\LikeableTrait;
    protected $fillable = ['body','user_id']; 
    protected $appends  = array('duration','likes','liked');

    public function user(){
    	return $this->belongsTo('App\User','user_id');
    }
    public function getDurationAttribute(){
    	return $this->created_at->diffForHumans();
    }
    public function getLikesAttribute(){
        return $this->likeCount;
    }
    public function getLikedAttribute(){
        return $this->liked(\Auth::id());
    }

    /**
     * Get all of the tags for the post.
     */
    public function comments()
    {
        return $this->morphToMany('App\Comment', 'commentable');
    }
    
    
        
}
